# https://apple.stackexchange.com/questions/12387/how-to-send-an-email-from-command-line
# # https://www.developerfiles.com/how-to-send-emails-from-localhost-mac-os-x-el-capitan/
# Librerías
library(dplyr)
library(ggplot2)
library(readr)
library(forcats)
library(repr)
library(mltools)
library(data.table)
library(rvest)
library(stringi)
library(zoo)
library(PMwR)
library(Quandl)
library(sendmailR)

# Lectura de datos train
gold_price <- Quandl("WGC/GOLD_DAILY_USD", api_key="xCLj7WmfLsvdi9xo2okv")
names(gold_price) <- c('date', 'goldprice')

dolar_price <- Quandl("BDM/SF60653", api_key="xCLj7WmfLsvdi9xo2okv")
names(dolar_price) <- c('date', 'dolarprice')

# Homologación de datos train
max_date <- max(gold_price$date)
min_date <- min(dolar_price$date)

gold_price %>%
  filter(date >= min_date) %>%
  filter(date <= max_date) -> gold_price

dolar_price %>%
  filter(date >= min_date) %>%
  filter(date <= max_date) -> dolar_price

# Join Data Train
dolar_price %>%
  left_join(gold_price, by = 'date') -> data

# Limpiar Datos Train
data %>%
  na.omit() -> data

data %>%
  filter(date >= as.Date('2010-01-01')) -> data

# Ingeniería de datos Train
## Promedio semanal y mes
data %>%
  mutate(week = paste0(year(date),'-',format(date, '%W'))) %>%
  group_by(week) %>%
  summarise(min_date = min(date),
            max_date = max(date),
            avg_gold = mean(goldprice, na.rm = TRUE),
            #avg_dol  = mean(dolarprice, na.rm = TRUE),
            std_gold = sd(goldprice, na.rm = TRUE)) %>%
  arrange(as.numeric(gsub('-','',week))) %>%
  filter(!is.na(std_gold)) %>%
  mutate(mes = month(max_date)) %>%
  mutate(mes = ifelse(mes == 1, 'Ene', mes),
         mes = ifelse(mes == 2, 'Feb', mes),
         mes = ifelse(mes == 3, 'Mar', mes),
         mes = ifelse(mes == 4, 'Abr', mes),
         mes = ifelse(mes == 5, 'May', mes),
         mes = ifelse(mes == 6, 'Jun', mes),
         mes = ifelse(mes == 7, 'Jul', mes),
         mes = ifelse(mes == 8, 'Ago', mes),
         mes = ifelse(mes == 9, 'Sep', mes),
         mes = ifelse(mes == 10, 'Oct', mes),
         mes = ifelse(mes == 11, 'Nov', mes),
         mes = ifelse(mes == 12, 'Dic', mes)) -> data

## Rendimientos Train
rend_func <- function(df, col_ref, nom_col_nueva, ind_train){
  
  # Para predict se quita un lag
  
  if(ind_train == TRUE){
    df %>%
      mutate_(aux1 = col_ref) %>%
      mutate(aux2  = lag(aux1)) %>%
      mutate(aux3  = log(aux2/lag(aux2))) %>%
      select(-aux1, -aux2) -> d 
  } else{
    df %>%
      mutate_(aux1 = col_ref) %>%
      mutate(aux2  = aux1) %>%
      mutate(aux3  = log(aux1/lag(aux2))) %>%
      select(-aux1, -aux2) -> d
  }
  
  names(d)[which(names(d) == 'aux3')] <- nom_col_nueva
  
  return(d)
}

data <- rend_func(data, 'avg_gold', 'rend', ind_train = TRUE)
#data <- rend_func(data, 'avg_dol', 'rend_dol', ind_train = TRUE)

# Rezagos train
rez_func <- function(df, col_ref, nom_col_nueva, num_rezagos, ind_train){
  # Cuando es para predict, se quita un lag
  if(ind_train == TRUE){
    # Se crea vector con código de número de lags a hacer y sus paréntesis de cierre para train
    num_lags  <- rep('lag(',num_rezagos)
    close_par <- rep(')', num_rezagos)
  } else {
    # Se crea vector con código de número de lags a hacer y sus paréntesis de cierre para predict
    num_lags  <- rep('lag(',num_rezagos-1)
    close_par <- rep(')', num_rezagos-1)
  }
  
  # Se convierten a un sólo caracter
  num_lags  <- stri_paste(num_lags, collapse = '')
  close_par <- stri_paste(close_par, collapse = '')
  
  # Se pega el código
  cod <- paste0(num_lags, 'aux', close_par)
  
  # Se crea la columna nueva
  df %>%
    mutate_(aux = col_ref) %>%
    mutate_(aux = cod) -> d
  
  names(d)[which(names(d) == 'aux')] <- nom_col_nueva
  
  return(d)
}

data <- rez_func(data, 'avg_gold', 'rez_avg_gold_1s',  1,  TRUE)
data <- rez_func(data, 'avg_gold', 'rez_avg_gold_2s',  2,  TRUE)
data <- rez_func(data, 'avg_gold', 'rez_avg_gold_3s',  3,  TRUE)
data <- rez_func(data, 'avg_gold', 'rez_avg_gold_4s',  4,  TRUE)
data <- rez_func(data, 'avg_gold', 'rez_avg_gold_6s',  6,  TRUE)
data <- rez_func(data, 'avg_gold', 'rez_avg_gold_8s',  8,  TRUE)
data <- rez_func(data, 'std_gold', 'rez_std_gold_1s',  1,  TRUE)
data <- rez_func(data, 'std_gold', 'rez_std_gold_2s',  2,  TRUE)
data <- rez_func(data, 'std_gold', 'rez_std_gold_3s',  3,  TRUE)
data <- rez_func(data, 'std_gold', 'rez_std_gold_4s',  4,  TRUE)
data <- rez_func(data, 'std_gold', 'rez_std_gold_6s',  6,  TRUE)
data <- rez_func(data, 'std_gold', 'rez_std_gold_8s',  8,  TRUE)
data <- rez_func(data, 'rend', 'rez_rend_gold_1s',  1,  TRUE)
data <- rez_func(data, 'rend', 'rez_rend_gold_2s',  2,  TRUE)
data <- rez_func(data, 'rend', 'rez_rend_gold_3s',  3,  TRUE)
data <- rez_func(data, 'rend', 'rez_rend_gold_4s',  4,  TRUE)
data <- rez_func(data, 'rend', 'rez_rend_gold_6s',  6,  TRUE)
data <- rez_func(data, 'rend', 'rez_rend_gold_8s',  8,  TRUE)

data %>%
  mutate(mes = lag(mes)) -> data

## Medias Móviles train
mm_func <- function(df, col_ref, nom_col_nueva, t, ind_train){
  # Cuando es para predict se quita un lag para calcular las medias móviles
  if(ind_train == TRUE){
    # Se crea el vector de medias móviles a "t" tiempos para train
    df %>%
      mutate_(aux = col_ref) %>%
      mutate(aux = lag(aux)) %>%
      select(aux) %>%
      pull() %>%
      rollmean(x = ., k = t) -> mm
  } else{
    # Se crea el vector de medias móviles a "t" tiempos para predict
    df %>%
      mutate_(aux = col_ref) %>%
      mutate(aux = aux) %>%
      select(aux) %>%
      pull() %>%
      rollmean(x = ., k = t) -> mm
    
  }
  
  # Para poder agregar la columna, se deben agregar NA's al principio para que coincida el número de columnas
  mm <- c(rep(NA,t-1), mm)
  
  # Se agrega el vector de medias móviles al df
  df %>%
    mutate(aux = mm) -> d
  
  names(d)[which(names(d) == 'aux')] <- nom_col_nueva
  
  return(d)
}

data <- mm_func(data, 'avg_gold', 'mm2_avg_gold', 2, TRUE)
data <- mm_func(data, 'avg_gold', 'mm3_avg_gold', 3, TRUE)
data <- mm_func(data, 'avg_gold', 'mm4_avg_gold', 4, TRUE)
data <- mm_func(data, 'std_gold', 'mm2_std_gold', 2, TRUE)
data <- mm_func(data, 'std_gold', 'mm3_std_gold', 3, TRUE)
data <- mm_func(data, 'std_gold', 'mm4_std_gold', 4, TRUE)
data <- mm_func(data, 'rend', 'mm2_rend', 2, TRUE)
data <- mm_func(data, 'rend', 'mm3_rend', 3, TRUE)
data <- mm_func(data, 'rend', 'mm4_rend', 4, TRUE)
#data <- mm_func(data, 'rend_dol', 'mm2_rend_dol', 2, TRUE)
#data <- mm_func(data, 'rend_dol', 'mm3_rend_dol', 3, TRUE)
#data <- mm_func(data, 'rend_dol', 'mm4_rend_dol', 4, TRUE)

## Rachas train
racha <- function(df, col_ref, nom_col_nueva, ind_train){
  # Cuando es para predict se quita un lag para calcular las medias móviles
  if(ind_train == TRUE){
    df %>%
      mutate_(aux1 = col_ref) %>%
      mutate(aux2  = lag(aux1)) %>%
      mutate(aux3  = aux2-lag(aux2)) %>%
      mutate(ind = ifelse(aux3 <= 0, FALSE, TRUE)) %>%
      group_by(grp = rleid(ind)) %>%
      mutate(n = rank(min_date)) %>%
      mutate(n = ifelse(ind == FALSE, 0, n)) %>%
      data.frame() %>%
      mutate(racha = lag(n)) %>%
      select(-aux1, -aux2, -aux3, -n, -grp, -ind) -> d
  } else {
    df %>%
      mutate_(aux1 = col_ref) %>%
      mutate(aux2  = aux1) %>%
      mutate(aux3  = aux1-lag(aux2)) %>%
      mutate(ind = ifelse(aux3 <= 0, FALSE, TRUE)) %>%
      group_by(grp = rleid(ind)) %>%
      mutate(n = rank(min_date)) %>%
      mutate(n = ifelse(ind == FALSE, 0, n)) %>%
      data.frame() %>%
      mutate(racha = lag(n)) %>%
      select(-aux1, -aux2, -aux3, -n, -grp, -ind) -> d 
  }
  
  names(d)[which(names(d) == 'racha')] <- nom_col_nueva
  
  return(d)
  
}

data <- racha(data, 'avg_gold', 'rachas_sube_gold', TRUE)
data <- racha(data, 'rez_avg_gold_1s', 'rachas_sube_gold_rez1d', TRUE)
data <- racha(data, 'rez_std_gold_1s', 'rachas_sube_stdgold_rez1d', TRUE)
#data <- racha(data, 'avg_dol', 'racha_avg_dol', TRUE)
#data <- racha(data, 'rend_dol', 'racha_rend_dol', TRUE)

## Dummies mes train
data %>%
  mutate(mes = as.factor(mes)) -> data

one_hot(as.data.table(data)) %>%
  as.data.frame -> data

data$mes_Ene <- NULL


## Polinomios Train
data %>%
  mutate(rez_avg_gold_1s_2 = rez_avg_gold_1s^2,
         rend_2 = rend^2
         #rend_3 = rend^3, rachas_sube_gold_2 = rachas_sube_gold^2
  ) -> data

data %>%
  na.omit() -> data

# Fit
set.seed(50)
indices <- sample(1:nrow(data), nrow(data)*0.8)
train <- data[indices,]
valid <- data[-indices,]

#m1 <- lm(avg_gold ~ ., data = train %>% select(-week, -min_date, -max_date, -std_gold))
#m1 <- step(m1)

# Modelo para semana n+1
m1 <- lm(avg_gold~., data = train %>% select(avg_gold, rez_avg_gold_1s, rez_avg_gold_2s,  rez_avg_gold_4s, rez_avg_gold_8s, rez_std_gold_8s, rez_rend_gold_8s, rachas_sube_stdgold_rez1d))

# Modelo para semana n+2
m2 <- lm(avg_gold~., data = train %>% select(avg_gold, rez_avg_gold_1s, rez_avg_gold_2s,  rez_avg_gold_4s, rez_avg_gold_8s, rez_rend_gold_8s))

#options(repr.plot.width=5, repr.plot.height=4)
data.frame(desc = c('train', 'valid'),
           rmse = c(rmse(train$avg_gold, predict(m1, train)),
                    rmse(valid$avg_gold, predict(m1, valid)))) %>%
  ggplot() +
  geom_bar(aes(x = desc, y = rmse), stat = 'identity', fill = 'darkblue') +
  theme_bw() +
  ggtitle('RMSE') +
  theme(axis.text.x = element_text(face = 'bold'),
        axis.text.y = element_text(face = 'bold')) +
  geom_text(aes(x = desc, y = rmse, label = round(rmse,2)), vjust = -0.5, size = 4) -> rmse_plot

m1$coefficients %>%
  data.frame() %>%
  rename('coef' = '.') %>%
  mutate(variable = rownames(.)) %>%
  filter(variable != '(Intercept)') %>%
  filter(!is.na(coef)) %>%
  arrange(coef) %>%
  mutate(variable = fct_inorder(variable)) %>%
  ggplot() +
  geom_col(aes(x = variable, y = coef), fill = 'darkblue') +
  theme_bw() +
  scale_y_continuous(labels = scales::comma) +
  theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.5)) -> coef_plot

#options(repr.plot.width=3, repr.plot.height=3)
valid %>%
  mutate(pred = predict(m1, valid)) %>%
  mutate(error = avg_gold - pred) %>%
  ggplot() +
  geom_line(aes(x = max_date, y = error, group = 1), colour = 'darkblue') +
  theme_bw() +
  ggtitle('Gráfico de errores') -> errores_plot





# Predicción

## Lectura y tranformación de datos
gold_pred <- Quandl("WGC/GOLD_DAILY_USD", api_key="xCLj7WmfLsvdi9xo2okv")
names(gold_pred) <- c('date', 'goldprice')

dolar_pred <- Quandl("BDM/SF60653", api_key="xCLj7WmfLsvdi9xo2okv")
names(dolar_pred) <- c('date', 'dolarprice')

max_date <- max(gold_pred$date)
min_date <- min(dolar_pred$date)

gold_pred %>%
  filter(date >= min_date) %>%
  filter(date <= max_date) -> gold_pred

dolar_pred %>%
  filter(date >= min_date) %>%
  filter(date <= max_date) -> dolar_pred

dolar_pred %>%
  left_join(gold_pred, by = 'date') %>%
  select(-dolarprice) -> data_pred

data_pred %>%
  na.omit() -> data_pred

data_pred %>%
  filter(date >= as.Date('2015-01-01')) -> data_pred

data_pred %>%
  mutate(week = paste0(year(date),'-',format(date, '%W'))) %>%
  group_by(week) %>%
  summarise(min_date = min(date),
            max_date = max(date),
            avg_gold = mean(goldprice, na.rm = TRUE),
            std_gold = sd(goldprice, na.rm = TRUE)) %>%
  arrange(as.numeric(gsub('-','',week))) %>%
  filter(!is.na(std_gold)) %>%
  mutate(mes = month(max_date)) %>%
  mutate(mes = ifelse(mes == 1, 'Ene', mes),
         mes = ifelse(mes == 2, 'Feb', mes),
         mes = ifelse(mes == 3, 'Mar', mes),
         mes = ifelse(mes == 4, 'Abr', mes),
         mes = ifelse(mes == 5, 'May', mes),
         mes = ifelse(mes == 6, 'Jun', mes),
         mes = ifelse(mes == 7, 'Jul', mes),
         mes = ifelse(mes == 8, 'Ago', mes),
         mes = ifelse(mes == 9, 'Sep', mes),
         mes = ifelse(mes == 10, 'Oct', mes),
         mes = ifelse(mes == 11, 'Nov', mes),
         mes = ifelse(mes == 12, 'Dic', mes)) -> data_pred

data_pred <- rend_func(data_pred, 'avg_gold', 'rend', ind_train = FALSE)

data_pred <- rez_func(data_pred, 'avg_gold', 'rez_avg_gold_1s',  1,  FALSE)
data_pred <- rez_func(data_pred, 'avg_gold', 'rez_avg_gold_2s',  2,  FALSE)
data_pred <- rez_func(data_pred, 'avg_gold', 'rez_avg_gold_3s',  3,  FALSE)
data_pred <- rez_func(data_pred, 'avg_gold', 'rez_avg_gold_4s',  4,  FALSE)
data_pred <- rez_func(data_pred, 'avg_gold', 'rez_avg_gold_6s',  6,  FALSE)
data_pred <- rez_func(data_pred, 'avg_gold', 'rez_avg_gold_8s',  8,  FALSE)
data_pred <- rez_func(data_pred, 'std_gold', 'rez_std_gold_1s',  1,  FALSE)
data_pred <- rez_func(data_pred, 'std_gold', 'rez_std_gold_2s',  2,  FALSE)
data_pred <- rez_func(data_pred, 'std_gold', 'rez_std_gold_3s',  3,  FALSE)
data_pred <- rez_func(data_pred, 'std_gold', 'rez_std_gold_4s',  4,  FALSE)
data_pred <- rez_func(data_pred, 'std_gold', 'rez_std_gold_6s',  6,  FALSE)
data_pred <- rez_func(data_pred, 'std_gold', 'rez_std_gold_8s',  8,  FALSE)
data_pred <- rez_func(data_pred, 'rend', 'rez_rend_gold_1s',  1,  FALSE)
data_pred <- rez_func(data_pred, 'rend', 'rez_rend_gold_2s',  2,  FALSE)
data_pred <- rez_func(data_pred, 'rend', 'rez_rend_gold_3s',  3,  FALSE)
data_pred <- rez_func(data_pred, 'rend', 'rez_rend_gold_4s',  4,  FALSE)
data_pred <- rez_func(data_pred, 'rend', 'rez_rend_gold_6s',  6,  FALSE)
data_pred <- rez_func(data_pred, 'rend', 'rez_rend_gold_8s',  8,  FALSE)
data_pred <- mm_func(data_pred, 'avg_gold', 'mm2_avg_gold', 2, TRUE)
data_pred <- mm_func(data_pred, 'avg_gold', 'mm3_avg_gold', 3, TRUE)
data_pred <- mm_func(data_pred, 'avg_gold', 'mm4_avg_gold', 4, TRUE)
data_pred <- mm_func(data_pred, 'std_gold', 'mm2_std_gold', 2, TRUE)
data_pred <- mm_func(data_pred, 'std_gold', 'mm3_std_gold', 3, TRUE)
data_pred <- mm_func(data_pred, 'std_gold', 'mm4_std_gold', 4, TRUE)
data_pred <- mm_func(data_pred, 'rend', 'mm2_rend', 2, TRUE)
data_pred <- mm_func(data_pred, 'rend', 'mm3_rend', 3, TRUE)
data_pred <- mm_func(data_pred, 'rend', 'mm4_rend', 4, TRUE)
data_pred <- racha(data_pred, 'avg_gold', 'rachas_sube_gold', FALSE)
data_pred <- racha(data_pred, 'rez_avg_gold_1s', 'rachas_sube_gold_rez1d', FALSE)
data_pred <- racha(data_pred, 'rez_std_gold_1s', 'rachas_sube_stdgold_rez1d', FALSE)
data_pred %>%
  mutate(mes = as.factor(mes)) -> data_pred

one_hot(as.data.table(data_pred)) %>%
  as.data.frame -> data_pred

data_pred %>%
  mutate(rez_avg_gold_1s_2 = rez_avg_gold_1s^2,
         rend_2 = rend^2,
         rend_3 = rend^3,
         rachas_sube_gold_2 = rachas_sube_gold^2,
         rez_rend_gold_2s_2 = rez_rend_gold_2s^2,
         rez_avg_gold_8s_2 = rez_avg_gold_8s^2) -> data_pred
data_pred %>%
  na.omit() -> data_pred

# Predicción siguiente semana
sp <- predict(m1, tail(data_pred,1))

# Promedio semana Actual
sa <- tail(data_pred$avg_gold,1)

# Resumen pronóstico
# RMSE del modelo en validación
rmse_valid <- rmse(valid$avg_gold, predict(m1, valid))

# Indicador de alta o baja: pronosticado proxima semana vs semana actual
if(sp >= sa){
  a <- 'subida'
  r    <- abs((sp)/(sa))-1
} else {
  a <- 'bajada'
  r    <- 1-abs((sp)/(sa))
}

# Intervalo de confianza inferior
if((sp-rmse_valid)/(sa) >= 1){
  II <- (sp-rmse_valid)/(sa)-1
} else {
  II <- 1-(sp-rmse_valid)/(sa)
}

# Intervalo de confianza superior
if((sp+rmse_valid)/(sa) >= 1){
  IS <- (sp+rmse_valid)/(sa)-1
} else {
  IS <- 1-(sp+rmse_valid)/(sa)
}

# Indicador de alta o baja de intervalo inferior
if(sp-rmse_valid>=sa){
  ai <- 'subir'
} else {
  ai <- 'bajar'
}

# Indicador de alta o baja de intervalo superior
if(sp+rmse_valid>=sa){
  as <- 'subir'
} else {
  as <- 'bajar'
}

# Predicción segunda semana

## Se crea un data frame sólo con fechas y el y el promedio del oro
data_pred %>%
  mutate(mes = month(max_date)) %>%
  select(week, min_date, max_date, mes, avg_gold) -> data_pred2

## Se obtienen las min y max fechas de la semana predicha
minf <- tail(data_pred$max_date,1) + 3
maxf <- tail(data_pred$max_date,1) + 11

## se obtiene el mes
m <- month(maxf)

## Se obtiene el número de semana
w <- paste0(year(maxf),'-',format(maxf, '%W'))

## Se crea data frame con la fila de la última semana predicha
data.frame(week = w,
           min_date = minf,
           max_date = maxf,
           mes = m,
           avg_gold = sp) -> df

## Se agrega la fila al data frame con los históricos
data_pred2 <- rbind(data_pred2, df)

# Se crean variables para el data frame
##################
data_pred2 %>%
  mutate(mes = ifelse(mes == 1, 'Ene', mes),
       mes = ifelse(mes == 2, 'Feb', mes),
       mes = ifelse(mes == 3, 'Mar', mes),
       mes = ifelse(mes == 4, 'Abr', mes),
       mes = ifelse(mes == 5, 'May', mes),
       mes = ifelse(mes == 6, 'Jun', mes),
       mes = ifelse(mes == 7, 'Jul', mes),
       mes = ifelse(mes == 8, 'Ago', mes),
       mes = ifelse(mes == 9, 'Sep', mes),
       mes = ifelse(mes == 10, 'Oct', mes),
       mes = ifelse(mes == 11, 'Nov', mes),
       mes = ifelse(mes == 12, 'Dic', mes)) -> data_pred2

data_pred2 <- rend_func(data_pred2, 'avg_gold', 'rend', ind_train = FALSE)

data_pred2 <- rez_func(data_pred2, 'avg_gold', 'rez_avg_gold_1s',  1,  FALSE)
data_pred2 <- rez_func(data_pred2, 'avg_gold', 'rez_avg_gold_2s',  2,  FALSE)
data_pred2 <- rez_func(data_pred2, 'avg_gold', 'rez_avg_gold_3s',  3,  FALSE)
data_pred2 <- rez_func(data_pred2, 'avg_gold', 'rez_avg_gold_4s',  4,  FALSE)
data_pred2 <- rez_func(data_pred2, 'avg_gold', 'rez_avg_gold_6s',  6,  FALSE)
data_pred2 <- rez_func(data_pred2, 'avg_gold', 'rez_avg_gold_8s',  8,  FALSE)

data_pred2 <- rez_func(data_pred2, 'rend', 'rez_rend_gold_1s',  1,  FALSE)
data_pred2 <- rez_func(data_pred2, 'rend', 'rez_rend_gold_2s',  2,  FALSE)
data_pred2 <- rez_func(data_pred2, 'rend', 'rez_rend_gold_3s',  3,  FALSE)
data_pred2 <- rez_func(data_pred2, 'rend', 'rez_rend_gold_4s',  4,  FALSE)
data_pred2 <- rez_func(data_pred2, 'rend', 'rez_rend_gold_6s',  6,  FALSE)
data_pred2 <- rez_func(data_pred2, 'rend', 'rez_rend_gold_8s',  8,  FALSE)
data_pred2 <- mm_func(data_pred2, 'avg_gold', 'mm2_avg_gold', 2, TRUE)
data_pred2 <- mm_func(data_pred2, 'avg_gold', 'mm3_avg_gold', 3, TRUE)
data_pred2 <- mm_func(data_pred2, 'avg_gold', 'mm4_avg_gold', 4, TRUE)

data_pred2 <- mm_func(data_pred2, 'rend', 'mm2_rend', 2, TRUE)
data_pred2 <- mm_func(data_pred2, 'rend', 'mm3_rend', 3, TRUE)
data_pred2 <- mm_func(data_pred2, 'rend', 'mm4_rend', 4, TRUE)
data_pred2 <- racha(data_pred2, 'avg_gold', 'rachas_sube_gold', FALSE)
data_pred2 <- racha(data_pred2, 'rez_avg_gold_1s', 'rachas_sube_gold_rez1d', FALSE)

data_pred2 %>%
  mutate(mes = as.factor(mes)) -> data_pred2

one_hot(as.data.table(data_pred2)) %>%
  as.data.frame -> data_pred2

data_pred2 %>%
  mutate(rez_avg_gold_1s_2 = rez_avg_gold_1s^2,
         rend_2 = rend^2,
         rend_3 = rend^3,
         rachas_sube_gold_2 = rachas_sube_gold^2,
         rez_rend_gold_2s_2 = rez_rend_gold_2s^2,
         rez_avg_gold_8s_2 = rez_avg_gold_8s^2) -> data_pred2
data_pred2 %>%
  na.omit() -> data_pred2

## Predicción segunda semana
sp2 <- predict(m2, tail(data_pred2,1))

## Predicción histórica usando el modelo 1, sólo se usará el modelo 2 para la semana n+2
p <- c(predict(m1, data_pred),
       predict(m2, tail(data_pred2,1)))

## Se agrega predicción a históricos
data_pred %>%
  select(max_date, avg_gold) %>%
  rbind(data.frame(max_date = data_pred2[,'max_date'] %>% tail(1) + 7, avg_gold = NA)) %>%
  mutate(pred = p) %>%
  mutate(avg_gold = lead(avg_gold)) %>%
  filter(max_date >= as.Date('2018-01-01')) %>%
  ggplot() +
  geom_line(aes(x = max_date, y = avg_gold, group = 1), colour = 'darkblue') +
  geom_line(aes(x = max_date, y = pred, group = 1), colour = 'darkgreen', size = 0.3) +
  theme_bw() +
  ggtitle('Promedio Precio Oro Semanal - Real vs Estimado Histórico') + 
  scale_y_continuous(labels = scales::comma) +
  xlab('Día') +
  ylab('Precio del Oro') +
  theme(axis.text.x = element_text(face = 'bold'),
        axis.text.y = element_text(face = 'bold')) -> obs_pred_plot
  # annotate(geom  = 'text',
  #          x     = as.Date('2019-01-15'),
  #          y     = 1310,
  #          label = 'Estimado',
  #          colour = 'darkgreen',
  #          hjust = 0.5) +
  # annotate(geom  = 'text',
  #          x     = as.Date('2019-01-15'),
  #          y     = 1315,
  #          label = 'Real',
  #          colour = 'darkblue',
  #          hjust = 0.5) 





t0 <- paste0('La semana del ', tail(data_pred$min_date,1), ' al ', tail(data_pred$max_date,1), ' tuvo un precio promedio de: $', tail(data_pred$avg_gold,1), ' dlls.')
t1 <- paste0('Para la próxima semana, se pronostica un valor promedio del oro de: $', round(sp,2), ' dlls.')
t2 <- paste0('Lo cuál indicaría una ', a, ' del ', round(r*100,2), '% respecto a esta semana.')
t3 <- paste0('Pero podría ', ai, ' de ', round(II*100,2), '%', ' a ', as, ' ', round(IS*100,2), '%.')
t4 <- paste0('La semana que sigue, se pronostica un valor promedio del oro de $', round(sp2,2), ' , pero podría variar entren $', round(sp2-rmse_valid*1.5,2), ' y $',  round(sp2+rmse_valid*1.5,2))

ruta <- '/Users/darias/Documents/GoldPrice/Notebook/Week_model/Automate_update_model/Imagenes'
fecha <- Sys.Date()
ggsave(filename = paste0('rmse_', fecha, '.png'), plot = rmse_plot, path = ruta, width = 5, height = 4)
ggsave(filename = paste0('errores_', fecha, '.png'), plot = errores_plot, path = ruta, width = 3, height = 3)
ggsave(filename = paste0('obs_pred_', fecha, '.png'), plot = obs_pred_plot, path = ruta, width = 7, height = 4)
ggsave(filename = paste0('coef_plot_', fecha, '.png'), plot = coef_plot, path = ruta, width = 7, height = 4)

# Correo

from <- 'construdiego@hotmail.com'
to <- 'darias@ezcorp.com'
subject <- 'Predicción oro'
body <- paste(t0, t1, t2, t3, t4, ' ')
data.frame(. = c(t0,t1,t2,t3,t4))
bodyWithAttachment <- list(body, mime_part(rmse_plot, 'RMSE'),
                           mime_part(errores_plot, 'Errores'),
                           mime_part(obs_pred_plot, 'Observado_vs_estimado'),
                           mime_part(coef_plot, 'Importancia de Variables'))
sendmail(from=from,to=to,subject=subject,msg=bodyWithAttachment)

to <- 'Jose_Aguilar@ezcorp.com'
sendmail(from=from,to=to,subject=subject,msg=bodyWithAttachment)

to <- 'Alejandro_Gonzalez@ezcorp.com'
sendmail(from=from,to=to,subject=subject,msg=bodyWithAttachment)

to <- 'LuisDavid_Valdez@ezcorp.com'
sendmail(from=from,to=to,subject=subject,msg=bodyWithAttachment)

to <- 'Fabiola_Sosa@ezcorp.com'
sendmail(from=from,to=to,subject=subject,msg=bodyWithAttachment)