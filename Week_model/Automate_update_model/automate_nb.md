
# Modelo automatizado

Ya que se hizo un modelo para predecir el promedio del valor semanal del oro, descargando los datos y leyéndolos en CSV, en esta versión se descargarán los datos directamente de un API de la página **QUANDL**, por medio del paquete de R del mismo nombre.

# Librerías


```R
library(dplyr)
library(ggplot2)
library(readr)
library(forcats)
library(repr)
library(mltools)
library(data.table)
library(rvest)
library(stringi)
library(zoo)
library(PMwR)
library(Quandl)
```

    Warning message:
    “package ‘dplyr’ was built under R version 3.5.2”
    Attaching package: ‘dplyr’
    
    The following objects are masked from ‘package:stats’:
    
        filter, lag
    
    The following objects are masked from ‘package:base’:
    
        intersect, setdiff, setequal, union
    
    Warning message:
    “package ‘repr’ was built under R version 3.5.2”Warning message:
    “package ‘data.table’ was built under R version 3.5.2”
    Attaching package: ‘data.table’
    
    The following objects are masked from ‘package:dplyr’:
    
        between, first, last
    
    Loading required package: xml2
    
    Attaching package: ‘rvest’
    
    The following object is masked from ‘package:readr’:
    
        guess_encoding
    
    Warning message:
    “package ‘stringi’ was built under R version 3.5.2”Warning message:
    “package ‘zoo’ was built under R version 3.5.2”
    Attaching package: ‘zoo’
    
    The following objects are masked from ‘package:base’:
    
        as.Date, as.Date.numeric
    
    Warning message:
    “package ‘PMwR’ was built under R version 3.5.2”Loading required package: xts
    
    Attaching package: ‘xts’
    
    The following objects are masked from ‘package:data.table’:
    
        first, last
    
    The following objects are masked from ‘package:dplyr’:
    
        first, last
    


# Lectura de datos

## Oro


```R
gold_price <- Quandl("WGC/GOLD_DAILY_USD", api_key="xCLj7WmfLsvdi9xo2okv")
names(gold_price) <- c('date', 'goldprice')
```

## Dólar


```R
dolar_price <- Quandl("BDM/SF60653", api_key="xCLj7WmfLsvdi9xo2okv")
names(dolar_price) <- c('date', 'dolarprice')
```

# Homologación de datos

Se dejarán ambos data frames con la máxima fecha del oro y la mínima del dólar


```R
max_date <- max(gold_price$date)
min_date <- min(dolar_price$date)

gold_price %>%
 filter(date >= min_date) %>%
 filter(date <= max_date) -> gold_price

dolar_price %>%
 filter(date >= min_date) %>%
 filter(date <= max_date) -> dolar_price
```

# Join data

Se unen los datos en un sólo _data set_, tomando como referencia el precio del oro


```R
dolar_price %>%
 left_join(gold_price, by = 'date') -> data
```

# Limpiar datos

Se quitan datos con NA's y se dejan datos a partir del 2010


```R
data %>%
 na.omit() -> data
```


```R
data %>%
 filter(date >= as.Date('2010-01-01')) -> data
```

# Ingeniería de datos

Se creará:
 - Promedio semanal del precio del oro
 - Desviación estandar semanal del precio del oro
 - Log Rendimientos
 - Rezagos
 - Medias Móviles
 - Rachas
 - Dummies de mes
 - Dummies de número de semana del mes

## Promedio semanal

Se creará el promedio semanal del precio del oro.


```R
data %>%
 mutate(week = paste0(year(date),'-',format(date, '%W'))) %>%
 group_by(week) %>%
 summarise(min_date = min(date),
           max_date = max(date),
           avg_gold = mean(goldprice, na.rm = TRUE),
           #avg_dol  = mean(dolarprice, na.rm = TRUE),
           std_gold = sd(goldprice, na.rm = TRUE)) %>%
 arrange(as.numeric(gsub('-','',week))) %>%
 filter(!is.na(std_gold)) %>%
 mutate(mes = month(max_date)) %>%
 mutate(mes = ifelse(mes == 1, 'Ene', mes),
        mes = ifelse(mes == 2, 'Feb', mes),
        mes = ifelse(mes == 3, 'Mar', mes),
        mes = ifelse(mes == 4, 'Abr', mes),
        mes = ifelse(mes == 5, 'May', mes),
        mes = ifelse(mes == 6, 'Jun', mes),
        mes = ifelse(mes == 7, 'Jul', mes),
        mes = ifelse(mes == 8, 'Ago', mes),
        mes = ifelse(mes == 9, 'Sep', mes),
        mes = ifelse(mes == 10, 'Oct', mes),
        mes = ifelse(mes == 11, 'Nov', mes),
        mes = ifelse(mes == 12, 'Dic', mes)) -> data
```


```R
head(data,3)
```


<table>
<thead><tr><th scope=col>week</th><th scope=col>min_date</th><th scope=col>max_date</th><th scope=col>avg_gold</th><th scope=col>std_gold</th><th scope=col>mes</th></tr></thead>
<tbody>
	<tr><td>2010-01   </td><td>2010-01-04</td><td>2010-01-08</td><td>1126.35   </td><td> 3.931444 </td><td>Ene       </td></tr>
	<tr><td>2010-02   </td><td>2010-01-11</td><td>2010-01-15</td><td>1139.55   </td><td>12.289986 </td><td>Ene       </td></tr>
	<tr><td>2010-03   </td><td>2010-01-18</td><td>2010-01-22</td><td>1116.00   </td><td>20.816910 </td><td>Ene       </td></tr>
</tbody>
</table>



## Rendimientos

Se obtendrá el _log rendimiento_ del oro, como una variable del modelo.

$$Rend_i = \ln{\frac{PrecioOro_{t+1}}{PrecioOro_{t}}}$$

### Función

Se crea función que agrega una columna con los rendimientos del oro.

Esta función tiene un indicador, el cual funciona para saber si se agrega a un _Data frame_ para entrenar un modelo o para hacer una predicción.

Si es para hacer una predicción, se hace con un _lag_ menos, ya que en una predicción se busca usar los datos más recientes para hacer una predicción a futuro.


```R
rend_func <- function(df, col_ref, nom_col_nueva, ind_train){
  
  # Para predict se quita un lag
  
  if(ind_train == TRUE){
    df %>%
      mutate_(aux1 = col_ref) %>%
      mutate(aux2  = lag(aux1)) %>%
      mutate(aux3  = log(aux2/lag(aux2))) %>%
      select(-aux1, -aux2) -> d 
  } else{
    df %>%
      mutate_(aux1 = col_ref) %>%
      mutate(aux2  = aux1) %>%
      mutate(aux3  = log(aux1/lag(aux2))) %>%
      select(-aux1, -aux2) -> d
  }
  
  names(d)[which(names(d) == 'aux3')] <- nom_col_nueva
  
  return(d)
}
```

### Se agregan rendimientos

Se aplica la función para agregar rendimientos


```R
data <- rend_func(data, 'avg_gold', 'rend', ind_train = TRUE)
#data <- rend_func(data, 'avg_dol', 'rend_dol', ind_train = TRUE)
```

    Warning message:
    “mutate_() is deprecated. 
    Please use mutate() instead
    
    The 'programming' vignette or the tidyeval book can help you
    to program with mutate() : https://tidyeval.tidyverse.org
    [90mThis warning is displayed once per session.[39m”

## Rezagos

Se agregarán rezagos de:
 - Promedio del precio del oro
 - Desviación estandar del precio del oro
 - Rendimientos del oro
 
A 1,2,3,4,6 y 8 semanas

Se agregará rezafo de 1 semana para el mes

### Función

Se creará una función que agregar una columna con los rezagos a _X_ semanas de una columnas específica.

Tendrá la misma consideración del indicador de si es una _data frame_ para entrenar o para predecir


```R
rez_func <- function(df, col_ref, nom_col_nueva, num_rezagos, ind_train){
  # Cuando es para predict, se quita un lag
  if(ind_train == TRUE){
    # Se crea vector con código de número de lags a hacer y sus paréntesis de cierre para train
    num_lags  <- rep('lag(',num_rezagos)
    close_par <- rep(')', num_rezagos)
  } else {
    # Se crea vector con código de número de lags a hacer y sus paréntesis de cierre para predict
    num_lags  <- rep('lag(',num_rezagos-1)
    close_par <- rep(')', num_rezagos-1)
  }
  
  # Se convierten a un sólo caracter
  num_lags  <- stri_paste(num_lags, collapse = '')
  close_par <- stri_paste(close_par, collapse = '')
  
  # Se pega el código
  cod <- paste0(num_lags, 'aux', close_par)
  
  # Se crea la columna nueva
  df %>%
    mutate_(aux = col_ref) %>%
    mutate_(aux = cod) -> d
  
  names(d)[which(names(d) == 'aux')] <- nom_col_nueva
  
  return(d)
}
```

### Se agregan rezagos


```R
data <- rez_func(data, 'avg_gold', 'rez_avg_gold_1s',  1,  TRUE)
data <- rez_func(data, 'avg_gold', 'rez_avg_gold_2s',  2,  TRUE)
data <- rez_func(data, 'avg_gold', 'rez_avg_gold_3s',  3,  TRUE)
data <- rez_func(data, 'avg_gold', 'rez_avg_gold_4s',  4,  TRUE)
data <- rez_func(data, 'avg_gold', 'rez_avg_gold_6s',  6,  TRUE)
data <- rez_func(data, 'avg_gold', 'rez_avg_gold_8s',  8,  TRUE)
```


```R
data <- rez_func(data, 'std_gold', 'rez_std_gold_1s',  1,  TRUE)
data <- rez_func(data, 'std_gold', 'rez_std_gold_2s',  2,  TRUE)
data <- rez_func(data, 'std_gold', 'rez_std_gold_3s',  3,  TRUE)
data <- rez_func(data, 'std_gold', 'rez_std_gold_4s',  4,  TRUE)
data <- rez_func(data, 'std_gold', 'rez_std_gold_6s',  6,  TRUE)
data <- rez_func(data, 'std_gold', 'rez_std_gold_8s',  8,  TRUE)
```


```R
data <- rez_func(data, 'rend', 'rez_rend_gold_1s',  1,  TRUE)
data <- rez_func(data, 'rend', 'rez_rend_gold_2s',  2,  TRUE)
data <- rez_func(data, 'rend', 'rez_rend_gold_3s',  3,  TRUE)
data <- rez_func(data, 'rend', 'rez_rend_gold_4s',  4,  TRUE)
data <- rez_func(data, 'rend', 'rez_rend_gold_6s',  6,  TRUE)
data <- rez_func(data, 'rend', 'rez_rend_gold_8s',  8,  TRUE)
```


```R
#data <- rez_func(data, 'avg_dol', 'rez_avg_dol_1s',  1,  TRUE)
#data <- rez_func(data, 'avg_dol', 'rez_avg_dol_2s',  2,  TRUE)
#data <- rez_func(data, 'avg_dol', 'rez_avg_dol_3s',  3,  TRUE)
#data <- rez_func(data, 'avg_dol', 'rez_avg_dol_4s',  4,  TRUE)
#data <- rez_func(data, 'avg_dol', 'rez_avg_dol_6s',  6,  TRUE)
#data <- rez_func(data, 'avg_dol', 'rez_avg_dol_8s',  8,  TRUE)

#data <- rez_func(data, 'rend_dol', 'rez_rend_dol_1s',  1,  TRUE)
#data <- rez_func(data, 'rend_dol', 'rez_rend_dol_2s',  2,  TRUE)
#data <- rez_func(data, 'rend_dol', 'rez_rend_dol_3s',  3,  TRUE)
#data <- rez_func(data, 'rend_dol', 'rez_rend_dol_4s',  4,  TRUE)
#data <- rez_func(data, 'rend_dol', 'rez_rend_dol_6s',  6,  TRUE)
#data <- rez_func(data, 'rend_dol', 'rez_rend_dol_8s',  8,  TRUE)
```


```R
data %>%
 mutate(mes = lag(mes)) -> data
```

## Medias Móviles

Se obtendrán las medias móviles a diferentes tiempos para:
 - Promedio de precio de oro
 - Desviación estandar del precio del oro
 - Rendimiento del oro
 
Con el indicador si para _train_ o _predict_ el _Data frame_

Media Móvil de una variable de _T_ tiempos

$$MediaMóvil_i = \frac{1}{n}\sum_{i=1}^{n} Variable_{t-i}$$

### Función


```R
mm_func <- function(df, col_ref, nom_col_nueva, t, ind_train){
  # Cuando es para predict se quita un lag para calcular las medias móviles
  if(ind_train == TRUE){
    # Se crea el vector de medias móviles a "t" tiempos para train
    df %>%
      mutate_(aux = col_ref) %>%
      mutate(aux = lag(aux)) %>%
      select(aux) %>%
      pull() %>%
      rollmean(x = ., k = t) -> mm
  } else{
    # Se crea el vector de medias móviles a "t" tiempos para predict
    df %>%
      mutate_(aux = col_ref) %>%
      mutate(aux = aux) %>%
      select(aux) %>%
      pull() %>%
      rollmean(x = ., k = t) -> mm
    
  }
  
  # Para poder agregar la columna, se deben agregar NA's al principio para que coincida el número de columnas
  mm <- c(rep(NA,t-1), mm)
  
  # Se agrega el vector de medias móviles al df
  df %>%
    mutate(aux = mm) -> d
  
  names(d)[which(names(d) == 'aux')] <- nom_col_nueva
  
  return(d)
}
```

### Se agregan Medias Móviles


```R
data <- mm_func(data, 'avg_gold', 'mm2_avg_gold', 2, TRUE)
data <- mm_func(data, 'avg_gold', 'mm3_avg_gold', 3, TRUE)
data <- mm_func(data, 'avg_gold', 'mm4_avg_gold', 4, TRUE)

data <- mm_func(data, 'std_gold', 'mm2_std_gold', 2, TRUE)
data <- mm_func(data, 'std_gold', 'mm3_std_gold', 3, TRUE)
data <- mm_func(data, 'std_gold', 'mm4_std_gold', 4, TRUE)

data <- mm_func(data, 'rend', 'mm2_rend', 2, TRUE)
data <- mm_func(data, 'rend', 'mm3_rend', 3, TRUE)
data <- mm_func(data, 'rend', 'mm4_rend', 4, TRUE)

#data <- mm_func(data, 'rend_dol', 'mm2_rend_dol', 2, TRUE)
#data <- mm_func(data, 'rend_dol', 'mm3_rend_dol', 3, TRUE)
#data <- mm_func(data, 'rend_dol', 'mm4_rend_dol', 4, TRUE)
```

## Rachas

Se hará una función que obtenga las rachas de rendimiento positivo, que agregue una columna con el número de semanas que ha subido el dólar de manera constante.

### Función


```R
racha <- function(df, col_ref, nom_col_nueva, ind_train){
  # Cuando es para predict se quita un lag para calcular las medias móviles
  if(ind_train == TRUE){
    df %>%
      mutate_(aux1 = col_ref) %>%
      mutate(aux2  = lag(aux1)) %>%
      mutate(aux3  = aux2-lag(aux2)) %>%
      mutate(ind = ifelse(aux3 <= 0, FALSE, TRUE)) %>%
      group_by(grp = rleid(ind)) %>%
      mutate(n = rank(min_date)) %>%
      mutate(n = ifelse(ind == FALSE, 0, n)) %>%
      data.frame() %>%
      mutate(racha = lag(n)) %>%
      select(-aux1, -aux2, -aux3, -n, -grp, -ind) -> d
  } else {
      df %>%
       mutate_(aux1 = col_ref) %>%
       mutate(aux2  = aux1) %>%
       mutate(aux3  = aux1-lag(aux2)) %>%
       mutate(ind = ifelse(aux3 <= 0, FALSE, TRUE)) %>%
       group_by(grp = rleid(ind)) %>%
       mutate(n = rank(min_date)) %>%
       mutate(n = ifelse(ind == FALSE, 0, n)) %>%
       data.frame() %>%
       mutate(racha = lag(n)) %>%
       select(-aux1, -aux2, -aux3, -n, -grp, -ind) -> d 
  }

    names(d)[which(names(d) == 'racha')] <- nom_col_nueva
    
    return(d)
    
}
```

### Se agregan rachas


```R
data <- racha(data, 'avg_gold', 'rachas_sube_gold', TRUE)
data <- racha(data, 'rez_avg_gold_1s', 'rachas_sube_gold_rez1d', TRUE)
data <- racha(data, 'rez_std_gold_1s', 'rachas_sube_stdgold_rez1d', TRUE)
#data <- racha(data, 'avg_dol', 'racha_avg_dol', TRUE)
#data <- racha(data, 'rend_dol', 'racha_rend_dol', TRUE)
```

## Dummies mes

Se agregan dummies de mes del año


```R
data %>%
 mutate(mes = as.factor(mes)) -> data

one_hot(as.data.table(data)) %>%
 as.data.frame -> data
```

Se quita una dummie para que la matriz sea invertible


```R
data$mes_Ene <- NULL
```

## Polinomios

Se agrega el rezago de 1s al cuadrado para:
 - Promedio semanal del Precio del oro
 - Desviación estanmdar del Precio del oro


```R
data %>%
 mutate(rez_avg_gold_1s_2 = rez_avg_gold_1s^2,
        rend_2 = rend^2
        #rend_3 = rend^3, rachas_sube_gold_2 = rachas_sube_gold^2
        ) -> data
```

## Se quitan NAs


```R
data %>%
 na.omit() -> data
```

# Descriptivos

## Serie de tiempo histórica


```R
options(repr.plot.width=7, repr.plot.height=4)
data %>%
 ggplot() +
 geom_line(aes(x = max_date, y = avg_gold, group = 1), colour = 'darkblue') +
 theme_bw() +
 ggtitle('Precio del oro - Promedio Semanal') +
 theme(axis.text.x = element_text(face = 'bold'),
       axis.text.y = element_text(face = 'bold')) +
 scale_y_continuous(labels = scales::comma) +
 xlab('semana') +
 ylab('$ dlls')
```


![png](output_72_0.png)


## Correlaciones

Se obtendrán las correlaciones entre las variables de medias móviles, rezagos y rendimientos


```R
options(repr.plot.width=8, repr.plot.height=8)
data %>%
  select(-grep('mes', names(data))) %>%
 select(-week, -min_date, -max_date, -std_gold) %>%
 cor() %>%
 heatmap(main = 'Heatmap - Correlaciones')
```


![png](output_75_0.png)


## Promedio del oro vs Rezagos precio oro


```R
options(repr.plot.width=5, repr.plot.height=4)
data.frame(avg_gold = rep(data$avg_gold,6),
           rezago   = c(data$rez_avg_gold_1s, data$rez_avg_gold_2s, data$rez_avg_gold_3s,
                        data$rez_avg_gold_4s, data$rez_avg_gold_6s, data$rez_avg_gold_8s),
           num_rezago = c(rep(1, nrow(data)), rep(2, nrow(data)), rep(3, nrow(data)),
                          rep(4, nrow(data)), rep(6, nrow(data)), rep(8, nrow(data)))) %>%
 ggplot() +
 geom_point(aes(x = avg_gold, y = rezago), colour = 'darkblue') +
 theme_bw() +
 ggtitle('Scatterplots - Rezagos') +
 facet_wrap(~num_rezago) +
 geom_smooth(aes(x = avg_gold, y = rezago))
```

    `geom_smooth()` using method = 'loess' and formula 'y ~ x'



![png](output_77_1.png)


## Promedio Oro vs Rendimientos


```R
options(repr.plot.width=5, repr.plot.height=4)
data.frame(avg_gold = rep(data$avg_gold,6),
           rezago   = c(data$rend, data$rez_rend_gold_1s, data$rez_rend_gold_2s,
                        data$rez_rend_gold_3s, data$rez_rend_gold_4s, data$rez_rend_gold_6s),
           num_rezago = c(rep(1, nrow(data)), rep(2, nrow(data)), rep(3, nrow(data)),
                          rep(4, nrow(data)), rep(6, nrow(data)), rep(8, nrow(data)))) %>%
 ggplot() +
 geom_point(aes(x = avg_gold, y = rezago), colour = 'darkblue') +
 theme_bw() +
 ggtitle('Scatterplots - Rendimientos') +
 facet_wrap(~num_rezago) +
 geom_smooth(aes(x = avg_gold, y = rezago))
```

    `geom_smooth()` using method = 'loess' and formula 'y ~ x'



![png](output_79_1.png)


# Modelo

Se hará un modelo de regresión lineal para estimar el valor del oro

$$PromedioPrecioOro_i = \alpha + \eta_1*MM2Oro_i + \eta_2*MM3Oro_i + \eta_3*MM4Oro_i + \eta_4*MM2RO_i + \eta_5*MM3RO_i + \eta_6*MM4RO_i + \eta_7*MM2SO_i + \eta8*MM3SO_i + \eta_9*MM4SO_i + \sum_j^{j\in\{1,2,3,4,6,8\}} \beta_j*RO_{j_i} + \sum_j^{j\in\{1,2,3,4,6,8\}} \delta_j*RezagoOro_{j_i} + \sum_j^{j\in\{1,2,3,4,6,8\}} \delta_j*RezagoSO_{j_i} + \sigma_j*DummieMes_{j_i} + \phi_1*RachaPO_i + \phi_2*RachaRO1_i + \phi_3*RachaRSD_i + \epsilon_i$$

Dónde:
 - **RO:** Rendimiento del oro
 - **MMi:** Media Móvile a "i" unidades de tiempo
 - **SO:** Desviación estándard del oro

Posteriormente se hará una optimización _stepwise_

## Fit

Se ajusta la regresión lineal


```R
set.seed(50)
indices <- sample(1:nrow(data), nrow(data)*0.8)
train <- data[indices,]
valid <- data[-indices,]
```


```R
m1 <- lm(avg_gold ~ ., data = train %>% select(-week, -min_date, -max_date, -std_gold))
```

### Stepwise


```R
m1 <- step(m1)
```

    Start:  AIC=2454.93
    avg_gold ~ mes_Abr + mes_Ago + mes_Dic + mes_Feb + mes_Jul + 
        mes_Jun + mes_Mar + mes_May + mes_Nov + mes_Oct + mes_Sep + 
        rend + rez_avg_gold_1s + rez_avg_gold_2s + rez_avg_gold_3s + 
        rez_avg_gold_4s + rez_avg_gold_6s + rez_avg_gold_8s + rez_std_gold_1s + 
        rez_std_gold_2s + rez_std_gold_3s + rez_std_gold_4s + rez_std_gold_6s + 
        rez_std_gold_8s + rez_rend_gold_1s + rez_rend_gold_2s + rez_rend_gold_3s + 
        rez_rend_gold_4s + rez_rend_gold_6s + rez_rend_gold_8s + 
        mm2_avg_gold + mm3_avg_gold + mm4_avg_gold + mm2_std_gold + 
        mm3_std_gold + mm4_std_gold + mm2_rend + mm3_rend + mm4_rend + 
        rachas_sube_gold + rachas_sube_gold_rez1d + rachas_sube_stdgold_rez1d + 
        rez_avg_gold_1s_2 + rend_2
    
    
    Step:  AIC=2454.93
    avg_gold ~ mes_Abr + mes_Ago + mes_Dic + mes_Feb + mes_Jul + 
        mes_Jun + mes_Mar + mes_May + mes_Nov + mes_Oct + mes_Sep + 
        rend + rez_avg_gold_1s + rez_avg_gold_2s + rez_avg_gold_3s + 
        rez_avg_gold_4s + rez_avg_gold_6s + rez_avg_gold_8s + rez_std_gold_1s + 
        rez_std_gold_2s + rez_std_gold_3s + rez_std_gold_4s + rez_std_gold_6s + 
        rez_std_gold_8s + rez_rend_gold_1s + rez_rend_gold_2s + rez_rend_gold_3s + 
        rez_rend_gold_4s + rez_rend_gold_6s + rez_rend_gold_8s + 
        mm2_avg_gold + mm3_avg_gold + mm4_avg_gold + mm2_std_gold + 
        mm3_std_gold + mm4_std_gold + mm2_rend + mm3_rend + rachas_sube_gold + 
        rachas_sube_gold_rez1d + rachas_sube_stdgold_rez1d + rez_avg_gold_1s_2 + 
        rend_2
    
    
    Step:  AIC=2454.93
    avg_gold ~ mes_Abr + mes_Ago + mes_Dic + mes_Feb + mes_Jul + 
        mes_Jun + mes_Mar + mes_May + mes_Nov + mes_Oct + mes_Sep + 
        rend + rez_avg_gold_1s + rez_avg_gold_2s + rez_avg_gold_3s + 
        rez_avg_gold_4s + rez_avg_gold_6s + rez_avg_gold_8s + rez_std_gold_1s + 
        rez_std_gold_2s + rez_std_gold_3s + rez_std_gold_4s + rez_std_gold_6s + 
        rez_std_gold_8s + rez_rend_gold_1s + rez_rend_gold_2s + rez_rend_gold_3s + 
        rez_rend_gold_4s + rez_rend_gold_6s + rez_rend_gold_8s + 
        mm2_avg_gold + mm3_avg_gold + mm4_avg_gold + mm2_std_gold + 
        mm3_std_gold + mm4_std_gold + mm2_rend + rachas_sube_gold + 
        rachas_sube_gold_rez1d + rachas_sube_stdgold_rez1d + rez_avg_gold_1s_2 + 
        rend_2
    
    
    Step:  AIC=2454.93
    avg_gold ~ mes_Abr + mes_Ago + mes_Dic + mes_Feb + mes_Jul + 
        mes_Jun + mes_Mar + mes_May + mes_Nov + mes_Oct + mes_Sep + 
        rend + rez_avg_gold_1s + rez_avg_gold_2s + rez_avg_gold_3s + 
        rez_avg_gold_4s + rez_avg_gold_6s + rez_avg_gold_8s + rez_std_gold_1s + 
        rez_std_gold_2s + rez_std_gold_3s + rez_std_gold_4s + rez_std_gold_6s + 
        rez_std_gold_8s + rez_rend_gold_1s + rez_rend_gold_2s + rez_rend_gold_3s + 
        rez_rend_gold_4s + rez_rend_gold_6s + rez_rend_gold_8s + 
        mm2_avg_gold + mm3_avg_gold + mm4_avg_gold + mm2_std_gold + 
        mm3_std_gold + mm4_std_gold + rachas_sube_gold + rachas_sube_gold_rez1d + 
        rachas_sube_stdgold_rez1d + rez_avg_gold_1s_2 + rend_2
    
    
    Step:  AIC=2454.93
    avg_gold ~ mes_Abr + mes_Ago + mes_Dic + mes_Feb + mes_Jul + 
        mes_Jun + mes_Mar + mes_May + mes_Nov + mes_Oct + mes_Sep + 
        rend + rez_avg_gold_1s + rez_avg_gold_2s + rez_avg_gold_3s + 
        rez_avg_gold_4s + rez_avg_gold_6s + rez_avg_gold_8s + rez_std_gold_1s + 
        rez_std_gold_2s + rez_std_gold_3s + rez_std_gold_4s + rez_std_gold_6s + 
        rez_std_gold_8s + rez_rend_gold_1s + rez_rend_gold_2s + rez_rend_gold_3s + 
        rez_rend_gold_4s + rez_rend_gold_6s + rez_rend_gold_8s + 
        mm2_avg_gold + mm3_avg_gold + mm4_avg_gold + mm2_std_gold + 
        mm3_std_gold + rachas_sube_gold + rachas_sube_gold_rez1d + 
        rachas_sube_stdgold_rez1d + rez_avg_gold_1s_2 + rend_2
    
    
    Step:  AIC=2454.93
    avg_gold ~ mes_Abr + mes_Ago + mes_Dic + mes_Feb + mes_Jul + 
        mes_Jun + mes_Mar + mes_May + mes_Nov + mes_Oct + mes_Sep + 
        rend + rez_avg_gold_1s + rez_avg_gold_2s + rez_avg_gold_3s + 
        rez_avg_gold_4s + rez_avg_gold_6s + rez_avg_gold_8s + rez_std_gold_1s + 
        rez_std_gold_2s + rez_std_gold_3s + rez_std_gold_4s + rez_std_gold_6s + 
        rez_std_gold_8s + rez_rend_gold_1s + rez_rend_gold_2s + rez_rend_gold_3s + 
        rez_rend_gold_4s + rez_rend_gold_6s + rez_rend_gold_8s + 
        mm2_avg_gold + mm3_avg_gold + mm4_avg_gold + mm2_std_gold + 
        rachas_sube_gold + rachas_sube_gold_rez1d + rachas_sube_stdgold_rez1d + 
        rez_avg_gold_1s_2 + rend_2
    
    
    Step:  AIC=2454.93
    avg_gold ~ mes_Abr + mes_Ago + mes_Dic + mes_Feb + mes_Jul + 
        mes_Jun + mes_Mar + mes_May + mes_Nov + mes_Oct + mes_Sep + 
        rend + rez_avg_gold_1s + rez_avg_gold_2s + rez_avg_gold_3s + 
        rez_avg_gold_4s + rez_avg_gold_6s + rez_avg_gold_8s + rez_std_gold_1s + 
        rez_std_gold_2s + rez_std_gold_3s + rez_std_gold_4s + rez_std_gold_6s + 
        rez_std_gold_8s + rez_rend_gold_1s + rez_rend_gold_2s + rez_rend_gold_3s + 
        rez_rend_gold_4s + rez_rend_gold_6s + rez_rend_gold_8s + 
        mm2_avg_gold + mm3_avg_gold + mm4_avg_gold + rachas_sube_gold + 
        rachas_sube_gold_rez1d + rachas_sube_stdgold_rez1d + rez_avg_gold_1s_2 + 
        rend_2
    
    
    Step:  AIC=2454.93
    avg_gold ~ mes_Abr + mes_Ago + mes_Dic + mes_Feb + mes_Jul + 
        mes_Jun + mes_Mar + mes_May + mes_Nov + mes_Oct + mes_Sep + 
        rend + rez_avg_gold_1s + rez_avg_gold_2s + rez_avg_gold_3s + 
        rez_avg_gold_4s + rez_avg_gold_6s + rez_avg_gold_8s + rez_std_gold_1s + 
        rez_std_gold_2s + rez_std_gold_3s + rez_std_gold_4s + rez_std_gold_6s + 
        rez_std_gold_8s + rez_rend_gold_1s + rez_rend_gold_2s + rez_rend_gold_3s + 
        rez_rend_gold_4s + rez_rend_gold_6s + rez_rend_gold_8s + 
        mm2_avg_gold + mm3_avg_gold + rachas_sube_gold + rachas_sube_gold_rez1d + 
        rachas_sube_stdgold_rez1d + rez_avg_gold_1s_2 + rend_2
    
    
    Step:  AIC=2454.93
    avg_gold ~ mes_Abr + mes_Ago + mes_Dic + mes_Feb + mes_Jul + 
        mes_Jun + mes_Mar + mes_May + mes_Nov + mes_Oct + mes_Sep + 
        rend + rez_avg_gold_1s + rez_avg_gold_2s + rez_avg_gold_3s + 
        rez_avg_gold_4s + rez_avg_gold_6s + rez_avg_gold_8s + rez_std_gold_1s + 
        rez_std_gold_2s + rez_std_gold_3s + rez_std_gold_4s + rez_std_gold_6s + 
        rez_std_gold_8s + rez_rend_gold_1s + rez_rend_gold_2s + rez_rend_gold_3s + 
        rez_rend_gold_4s + rez_rend_gold_6s + rez_rend_gold_8s + 
        mm2_avg_gold + rachas_sube_gold + rachas_sube_gold_rez1d + 
        rachas_sube_stdgold_rez1d + rez_avg_gold_1s_2 + rend_2
    
    
    Step:  AIC=2454.93
    avg_gold ~ mes_Abr + mes_Ago + mes_Dic + mes_Feb + mes_Jul + 
        mes_Jun + mes_Mar + mes_May + mes_Nov + mes_Oct + mes_Sep + 
        rend + rez_avg_gold_1s + rez_avg_gold_2s + rez_avg_gold_3s + 
        rez_avg_gold_4s + rez_avg_gold_6s + rez_avg_gold_8s + rez_std_gold_1s + 
        rez_std_gold_2s + rez_std_gold_3s + rez_std_gold_4s + rez_std_gold_6s + 
        rez_std_gold_8s + rez_rend_gold_1s + rez_rend_gold_2s + rez_rend_gold_3s + 
        rez_rend_gold_4s + rez_rend_gold_6s + rez_rend_gold_8s + 
        rachas_sube_gold + rachas_sube_gold_rez1d + rachas_sube_stdgold_rez1d + 
        rez_avg_gold_1s_2 + rend_2
    
                                Df Sum of Sq    RSS    AIC
    - rend                       1       0.2 192873 2452.9
    - rez_std_gold_8s            1       2.2 192875 2452.9
    - rez_std_gold_3s            1       8.5 192881 2452.9
    - mes_Ago                    1      21.4 192894 2453.0
    - rachas_sube_gold           1      47.4 192920 2453.0
    - rez_rend_gold_1s           1      55.9 192929 2453.0
    - rez_avg_gold_2s            1      81.1 192954 2453.1
    - mes_Feb                    1      98.0 192971 2453.1
    - rez_rend_gold_8s           1     185.1 193058 2453.3
    - rez_rend_gold_6s           1     209.9 193083 2453.3
    - rez_avg_gold_6s            1     213.7 193087 2453.4
    - mes_Oct                    1     253.7 193127 2453.4
    - rez_avg_gold_3s            1     322.7 193196 2453.6
    - rachas_sube_gold_rez1d     1     441.4 193314 2453.8
    - rez_avg_gold_1s_2          1     499.0 193372 2453.9
    - rez_rend_gold_4s           1     518.2 193391 2454.0
    - rez_rend_gold_3s           1     538.7 193412 2454.0
    - rez_rend_gold_2s           1     794.7 193668 2454.5
    - rez_std_gold_2s            1     831.0 193704 2454.6
    - rez_avg_gold_8s            1     998.1 193871 2454.9
    - rez_std_gold_4s            1    1005.7 193879 2454.9
    <none>                                   192873 2454.9
    - mes_Jul                    1    1064.9 193938 2455.0
    - mes_Abr                    1    1176.8 194050 2455.3
    - mes_Dic                    1    1229.4 194102 2455.4
    - rez_avg_gold_4s            1    1253.5 194126 2455.4
    - mes_Jun                    1    1385.1 194258 2455.7
    - rachas_sube_stdgold_rez1d  1    1690.7 194564 2456.3
    - rez_std_gold_6s            1    1782.9 194656 2456.5
    - mes_Nov                    1    2268.9 195142 2457.4
    - mes_Mar                    1    3046.5 195920 2458.9
    - rez_std_gold_1s            1    3448.3 196321 2459.7
    - mes_May                    1    3767.2 196640 2460.3
    - mes_Sep                    1    3995.0 196868 2460.8
    - rez_avg_gold_1s            1    6202.6 199076 2465.1
    - rend_2                     1   11563.8 204437 2475.2
    
    Step:  AIC=2452.93
    avg_gold ~ mes_Abr + mes_Ago + mes_Dic + mes_Feb + mes_Jul + 
        mes_Jun + mes_Mar + mes_May + mes_Nov + mes_Oct + mes_Sep + 
        rez_avg_gold_1s + rez_avg_gold_2s + rez_avg_gold_3s + rez_avg_gold_4s + 
        rez_avg_gold_6s + rez_avg_gold_8s + rez_std_gold_1s + rez_std_gold_2s + 
        rez_std_gold_3s + rez_std_gold_4s + rez_std_gold_6s + rez_std_gold_8s + 
        rez_rend_gold_1s + rez_rend_gold_2s + rez_rend_gold_3s + 
        rez_rend_gold_4s + rez_rend_gold_6s + rez_rend_gold_8s + 
        rachas_sube_gold + rachas_sube_gold_rez1d + rachas_sube_stdgold_rez1d + 
        rez_avg_gold_1s_2 + rend_2
    
                                Df Sum of Sq    RSS    AIC
    - rez_std_gold_8s            1         2 192876 2450.9
    - rez_std_gold_3s            1         8 192882 2450.9
    - mes_Ago                    1        21 192894 2451.0
    - rachas_sube_gold           1        48 192921 2451.0
    - rez_rend_gold_1s           1        58 192931 2451.1
    - mes_Feb                    1        98 192971 2451.1
    - rez_rend_gold_8s           1       185 193058 2451.3
    - rez_rend_gold_6s           1       211 193084 2451.3
    - rez_avg_gold_6s            1       213 193087 2451.4
    - rez_avg_gold_2s            1       221 193094 2451.4
    - mes_Oct                    1       256 193129 2451.4
    - rez_avg_gold_3s            1       338 193212 2451.6
    - rachas_sube_gold_rez1d     1       446 193319 2451.8
    - rez_avg_gold_1s_2          1       501 193375 2451.9
    - rez_rend_gold_4s           1       518 193391 2452.0
    - rez_rend_gold_3s           1       539 193413 2452.0
    - rez_rend_gold_2s           1       796 193669 2452.5
    - rez_std_gold_2s            1       835 193708 2452.6
    - rez_avg_gold_8s            1      1004 193877 2452.9
    <none>                                   192873 2452.9
    - rez_std_gold_4s            1      1036 193909 2453.0
    - mes_Jul                    1      1079 193953 2453.1
    - mes_Abr                    1      1179 194052 2453.3
    - mes_Dic                    1      1229 194103 2453.4
    - rez_avg_gold_4s            1      1253 194126 2453.4
    - mes_Jun                    1      1392 194265 2453.7
    - rachas_sube_stdgold_rez1d  1      1691 194564 2454.3
    - rez_std_gold_6s            1      1814 194687 2454.5
    - mes_Nov                    1      2276 195149 2455.4
    - mes_Mar                    1      3052 195925 2456.9
    - rez_std_gold_1s            1      3458 196332 2457.7
    - mes_May                    1      3772 196645 2458.3
    - mes_Sep                    1      3999 196872 2458.8
    - rend_2                     1     12167 205040 2474.4
    - rez_avg_gold_1s            1     55269 248142 2547.4
    
    Step:  AIC=2450.94
    avg_gold ~ mes_Abr + mes_Ago + mes_Dic + mes_Feb + mes_Jul + 
        mes_Jun + mes_Mar + mes_May + mes_Nov + mes_Oct + mes_Sep + 
        rez_avg_gold_1s + rez_avg_gold_2s + rez_avg_gold_3s + rez_avg_gold_4s + 
        rez_avg_gold_6s + rez_avg_gold_8s + rez_std_gold_1s + rez_std_gold_2s + 
        rez_std_gold_3s + rez_std_gold_4s + rez_std_gold_6s + rez_rend_gold_1s + 
        rez_rend_gold_2s + rez_rend_gold_3s + rez_rend_gold_4s + 
        rez_rend_gold_6s + rez_rend_gold_8s + rachas_sube_gold + 
        rachas_sube_gold_rez1d + rachas_sube_stdgold_rez1d + rez_avg_gold_1s_2 + 
        rend_2
    
                                Df Sum of Sq    RSS    AIC
    - rez_std_gold_3s            1         8 192883 2448.9
    - mes_Ago                    1        20 192895 2449.0
    - rachas_sube_gold           1        49 192924 2449.0
    - rez_rend_gold_1s           1        62 192937 2449.1
    - mes_Feb                    1       104 192980 2449.1
    - rez_rend_gold_8s           1       188 193064 2449.3
    - rez_avg_gold_6s            1       212 193087 2449.4
    - rez_avg_gold_2s            1       219 193094 2449.4
    - rez_rend_gold_6s           1       224 193099 2449.4
    - mes_Oct                    1       263 193139 2449.5
    - rez_avg_gold_3s            1       336 193212 2449.6
    - rachas_sube_gold_rez1d     1       444 193319 2449.8
    - rez_avg_gold_1s_2          1       512 193387 2449.9
    - rez_rend_gold_4s           1       519 193394 2450.0
    - rez_rend_gold_3s           1       541 193417 2450.0
    - rez_rend_gold_2s           1       803 193678 2450.5
    - rez_std_gold_2s            1       839 193715 2450.6
    <none>                                   192876 2450.9
    - rez_avg_gold_8s            1      1014 193889 2450.9
    - rez_std_gold_4s            1      1041 193917 2451.0
    - mes_Jul                    1      1089 193964 2451.1
    - mes_Abr                    1      1189 194065 2451.3
    - rez_avg_gold_4s            1      1251 194127 2451.4
    - mes_Dic                    1      1251 194127 2451.4
    - mes_Jun                    1      1401 194276 2451.7
    - rachas_sube_stdgold_rez1d  1      1707 194583 2452.3
    - rez_std_gold_6s            1      1867 194743 2452.6
    - mes_Nov                    1      2297 195172 2453.5
    - mes_Mar                    1      3059 195934 2455.0
    - rez_std_gold_1s            1      3459 196334 2455.7
    - mes_May                    1      3788 196664 2456.4
    - mes_Sep                    1      3998 196873 2456.8
    - rend_2                     1     12197 205073 2472.4
    - rez_avg_gold_1s            1     55748 248624 2546.2
    
    Step:  AIC=2448.95
    avg_gold ~ mes_Abr + mes_Ago + mes_Dic + mes_Feb + mes_Jul + 
        mes_Jun + mes_Mar + mes_May + mes_Nov + mes_Oct + mes_Sep + 
        rez_avg_gold_1s + rez_avg_gold_2s + rez_avg_gold_3s + rez_avg_gold_4s + 
        rez_avg_gold_6s + rez_avg_gold_8s + rez_std_gold_1s + rez_std_gold_2s + 
        rez_std_gold_4s + rez_std_gold_6s + rez_rend_gold_1s + rez_rend_gold_2s + 
        rez_rend_gold_3s + rez_rend_gold_4s + rez_rend_gold_6s + 
        rez_rend_gold_8s + rachas_sube_gold + rachas_sube_gold_rez1d + 
        rachas_sube_stdgold_rez1d + rez_avg_gold_1s_2 + rend_2
    
                                Df Sum of Sq    RSS    AIC
    - mes_Ago                    1        18 192901 2447.0
    - rachas_sube_gold           1        52 192935 2447.1
    - rez_rend_gold_1s           1        61 192944 2447.1
    - mes_Feb                    1       106 192989 2447.2
    - rez_rend_gold_8s           1       183 193067 2447.3
    - rez_avg_gold_6s            1       209 193093 2447.4
    - rez_avg_gold_2s            1       225 193108 2447.4
    - rez_rend_gold_6s           1       228 193111 2447.4
    - mes_Oct                    1       263 193146 2447.5
    - rez_avg_gold_3s            1       339 193222 2447.6
    - rachas_sube_gold_rez1d     1       445 193328 2447.8
    - rez_avg_gold_1s_2          1       515 193398 2448.0
    - rez_rend_gold_4s           1       518 193401 2448.0
    - rez_rend_gold_3s           1       537 193420 2448.0
    - rez_rend_gold_2s           1       801 193684 2448.5
    - rez_std_gold_2s            1       858 193742 2448.7
    <none>                                   192883 2448.9
    - rez_avg_gold_8s            1      1026 193909 2449.0
    - mes_Jul                    1      1096 193979 2449.1
    - mes_Abr                    1      1204 194087 2449.3
    - rez_avg_gold_4s            1      1247 194130 2449.4
    - mes_Dic                    1      1263 194146 2449.4
    - rez_std_gold_4s            1      1278 194161 2449.5
    - mes_Jun                    1      1419 194302 2449.8
    - rez_std_gold_6s            1      1896 194780 2450.7
    - mes_Nov                    1      2313 195196 2451.5
    - rachas_sube_stdgold_rez1d  1      2843 195727 2452.6
    - mes_Mar                    1      3102 195985 2453.1
    - rez_std_gold_1s            1      3452 196335 2453.7
    - mes_May                    1      3814 196697 2454.4
    - mes_Sep                    1      4001 196884 2454.8
    - rend_2                     1     12217 205100 2470.5
    - rez_avg_gold_1s            1     55922 248806 2544.5
    
    Step:  AIC=2446.99
    avg_gold ~ mes_Abr + mes_Dic + mes_Feb + mes_Jul + mes_Jun + 
        mes_Mar + mes_May + mes_Nov + mes_Oct + mes_Sep + rez_avg_gold_1s + 
        rez_avg_gold_2s + rez_avg_gold_3s + rez_avg_gold_4s + rez_avg_gold_6s + 
        rez_avg_gold_8s + rez_std_gold_1s + rez_std_gold_2s + rez_std_gold_4s + 
        rez_std_gold_6s + rez_rend_gold_1s + rez_rend_gold_2s + rez_rend_gold_3s + 
        rez_rend_gold_4s + rez_rend_gold_6s + rez_rend_gold_8s + 
        rachas_sube_gold + rachas_sube_gold_rez1d + rachas_sube_stdgold_rez1d + 
        rez_avg_gold_1s_2 + rend_2
    
                                Df Sum of Sq    RSS    AIC
    - rachas_sube_gold           1        50 192950 2445.1
    - rez_rend_gold_1s           1        65 192966 2445.1
    - rez_rend_gold_8s           1       190 193091 2445.4
    - rez_avg_gold_6s            1       197 193098 2445.4
    - mes_Feb                    1       208 193109 2445.4
    - rez_avg_gold_2s            1       218 193119 2445.4
    - rez_rend_gold_6s           1       230 193130 2445.4
    - rez_avg_gold_3s            1       341 193242 2445.7
    - rachas_sube_gold_rez1d     1       440 193341 2445.9
    - mes_Oct                    1       457 193357 2445.9
    - rez_rend_gold_4s           1       504 193405 2446.0
    - rez_rend_gold_3s           1       521 193422 2446.0
    - rez_avg_gold_1s_2          1       532 193433 2446.0
    - rez_rend_gold_2s           1       830 193731 2446.6
    - rez_std_gold_2s            1       848 193749 2446.7
    <none>                                   192901 2447.0
    - rez_avg_gold_8s            1      1028 193929 2447.0
    - rez_avg_gold_4s            1      1246 194147 2447.4
    - rez_std_gold_4s            1      1261 194162 2447.5
    - mes_Jul                    1      1752 194653 2448.4
    - rez_std_gold_6s            1      1883 194784 2448.7
    - mes_Abr                    1      1996 194897 2448.9
    - mes_Dic                    1      2032 194933 2449.0
    - mes_Jun                    1      2171 195071 2449.3
    - rachas_sube_stdgold_rez1d  1      2829 195730 2450.6
    - mes_Nov                    1      3320 196221 2451.5
    - rez_std_gold_1s            1      3470 196371 2451.8
    - mes_Mar                    1      4715 197616 2454.2
    - mes_May                    1      5696 198597 2456.1
    - mes_Sep                    1      5902 198803 2456.5
    - rend_2                     1     12247 205148 2468.6
    - rez_avg_gold_1s            1     56414 249314 2543.2
    
    Step:  AIC=2445.08
    avg_gold ~ mes_Abr + mes_Dic + mes_Feb + mes_Jul + mes_Jun + 
        mes_Mar + mes_May + mes_Nov + mes_Oct + mes_Sep + rez_avg_gold_1s + 
        rez_avg_gold_2s + rez_avg_gold_3s + rez_avg_gold_4s + rez_avg_gold_6s + 
        rez_avg_gold_8s + rez_std_gold_1s + rez_std_gold_2s + rez_std_gold_4s + 
        rez_std_gold_6s + rez_rend_gold_1s + rez_rend_gold_2s + rez_rend_gold_3s + 
        rez_rend_gold_4s + rez_rend_gold_6s + rez_rend_gold_8s + 
        rachas_sube_gold_rez1d + rachas_sube_stdgold_rez1d + rez_avg_gold_1s_2 + 
        rend_2
    
                                Df Sum of Sq    RSS    AIC
    - rez_rend_gold_1s           1        52 193002 2443.2
    - rez_rend_gold_8s           1       192 193143 2443.5
    - rez_avg_gold_6s            1       196 193146 2443.5
    - rez_rend_gold_6s           1       213 193163 2443.5
    - mes_Feb                    1       215 193165 2443.5
    - rez_avg_gold_2s            1       232 193183 2443.6
    - rez_avg_gold_3s            1       341 193291 2443.8
    - rachas_sube_gold_rez1d     1       429 193379 2443.9
    - mes_Oct                    1       454 193405 2444.0
    - rez_rend_gold_4s           1       505 193455 2444.1
    - rez_rend_gold_3s           1       519 193470 2444.1
    - rez_avg_gold_1s_2          1       556 193506 2444.2
    - rez_rend_gold_2s           1       813 193764 2444.7
    - rez_std_gold_2s            1       875 193825 2444.8
    <none>                                   192950 2445.1
    - rez_avg_gold_8s            1      1012 193962 2445.1
    - rez_avg_gold_4s            1      1226 194177 2445.5
    - rez_std_gold_4s            1      1267 194218 2445.6
    - mes_Jul                    1      1748 194698 2446.5
    - rez_std_gold_6s            1      1877 194828 2446.8
    - mes_Abr                    1      1986 194937 2447.0
    - mes_Dic                    1      2012 194962 2447.1
    - mes_Jun                    1      2174 195125 2447.4
    - rachas_sube_stdgold_rez1d  1      2871 195822 2448.7
    - mes_Nov                    1      3310 196260 2449.6
    - rez_std_gold_1s            1      3481 196432 2449.9
    - mes_Mar                    1      4685 197635 2452.3
    - mes_May                    1      5745 198696 2454.3
    - mes_Sep                    1      5853 198803 2454.5
    - rend_2                     1     12255 205205 2466.7
    - rez_avg_gold_1s            1     57500 250450 2543.0
    
    Step:  AIC=2443.19
    avg_gold ~ mes_Abr + mes_Dic + mes_Feb + mes_Jul + mes_Jun + 
        mes_Mar + mes_May + mes_Nov + mes_Oct + mes_Sep + rez_avg_gold_1s + 
        rez_avg_gold_2s + rez_avg_gold_3s + rez_avg_gold_4s + rez_avg_gold_6s + 
        rez_avg_gold_8s + rez_std_gold_1s + rez_std_gold_2s + rez_std_gold_4s + 
        rez_std_gold_6s + rez_rend_gold_2s + rez_rend_gold_3s + rez_rend_gold_4s + 
        rez_rend_gold_6s + rez_rend_gold_8s + rachas_sube_gold_rez1d + 
        rachas_sube_stdgold_rez1d + rez_avg_gold_1s_2 + rend_2
    
                                Df Sum of Sq    RSS    AIC
    - rez_rend_gold_8s           1       188 193190 2441.6
    - rez_avg_gold_6s            1       195 193198 2441.6
    - rez_rend_gold_6s           1       212 193214 2441.6
    - mes_Feb                    1       213 193216 2441.6
    - rachas_sube_gold_rez1d     1       425 193427 2442.0
    - mes_Oct                    1       480 193483 2442.1
    - rez_rend_gold_4s           1       515 193518 2442.2
    - rez_rend_gold_3s           1       521 193524 2442.2
    - rez_avg_gold_1s_2          1       522 193524 2442.2
    - rez_std_gold_2s            1       857 193859 2442.9
    - rez_rend_gold_2s           1       909 193911 2443.0
    <none>                                   193002 2443.2
    - rez_avg_gold_8s            1      1035 194038 2443.2
    - rez_std_gold_4s            1      1291 194293 2443.7
    - rez_avg_gold_4s            1      1313 194315 2443.8
    - rez_avg_gold_3s            1      1390 194393 2443.9
    - mes_Jul                    1      1704 194707 2444.6
    - rez_std_gold_6s            1      1867 194869 2444.9
    - mes_Abr                    1      1975 194978 2445.1
    - mes_Dic                    1      1988 194990 2445.1
    - mes_Jun                    1      2135 195138 2445.4
    - rachas_sube_stdgold_rez1d  1      2870 195873 2446.8
    - mes_Nov                    1      3263 196266 2447.6
    - rez_std_gold_1s            1      3563 196566 2448.2
    - mes_Mar                    1      4675 197678 2450.4
    - mes_May                    1      5734 198737 2452.4
    - mes_Sep                    1      5827 198830 2452.6
    - rez_avg_gold_2s            1     10922 203925 2462.3
    - rend_2                     1     12214 205216 2464.7
    - rez_avg_gold_1s            1     57897 250900 2541.7
    
    Step:  AIC=2441.56
    avg_gold ~ mes_Abr + mes_Dic + mes_Feb + mes_Jul + mes_Jun + 
        mes_Mar + mes_May + mes_Nov + mes_Oct + mes_Sep + rez_avg_gold_1s + 
        rez_avg_gold_2s + rez_avg_gold_3s + rez_avg_gold_4s + rez_avg_gold_6s + 
        rez_avg_gold_8s + rez_std_gold_1s + rez_std_gold_2s + rez_std_gold_4s + 
        rez_std_gold_6s + rez_rend_gold_2s + rez_rend_gold_3s + rez_rend_gold_4s + 
        rez_rend_gold_6s + rachas_sube_gold_rez1d + rachas_sube_stdgold_rez1d + 
        rez_avg_gold_1s_2 + rend_2
    
                                Df Sum of Sq    RSS    AIC
    - rez_avg_gold_6s            1       187 193378 2439.9
    - rez_rend_gold_6s           1       194 193385 2439.9
    - mes_Feb                    1       221 193411 2440.0
    - rachas_sube_gold_rez1d     1       416 193607 2440.4
    - mes_Oct                    1       421 193612 2440.4
    - rez_rend_gold_4s           1       497 193688 2440.6
    - rez_rend_gold_3s           1       505 193695 2440.6
    - rez_avg_gold_1s_2          1       555 193745 2440.7
    - rez_rend_gold_2s           1       813 194003 2441.2
    - rez_std_gold_2s            1       936 194126 2441.4
    <none>                                   193190 2441.6
    - rez_avg_gold_8s            1      1021 194211 2441.6
    - rez_avg_gold_4s            1      1209 194400 2441.9
    - rez_std_gold_4s            1      1248 194438 2442.0
    - rez_avg_gold_3s            1      1278 194468 2442.1
    - mes_Jul                    1      1673 194864 2442.9
    - rez_std_gold_6s            1      1850 195041 2443.2
    - mes_Abr                    1      1904 195094 2443.3
    - mes_Dic                    1      1985 195175 2443.5
    - mes_Jun                    1      2096 195286 2443.7
    - rachas_sube_stdgold_rez1d  1      2957 196148 2445.4
    - mes_Nov                    1      3290 196480 2446.0
    - rez_std_gold_1s            1      3419 196610 2446.3
    - mes_Mar                    1      4538 197728 2448.4
    - mes_Sep                    1      5707 198897 2450.7
    - mes_May                    1      5839 199030 2451.0
    - rez_avg_gold_2s            1     11129 204319 2461.0
    - rend_2                     1     12026 205216 2462.7
    - rez_avg_gold_1s            1     58389 251579 2540.7
    
    Step:  AIC=2439.93
    avg_gold ~ mes_Abr + mes_Dic + mes_Feb + mes_Jul + mes_Jun + 
        mes_Mar + mes_May + mes_Nov + mes_Oct + mes_Sep + rez_avg_gold_1s + 
        rez_avg_gold_2s + rez_avg_gold_3s + rez_avg_gold_4s + rez_avg_gold_8s + 
        rez_std_gold_1s + rez_std_gold_2s + rez_std_gold_4s + rez_std_gold_6s + 
        rez_rend_gold_2s + rez_rend_gold_3s + rez_rend_gold_4s + 
        rez_rend_gold_6s + rachas_sube_gold_rez1d + rachas_sube_stdgold_rez1d + 
        rez_avg_gold_1s_2 + rend_2
    
                                Df Sum of Sq    RSS    AIC
    - mes_Feb                    1       205 193582 2438.3
    - rez_rend_gold_6s           1       207 193585 2438.3
    - rachas_sube_gold_rez1d     1       399 193777 2438.7
    - mes_Oct                    1       415 193793 2438.8
    - rez_rend_gold_2s           1       717 194095 2439.3
    - rez_avg_gold_1s_2          1       756 194133 2439.4
    - rez_rend_gold_4s           1       870 194247 2439.7
    - rez_std_gold_2s            1       929 194306 2439.8
    <none>                                   193378 2439.9
    - rez_rend_gold_3s           1      1104 194482 2440.1
    - rez_avg_gold_4s            1      1137 194515 2440.2
    - rez_avg_gold_3s            1      1164 194542 2440.2
    - rez_avg_gold_8s            1      1178 194556 2440.3
    - rez_std_gold_4s            1      1223 194601 2440.3
    - mes_Jul                    1      1701 195078 2441.3
    - rez_std_gold_6s            1      1890 195268 2441.7
    - mes_Abr                    1      1911 195289 2441.7
    - mes_Jun                    1      2087 195464 2442.0
    - mes_Dic                    1      2099 195477 2442.1
    - rachas_sube_stdgold_rez1d  1      2930 196308 2443.7
    - mes_Nov                    1      3226 196604 2444.3
    - rez_std_gold_1s            1      3317 196695 2444.4
    - mes_Mar                    1      4420 197798 2446.6
    - mes_Sep                    1      5814 199192 2449.3
    - mes_May                    1      5900 199278 2449.4
    - rez_avg_gold_2s            1     11395 204772 2459.9
    - rend_2                     1     11923 205301 2460.8
    - rez_avg_gold_1s            1     62875 256252 2545.8
    
    Step:  AIC=2438.34
    avg_gold ~ mes_Abr + mes_Dic + mes_Jul + mes_Jun + mes_Mar + 
        mes_May + mes_Nov + mes_Oct + mes_Sep + rez_avg_gold_1s + 
        rez_avg_gold_2s + rez_avg_gold_3s + rez_avg_gold_4s + rez_avg_gold_8s + 
        rez_std_gold_1s + rez_std_gold_2s + rez_std_gold_4s + rez_std_gold_6s + 
        rez_rend_gold_2s + rez_rend_gold_3s + rez_rend_gold_4s + 
        rez_rend_gold_6s + rachas_sube_gold_rez1d + rachas_sube_stdgold_rez1d + 
        rez_avg_gold_1s_2 + rend_2
    
                                Df Sum of Sq    RSS    AIC
    - rez_rend_gold_6s           1       202 193784 2436.7
    - mes_Oct                    1       277 193859 2436.9
    - rachas_sube_gold_rez1d     1       431 194013 2437.2
    - rez_rend_gold_2s           1       724 194306 2437.8
    - rez_avg_gold_1s_2          1       769 194352 2437.9
    - rez_rend_gold_4s           1       843 194426 2438.0
    - rez_std_gold_2s            1       963 194546 2438.2
    <none>                                   193582 2438.3
    - rez_rend_gold_3s           1      1136 194718 2438.6
    - rez_avg_gold_4s            1      1154 194736 2438.6
    - rez_avg_gold_3s            1      1163 194745 2438.6
    - rez_avg_gold_8s            1      1278 194860 2438.9
    - rez_std_gold_4s            1      1288 194870 2438.9
    - mes_Jul                    1      1499 195082 2439.3
    - mes_Abr                    1      1707 195289 2439.7
    - mes_Jun                    1      1883 195466 2440.1
    - mes_Dic                    1      1895 195477 2440.1
    - rez_std_gold_6s            1      1993 195576 2440.3
    - rachas_sube_stdgold_rez1d  1      2990 196572 2442.2
    - mes_Nov                    1      3024 196606 2442.3
    - rez_std_gold_1s            1      3245 196827 2442.7
    - mes_Mar                    1      4316 197898 2444.8
    - mes_Sep                    1      5775 199358 2447.6
    - mes_May                    1      5792 199374 2447.6
    - rez_avg_gold_2s            1     11241 204823 2458.0
    - rend_2                     1     11779 205361 2459.0
    - rez_avg_gold_1s            1     62799 256381 2543.9
    
    Step:  AIC=2436.74
    avg_gold ~ mes_Abr + mes_Dic + mes_Jul + mes_Jun + mes_Mar + 
        mes_May + mes_Nov + mes_Oct + mes_Sep + rez_avg_gold_1s + 
        rez_avg_gold_2s + rez_avg_gold_3s + rez_avg_gold_4s + rez_avg_gold_8s + 
        rez_std_gold_1s + rez_std_gold_2s + rez_std_gold_4s + rez_std_gold_6s + 
        rez_rend_gold_2s + rez_rend_gold_3s + rez_rend_gold_4s + 
        rachas_sube_gold_rez1d + rachas_sube_stdgold_rez1d + rez_avg_gold_1s_2 + 
        rend_2
    
                                Df Sum of Sq    RSS    AIC
    - mes_Oct                    1       265 194049 2435.3
    - rachas_sube_gold_rez1d     1       447 194231 2435.6
    - rez_rend_gold_4s           1       656 194440 2436.0
    - rez_rend_gold_2s           1       674 194458 2436.1
    - rez_avg_gold_1s_2          1       889 194673 2436.5
    - rez_rend_gold_3s           1       945 194729 2436.6
    - rez_std_gold_2s            1      1005 194789 2436.7
    <none>                                   193784 2436.7
    - rez_avg_gold_4s            1      1020 194804 2436.8
    - rez_avg_gold_3s            1      1099 194883 2436.9
    - rez_std_gold_4s            1      1233 195017 2437.2
    - mes_Jul                    1      1451 195235 2437.6
    - rez_avg_gold_8s            1      1522 195306 2437.7
    - mes_Abr                    1      1667 195451 2438.0
    - mes_Jun                    1      1823 195607 2438.3
    - mes_Dic                    1      1859 195643 2438.4
    - rez_std_gold_6s            1      1938 195722 2438.6
    - rachas_sube_stdgold_rez1d  1      2945 196729 2440.5
    - mes_Nov                    1      2957 196741 2440.5
    - rez_std_gold_1s            1      3216 197000 2441.0
    - mes_Mar                    1      4228 198012 2443.0
    - mes_May                    1      5749 199533 2445.9
    - mes_Sep                    1      5762 199546 2446.0
    - rez_avg_gold_2s            1     11315 205099 2456.5
    - rend_2                     1     11628 205412 2457.1
    - rez_avg_gold_1s            1     64788 258572 2545.2
    
    Step:  AIC=2435.26
    avg_gold ~ mes_Abr + mes_Dic + mes_Jul + mes_Jun + mes_Mar + 
        mes_May + mes_Nov + mes_Sep + rez_avg_gold_1s + rez_avg_gold_2s + 
        rez_avg_gold_3s + rez_avg_gold_4s + rez_avg_gold_8s + rez_std_gold_1s + 
        rez_std_gold_2s + rez_std_gold_4s + rez_std_gold_6s + rez_rend_gold_2s + 
        rez_rend_gold_3s + rez_rend_gold_4s + rachas_sube_gold_rez1d + 
        rachas_sube_stdgold_rez1d + rez_avg_gold_1s_2 + rend_2
    
                                Df Sum of Sq    RSS    AIC
    - rachas_sube_gold_rez1d     1       540 194589 2434.3
    - rez_rend_gold_2s           1       717 194766 2434.7
    - rez_rend_gold_4s           1       717 194766 2434.7
    - rez_avg_gold_1s_2          1       959 195008 2435.2
    - rez_std_gold_2s            1      1010 195059 2435.2
    - rez_rend_gold_3s           1      1016 195065 2435.3
    <none>                                   194049 2435.3
    - rez_avg_gold_4s            1      1098 195147 2435.4
    - rez_avg_gold_3s            1      1164 195213 2435.6
    - mes_Jul                    1      1229 195279 2435.7
    - rez_std_gold_4s            1      1257 195306 2435.7
    - mes_Abr                    1      1430 195479 2436.1
    - rez_avg_gold_8s            1      1561 195610 2436.3
    - mes_Jun                    1      1591 195640 2436.4
    - mes_Dic                    1      1604 195653 2436.4
    - rez_std_gold_6s            1      2001 196050 2437.2
    - mes_Nov                    1      2704 196753 2438.6
    - rachas_sube_stdgold_rez1d  1      3008 197057 2439.2
    - rez_std_gold_1s            1      3249 197298 2439.6
    - mes_Mar                    1      3969 198018 2441.0
    - mes_May                    1      5499 199548 2444.0
    - mes_Sep                    1      5517 199566 2444.0
    - rez_avg_gold_2s            1     11346 205395 2455.0
    - rend_2                     1     11843 205892 2455.9
    - rez_avg_gold_1s            1     66095 260144 2545.5
    
    Step:  AIC=2434.32
    avg_gold ~ mes_Abr + mes_Dic + mes_Jul + mes_Jun + mes_Mar + 
        mes_May + mes_Nov + mes_Sep + rez_avg_gold_1s + rez_avg_gold_2s + 
        rez_avg_gold_3s + rez_avg_gold_4s + rez_avg_gold_8s + rez_std_gold_1s + 
        rez_std_gold_2s + rez_std_gold_4s + rez_std_gold_6s + rez_rend_gold_2s + 
        rez_rend_gold_3s + rez_rend_gold_4s + rachas_sube_stdgold_rez1d + 
        rez_avg_gold_1s_2 + rend_2
    
                                Df Sum of Sq    RSS    AIC
    - rez_rend_gold_4s           1       648 195237 2433.6
    - rez_avg_gold_1s_2          1       776 195364 2433.8
    - rez_rend_gold_3s           1       833 195422 2434.0
    - rez_rend_gold_2s           1       862 195450 2434.0
    - rez_std_gold_2s            1       957 195546 2434.2
    <none>                                   194589 2434.3
    - rez_std_gold_4s            1      1025 195613 2434.3
    - mes_Jul                    1      1099 195688 2434.5
    - rez_avg_gold_4s            1      1166 195754 2434.6
    - rez_avg_gold_3s            1      1230 195819 2434.7
    - mes_Abr                    1      1273 195862 2434.8
    - mes_Jun                    1      1406 195994 2435.1
    - mes_Dic                    1      1656 196245 2435.6
    - rez_std_gold_6s            1      1968 196556 2436.2
    - rez_avg_gold_8s            1      2066 196655 2436.4
    - mes_Nov                    1      2510 197099 2437.2
    - rachas_sube_stdgold_rez1d  1      2832 197420 2437.9
    - rez_std_gold_1s            1      3442 198031 2439.0
    - mes_Mar                    1      4251 198840 2440.6
    - mes_May                    1      5394 199983 2442.8
    - mes_Sep                    1      5638 200227 2443.3
    - rez_avg_gold_2s            1     11613 206202 2454.5
    - rend_2                     1     11825 206414 2454.9
    - rez_avg_gold_1s            1     65759 260347 2543.8
    
    Step:  AIC=2433.6
    avg_gold ~ mes_Abr + mes_Dic + mes_Jul + mes_Jun + mes_Mar + 
        mes_May + mes_Nov + mes_Sep + rez_avg_gold_1s + rez_avg_gold_2s + 
        rez_avg_gold_3s + rez_avg_gold_4s + rez_avg_gold_8s + rez_std_gold_1s + 
        rez_std_gold_2s + rez_std_gold_4s + rez_std_gold_6s + rez_rend_gold_2s + 
        rez_rend_gold_3s + rachas_sube_stdgold_rez1d + rez_avg_gold_1s_2 + 
        rend_2
    
                                Df Sum of Sq    RSS    AIC
    - rez_rend_gold_3s           1       680 195917 2432.9
    - rez_avg_gold_1s_2          1       760 195997 2433.1
    - rez_rend_gold_2s           1       796 196033 2433.2
    - rez_std_gold_4s            1       954 196190 2433.5
    - rez_avg_gold_4s            1      1019 196255 2433.6
    <none>                                   195237 2433.6
    - rez_std_gold_2s            1      1039 196275 2433.6
    - rez_avg_gold_3s            1      1145 196381 2433.8
    - mes_Jul                    1      1188 196424 2433.9
    - mes_Abr                    1      1317 196554 2434.2
    - rez_avg_gold_8s            1      1420 196657 2434.4
    - mes_Jun                    1      1521 196757 2434.6
    - mes_Dic                    1      1759 196995 2435.0
    - rez_std_gold_6s            1      2320 197556 2436.1
    - mes_Nov                    1      2496 197732 2436.5
    - rachas_sube_stdgold_rez1d  1      2956 198193 2437.3
    - rez_std_gold_1s            1      3230 198466 2437.9
    - mes_Mar                    1      4200 199437 2439.8
    - mes_May                    1      5357 200594 2442.0
    - mes_Sep                    1      5628 200865 2442.5
    - rez_avg_gold_2s            1     11469 206705 2453.5
    - rend_2                     1     11898 207135 2454.2
    - rez_avg_gold_1s            1     65566 260803 2542.5
    
    Step:  AIC=2432.93
    avg_gold ~ mes_Abr + mes_Dic + mes_Jul + mes_Jun + mes_Mar + 
        mes_May + mes_Nov + mes_Sep + rez_avg_gold_1s + rez_avg_gold_2s + 
        rez_avg_gold_3s + rez_avg_gold_4s + rez_avg_gold_8s + rez_std_gold_1s + 
        rez_std_gold_2s + rez_std_gold_4s + rez_std_gold_6s + rez_rend_gold_2s + 
        rachas_sube_stdgold_rez1d + rez_avg_gold_1s_2 + rend_2
    
                                Df Sum of Sq    RSS    AIC
    - rez_rend_gold_2s           1       727 196644 2432.3
    - rez_avg_gold_1s_2          1       737 196654 2432.4
    - rez_std_gold_4s            1       768 196685 2432.4
    - rez_avg_gold_8s            1       819 196736 2432.5
    - rez_avg_gold_4s            1       929 196846 2432.7
    <none>                                   195917 2432.9
    - rez_avg_gold_3s            1      1107 197024 2433.1
    - rez_std_gold_2s            1      1171 197088 2433.2
    - mes_Jul                    1      1171 197088 2433.2
    - mes_Abr                    1      1261 197178 2433.4
    - mes_Jun                    1      1581 197498 2434.0
    - mes_Dic                    1      1815 197731 2434.5
    - rez_std_gold_6s            1      2150 198067 2435.1
    - mes_Nov                    1      2328 198245 2435.4
    - rachas_sube_stdgold_rez1d  1      2905 198822 2436.6
    - rez_std_gold_1s            1      3176 199093 2437.1
    - mes_Mar                    1      4189 200106 2439.0
    - mes_May                    1      5445 201362 2441.4
    - mes_Sep                    1      5514 201431 2441.6
    - rez_avg_gold_2s            1     11727 207644 2453.2
    - rend_2                     1     11811 207728 2453.3
    - rez_avg_gold_1s            1     65720 261637 2541.7
    
    Step:  AIC=2432.35
    avg_gold ~ mes_Abr + mes_Dic + mes_Jul + mes_Jun + mes_Mar + 
        mes_May + mes_Nov + mes_Sep + rez_avg_gold_1s + rez_avg_gold_2s + 
        rez_avg_gold_3s + rez_avg_gold_4s + rez_avg_gold_8s + rez_std_gold_1s + 
        rez_std_gold_2s + rez_std_gold_4s + rez_std_gold_6s + rachas_sube_stdgold_rez1d + 
        rez_avg_gold_1s_2 + rend_2
    
                                Df Sum of Sq    RSS    AIC
    - rez_avg_gold_1s_2          1       463 197107 2431.2
    - rez_avg_gold_4s            1       579 197223 2431.5
    - rez_std_gold_4s            1       752 197396 2431.8
    - rez_avg_gold_8s            1       789 197433 2431.9
    - rez_avg_gold_3s            1       985 197629 2432.3
    - mes_Jul                    1       999 197643 2432.3
    <none>                                   196644 2432.3
    - rez_std_gold_2s            1      1174 197818 2432.6
    - mes_Abr                    1      1238 197882 2432.8
    - mes_Jun                    1      1376 198021 2433.0
    - mes_Dic                    1      1770 198414 2433.8
    - mes_Nov                    1      2219 198863 2434.6
    - rez_std_gold_6s            1      2493 199137 2435.2
    - rachas_sube_stdgold_rez1d  1      2893 199537 2435.9
    - rez_std_gold_1s            1      3189 199833 2436.5
    - mes_Mar                    1      4125 200769 2438.3
    - mes_Sep                    1      5206 201850 2440.3
    - mes_May                    1      5350 201994 2440.6
    - rend_2                     1     11700 208344 2452.5
    - rez_avg_gold_2s            1     11838 208482 2452.7
    - rez_avg_gold_1s            1     65663 262307 2540.7
    
    Step:  AIC=2431.25
    avg_gold ~ mes_Abr + mes_Dic + mes_Jul + mes_Jun + mes_Mar + 
        mes_May + mes_Nov + mes_Sep + rez_avg_gold_1s + rez_avg_gold_2s + 
        rez_avg_gold_3s + rez_avg_gold_4s + rez_avg_gold_8s + rez_std_gold_1s + 
        rez_std_gold_2s + rez_std_gold_4s + rez_std_gold_6s + rachas_sube_stdgold_rez1d + 
        rend_2
    
                                Df Sum of Sq    RSS    AIC
    - rez_avg_gold_4s            1       586 197693 2430.4
    - rez_std_gold_4s            1       589 197696 2430.4
    - rez_avg_gold_8s            1       855 197961 2430.9
    - mes_Jul                    1       910 198017 2431.0
    - rez_avg_gold_3s            1       994 198101 2431.2
    <none>                                   197107 2431.2
    - rez_std_gold_2s            1      1071 198177 2431.3
    - mes_Abr                    1      1099 198206 2431.4
    - mes_Jun                    1      1176 198283 2431.5
    - mes_Dic                    1      2006 199113 2433.1
    - mes_Nov                    1      2380 199487 2433.8
    - rachas_sube_stdgold_rez1d  1      2536 199643 2434.1
    - rez_std_gold_6s            1      2653 199760 2434.4
    - rez_std_gold_1s            1      3314 200421 2435.6
    - mes_Mar                    1      3912 201019 2436.8
    - mes_May                    1      4995 202102 2438.8
    - mes_Sep                    1      5132 202238 2439.1
    - rez_avg_gold_2s            1     11841 208948 2451.6
    - rend_2                     1     11869 208976 2451.6
    - rez_avg_gold_1s            1    302031 499138 2785.1
    
    Step:  AIC=2430.38
    avg_gold ~ mes_Abr + mes_Dic + mes_Jul + mes_Jun + mes_Mar + 
        mes_May + mes_Nov + mes_Sep + rez_avg_gold_1s + rez_avg_gold_2s + 
        rez_avg_gold_3s + rez_avg_gold_8s + rez_std_gold_1s + rez_std_gold_2s + 
        rez_std_gold_4s + rez_std_gold_6s + rachas_sube_stdgold_rez1d + 
        rend_2
    
                                Df Sum of Sq    RSS    AIC
    - rez_std_gold_4s            1       403 198096 2429.2
    - rez_avg_gold_3s            1       413 198106 2429.2
    - rez_avg_gold_8s            1       418 198111 2429.2
    - mes_Jul                    1       941 198634 2430.2
    - rez_std_gold_2s            1      1025 198717 2430.4
    <none>                                   197693 2430.4
    - mes_Abr                    1      1036 198728 2430.4
    - mes_Jun                    1      1236 198929 2430.8
    - mes_Dic                    1      2084 199777 2432.4
    - mes_Nov                    1      2296 199989 2432.8
    - rez_std_gold_6s            1      2414 200107 2433.0
    - rachas_sube_stdgold_rez1d  1      2490 200182 2433.2
    - rez_std_gold_1s            1      3107 200800 2434.4
    - mes_Mar                    1      4207 201899 2436.4
    - mes_Sep                    1      5332 203025 2438.6
    - mes_May                    1      5338 203030 2438.6
    - rez_avg_gold_2s            1     11341 209034 2449.8
    - rend_2                     1     11527 209219 2450.1
    - rez_avg_gold_1s            1    301732 499425 2783.3
    
    Step:  AIC=2429.16
    avg_gold ~ mes_Abr + mes_Dic + mes_Jul + mes_Jun + mes_Mar + 
        mes_May + mes_Nov + mes_Sep + rez_avg_gold_1s + rez_avg_gold_2s + 
        rez_avg_gold_3s + rez_avg_gold_8s + rez_std_gold_1s + rez_std_gold_2s + 
        rez_std_gold_6s + rachas_sube_stdgold_rez1d + rend_2
    
                                Df Sum of Sq    RSS    AIC
    - rez_avg_gold_3s            1       403 198499 2427.9
    - rez_avg_gold_8s            1       405 198501 2427.9
    - mes_Jul                    1       824 198920 2428.8
    - mes_Abr                    1       994 199090 2429.1
    <none>                                   198096 2429.2
    - mes_Jun                    1      1131 199227 2429.3
    - rez_std_gold_2s            1      1373 199469 2429.8
    - mes_Dic                    1      2103 200199 2431.2
    - rachas_sube_stdgold_rez1d  1      2164 200260 2431.3
    - rez_std_gold_6s            1      2187 200283 2431.4
    - mes_Nov                    1      2241 200337 2431.5
    - rez_std_gold_1s            1      2817 200913 2432.6
    - mes_Mar                    1      4310 202406 2435.4
    - mes_Sep                    1      5165 203261 2437.0
    - mes_May                    1      5364 203460 2437.4
    - rend_2                     1     11158 209254 2448.2
    - rez_avg_gold_2s            1     11351 209447 2448.5
    - rez_avg_gold_1s            1    303776 501872 2783.2
    
    Step:  AIC=2427.94
    avg_gold ~ mes_Abr + mes_Dic + mes_Jul + mes_Jun + mes_Mar + 
        mes_May + mes_Nov + mes_Sep + rez_avg_gold_1s + rez_avg_gold_2s + 
        rez_avg_gold_8s + rez_std_gold_1s + rez_std_gold_2s + rez_std_gold_6s + 
        rachas_sube_stdgold_rez1d + rend_2
    
                                Df Sum of Sq    RSS    AIC
    - mes_Jul                    1       818 199317 2427.5
    - rez_avg_gold_8s            1       904 199403 2427.7
    - mes_Abr                    1       956 199455 2427.8
    <none>                                   198499 2427.9
    - mes_Jun                    1      1150 199649 2428.2
    - rez_std_gold_2s            1      1512 200011 2428.8
    - mes_Dic                    1      2071 200570 2429.9
    - mes_Nov                    1      2201 200700 2430.2
    - rachas_sube_stdgold_rez1d  1      2262 200761 2430.3
    - rez_std_gold_6s            1      2336 200835 2430.4
    - rez_std_gold_1s            1      2698 201197 2431.1
    - mes_Mar                    1      4170 202669 2433.9
    - mes_Sep                    1      4980 203479 2435.4
    - mes_May                    1      5407 203906 2436.2
    - rend_2                     1     10991 209490 2446.6
    - rez_avg_gold_2s            1     18255 216754 2459.6
    - rez_avg_gold_1s            1    313747 512246 2789.0
    
    Step:  AIC=2427.52
    avg_gold ~ mes_Abr + mes_Dic + mes_Jun + mes_Mar + mes_May + 
        mes_Nov + mes_Sep + rez_avg_gold_1s + rez_avg_gold_2s + rez_avg_gold_8s + 
        rez_std_gold_1s + rez_std_gold_2s + rez_std_gold_6s + rachas_sube_stdgold_rez1d + 
        rend_2
    
                                Df Sum of Sq    RSS    AIC
    - mes_Abr                    1       607 199923 2426.7
    - rez_avg_gold_8s            1       771 200087 2427.0
    - mes_Jun                    1       803 200120 2427.1
    <none>                                   199317 2427.5
    - rez_std_gold_2s            1      1524 200841 2428.4
    - mes_Dic                    1      1570 200886 2428.5
    - mes_Nov                    1      1743 201060 2428.8
    - rachas_sube_stdgold_rez1d  1      2143 201460 2429.6
    - rez_std_gold_6s            1      2369 201686 2430.0
    - rez_std_gold_1s            1      2575 201892 2430.4
    - mes_Mar                    1      3566 202883 2432.3
    - mes_Sep                    1      4377 203693 2433.8
    - mes_May                    1      4750 204067 2434.5
    - rend_2                     1     11004 210321 2446.1
    - rez_avg_gold_2s            1     18509 217826 2459.5
    - rez_avg_gold_1s            1    317005 516322 2790.1
    
    Step:  AIC=2426.68
    avg_gold ~ mes_Dic + mes_Jun + mes_Mar + mes_May + mes_Nov + 
        mes_Sep + rez_avg_gold_1s + rez_avg_gold_2s + rez_avg_gold_8s + 
        rez_std_gold_1s + rez_std_gold_2s + rez_std_gold_6s + rachas_sube_stdgold_rez1d + 
        rend_2
    
                                Df Sum of Sq    RSS    AIC
    - mes_Jun                    1       578 200502 2425.8
    - rez_avg_gold_8s            1       686 200609 2426.0
    <none>                                   199923 2426.7
    - mes_Dic                    1      1223 201146 2427.0
    - mes_Nov                    1      1415 201338 2427.4
    - rez_std_gold_2s            1      1673 201596 2427.9
    - rachas_sube_stdgold_rez1d  1      2024 201947 2428.5
    - rez_std_gold_6s            1      2247 202170 2429.0
    - rez_std_gold_1s            1      2554 202477 2429.5
    - mes_Mar                    1      3125 203048 2430.6
    - mes_Sep                    1      3938 203861 2432.2
    - mes_May                    1      4263 204186 2432.8
    - rend_2                     1     10801 210724 2444.8
    - rez_avg_gold_2s            1     19036 218959 2459.5
    - rez_avg_gold_1s            1    321647 521570 2791.9
    
    Step:  AIC=2425.79
    avg_gold ~ mes_Dic + mes_Mar + mes_May + mes_Nov + mes_Sep + 
        rez_avg_gold_1s + rez_avg_gold_2s + rez_avg_gold_8s + rez_std_gold_1s + 
        rez_std_gold_2s + rez_std_gold_6s + rachas_sube_stdgold_rez1d + 
        rend_2
    
                                Df Sum of Sq    RSS    AIC
    - rez_avg_gold_8s            1       628 201130 2425.0
    - mes_Dic                    1       977 201478 2425.7
    <none>                                   200502 2425.8
    - mes_Nov                    1      1191 201693 2426.1
    - rez_std_gold_2s            1      1612 202113 2426.8
    - rachas_sube_stdgold_rez1d  1      1934 202436 2427.5
    - rez_std_gold_6s            1      2301 202802 2428.2
    - rez_std_gold_1s            1      2759 203261 2429.0
    - mes_Mar                    1      2799 203300 2429.1
    - mes_Sep                    1      3583 204085 2430.6
    - mes_May                    1      3885 204386 2431.1
    - rend_2                     1     11058 211559 2444.3
    - rez_avg_gold_2s            1     19323 219824 2459.0
    - rez_avg_gold_1s            1    324798 525300 2792.7
    
    Step:  AIC=2424.99
    avg_gold ~ mes_Dic + mes_Mar + mes_May + mes_Nov + mes_Sep + 
        rez_avg_gold_1s + rez_avg_gold_2s + rez_std_gold_1s + rez_std_gold_2s + 
        rez_std_gold_6s + rachas_sube_stdgold_rez1d + rend_2
    
                                Df Sum of Sq    RSS    AIC
    - mes_Dic                    1       755 201885 2424.4
    <none>                                   201130 2425.0
    - mes_Nov                    1      1085 202215 2425.1
    - rez_std_gold_2s            1      1718 202847 2426.2
    - rachas_sube_stdgold_rez1d  1      1791 202920 2426.4
    - rez_std_gold_6s            1      2033 203163 2426.8
    - rez_std_gold_1s            1      2756 203886 2428.2
    - mes_Mar                    1      3098 204227 2428.8
    - mes_May                    1      3799 204929 2430.2
    - mes_Sep                    1      4314 205443 2431.1
    - rend_2                     1     11179 212309 2443.7
    - rez_avg_gold_2s            1     18852 219982 2457.3
    - rez_avg_gold_1s            1    326915 528044 2792.7
    
    Step:  AIC=2424.42
    avg_gold ~ mes_Mar + mes_May + mes_Nov + mes_Sep + rez_avg_gold_1s + 
        rez_avg_gold_2s + rez_std_gold_1s + rez_std_gold_2s + rez_std_gold_6s + 
        rachas_sube_stdgold_rez1d + rend_2
    
                                Df Sum of Sq    RSS    AIC
    - mes_Nov                    1       856 202741 2424.0
    <none>                                   201885 2424.4
    - rez_std_gold_2s            1      1640 203525 2425.5
    - rachas_sube_stdgold_rez1d  1      1737 203622 2425.7
    - rez_std_gold_6s            1      1979 203864 2426.2
    - mes_Mar                    1      2698 204584 2427.5
    - rez_std_gold_1s            1      2973 204858 2428.0
    - mes_May                    1      3367 205252 2428.8
    - mes_Sep                    1      3853 205738 2429.7
    - rend_2                     1     11945 213831 2444.4
    - rez_avg_gold_2s            1     20605 222490 2459.6
    - rez_avg_gold_1s            1    342283 544168 2802.2
    
    Step:  AIC=2424.04
    avg_gold ~ mes_Mar + mes_May + mes_Sep + rez_avg_gold_1s + rez_avg_gold_2s + 
        rez_std_gold_1s + rez_std_gold_2s + rez_std_gold_6s + rachas_sube_stdgold_rez1d + 
        rend_2
    
                                Df Sum of Sq    RSS    AIC
    <none>                                   202741 2424.0
    - rachas_sube_stdgold_rez1d  1      1346 204087 2424.6
    - rez_std_gold_2s            1      1479 204220 2424.8
    - rez_std_gold_6s            1      1813 204555 2425.4
    - mes_Mar                    1      2366 205107 2426.5
    - rez_std_gold_1s            1      2936 205677 2427.6
    - mes_May                    1      3009 205750 2427.7
    - mes_Sep                    1      3450 206191 2428.5
    - rend_2                     1     12135 214876 2444.3
    - rez_avg_gold_2s            1     21801 224542 2461.2
    - rez_avg_gold_1s            1    349375 552117 2805.7


## Coeficientes

### Summary


```R
summary(m1)
```


    
    Call:
    lm(formula = avg_gold ~ mes_Mar + mes_May + mes_Sep + rez_avg_gold_1s + 
        rez_avg_gold_2s + rez_std_gold_1s + rez_std_gold_2s + rez_std_gold_6s + 
        rachas_sube_stdgold_rez1d + rend_2, data = train %>% select(-week, 
        -min_date, -max_date, -std_gold))
    
    Residuals:
         Min       1Q   Median       3Q      Max 
    -169.063  -13.730    0.886   13.073   79.576 
    
    Coefficients:
                                Estimate Std. Error t value Pr(>|t|)    
    (Intercept)                 13.81442    9.29634   1.486   0.1381    
    mes_Mar                     -8.75627    4.20258  -2.084   0.0379 *  
    mes_May                    -10.32351    4.39380  -2.350   0.0193 *  
    mes_Sep                    -11.14076    4.42782  -2.516   0.0123 *  
    rez_avg_gold_1s              1.32302    0.05225  25.319  < 2e-16 ***
    rez_avg_gold_2s             -0.33038    0.05224  -6.325 7.28e-10 ***
    rez_std_gold_1s             -0.42792    0.18438  -2.321   0.0208 *  
    rez_std_gold_2s              0.29425    0.17865   1.647   0.1004    
    rez_std_gold_6s             -0.30377    0.16653  -1.824   0.0689 .  
    rachas_sube_stdgold_rez1d    2.07288    1.31894   1.572   0.1169    
    rend_2                    7334.39375 1554.36039   4.719 3.37e-06 ***
    ---
    Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
    
    Residual standard error: 23.35 on 372 degrees of freedom
    Multiple R-squared:  0.9843,	Adjusted R-squared:  0.9839 
    F-statistic:  2330 on 10 and 372 DF,  p-value: < 2.2e-16



### Gráfica


```R
options(repr.plot.width=5, repr.plot.height=4)
m1$coefficients %>%
 data.frame() %>%
 rename('coef' = '.') %>%
 mutate(variable = rownames(.)) %>%
 filter(variable != '(Intercept)') %>%
 filter(!is.na(coef)) %>%
 arrange(coef) %>%
 mutate(variable = fct_inorder(variable)) %>%
 ggplot() +
 geom_col(aes(x = variable, y = coef), fill = 'darkblue') +
 theme_bw() +
 scale_y_continuous(labels = scales::comma) +
 theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.5))
```


![png](output_95_0.png)


## RMSE - Train & Valid


```R
options(repr.plot.width=5, repr.plot.height=4)
data.frame(desc = c('train', 'valid'),
           rmse = c(rmse(train$avg_gold, predict(m1, train)),
                    rmse(valid$avg_gold, predict(m1, valid)))) %>%
 ggplot() +
 geom_bar(aes(x = desc, y = rmse), stat = 'identity', fill = 'darkblue') +
 theme_bw() +
 ggtitle('RMSE') +
 theme(axis.text.x = element_text(face = 'bold'),
       axis.text.y = element_text(face = 'bold')) +
 geom_text(aes(x = desc, y = rmse, label = round(rmse,2)), vjust = -0.5, size = 4)
```


![png](output_97_0.png)


## Gráfico de errores

Gráfica de errores buscando que se comporte como ruido blanco _(es decir, sin alguna tendencia fuertemente perceptible)_

$$Error_i = PrecioOro_i - PrecioEstimado_i $$


```R
options(repr.plot.width=3, repr.plot.height=3)
valid %>%
 mutate(pred = predict(m1, valid)) %>%
 mutate(error = avg_gold - pred) %>%
 ggplot() +
 geom_line(aes(x = max_date, y = error, group = 1), colour = 'darkblue') +
 theme_bw() +
 ggtitle('Gráfico de errores')
```


![png](output_101_0.png)


## Observado vs Estimado


```R
options(repr.plot.width=7, repr.plot.height=4)
valid %>%
 mutate(pred = predict(m1, valid)) %>%
 ggplot() +
 geom_line(aes(x = max_date, y = avg_gold, group = 1), colour = 'darkblue') +
 geom_line(aes(x = max_date, y = pred, group = 1), colour = 'darkgreen', size = 0.3) +
 theme_bw() +
 ggtitle('Goldprice - Real vs Estimado') + 
 scale_y_continuous(labels = scales::comma) +
 xlab('Día') +
 ylab('Precio del Oro') +
 theme(axis.text.x = element_text(face = 'bold'),
       axis.text.y = element_text(face = 'bold')) +
 annotate(geom  = 'text',
          x     = as.Date('2016-01-01'),
          y     = 1650,
          label = 'Estimado',
          colour = 'darkgreen',
          hjust = 1) +
 annotate(geom  = 'text',
          x     = as.Date('2016-01-01'),
          y     = 1620,
          label = 'Real',
          colour = 'darkblue',
          hjust = 1.2)
```


![png](output_103_0.png)


# Predicción

Se hará la predicción del promedio del precio del oro de una semana después de la última registrada.

Para crear el _data frame_ para la predicción, los _rezagos_ serán equivalentes al _rezago - 1_ de los usados para crear el _data frame_ de _train_, ya que aquí se busco que el "último rezago" se la última observación.

## Lectura de datos


```R
gold_pred <- Quandl("WGC/GOLD_DAILY_USD", api_key="xCLj7WmfLsvdi9xo2okv")
names(gold_pred) <- c('date', 'goldprice')

dolar_pred <- Quandl("BDM/SF60653", api_key="xCLj7WmfLsvdi9xo2okv")
names(dolar_pred) <- c('date', 'dolarprice')
```

## Homologación de datos


```R
max_date <- max(gold_pred$date)
min_date <- min(dolar_pred$date)

gold_pred %>%
 filter(date >= min_date) %>%
 filter(date <= max_date) -> gold_pred

dolar_pred %>%
 filter(date >= min_date) %>%
 filter(date <= max_date) -> dolar_pred
```

## Join de datos


```R
dolar_pred %>%
 left_join(gold_pred, by = 'date') %>%
 select(-dolarprice) -> data_pred
```

## Limpiar datos


```R
data_pred %>%
 na.omit() -> data_pred
```


```R
data_pred %>%
 filter(date >= as.Date('2015-01-01')) -> data_pred
```

## Promedio Semanal


```R
data_pred %>%
 mutate(week = paste0(year(date),'-',format(date, '%W'))) %>%
 group_by(week) %>%
 summarise(min_date = min(date),
           max_date = max(date),
           avg_gold = mean(goldprice, na.rm = TRUE),
           std_gold = sd(goldprice, na.rm = TRUE)) %>%
 arrange(as.numeric(gsub('-','',week))) %>%
 filter(!is.na(std_gold)) %>%
 mutate(mes = month(max_date)) %>%
 mutate(mes = ifelse(mes == 1, 'Ene', mes),
        mes = ifelse(mes == 2, 'Feb', mes),
        mes = ifelse(mes == 3, 'Mar', mes),
        mes = ifelse(mes == 4, 'Abr', mes),
        mes = ifelse(mes == 5, 'May', mes),
        mes = ifelse(mes == 6, 'Jun', mes),
        mes = ifelse(mes == 7, 'Jul', mes),
        mes = ifelse(mes == 8, 'Ago', mes),
        mes = ifelse(mes == 9, 'Sep', mes),
        mes = ifelse(mes == 10, 'Oct', mes),
        mes = ifelse(mes == 11, 'Nov', mes),
        mes = ifelse(mes == 12, 'Dic', mes)) -> data_pred
```

## Rendimientos


```R
data_pred <- rend_func(data_pred, 'avg_gold', 'rend', ind_train = FALSE)
```

## Rezagos


```R
data_pred <- rez_func(data_pred, 'avg_gold', 'rez_avg_gold_1s',  1,  FALSE)
data_pred <- rez_func(data_pred, 'avg_gold', 'rez_avg_gold_2s',  2,  FALSE)
data_pred <- rez_func(data_pred, 'avg_gold', 'rez_avg_gold_3s',  3,  FALSE)
data_pred <- rez_func(data_pred, 'avg_gold', 'rez_avg_gold_4s',  4,  FALSE)
data_pred <- rez_func(data_pred, 'avg_gold', 'rez_avg_gold_6s',  6,  FALSE)
data_pred <- rez_func(data_pred, 'avg_gold', 'rez_avg_gold_8s',  8,  FALSE)
```


```R
data_pred <- rez_func(data_pred, 'std_gold', 'rez_std_gold_1s',  1,  FALSE)
data_pred <- rez_func(data_pred, 'std_gold', 'rez_std_gold_2s',  2,  FALSE)
data_pred <- rez_func(data_pred, 'std_gold', 'rez_std_gold_3s',  3,  FALSE)
data_pred <- rez_func(data_pred, 'std_gold', 'rez_std_gold_4s',  4,  FALSE)
data_pred <- rez_func(data_pred, 'std_gold', 'rez_std_gold_6s',  6,  FALSE)
data_pred <- rez_func(data_pred, 'std_gold', 'rez_std_gold_8s',  8,  FALSE)
```


```R
data_pred <- rez_func(data_pred, 'rend', 'rez_rend_gold_1s',  1,  FALSE)
data_pred <- rez_func(data_pred, 'rend', 'rez_rend_gold_2s',  2,  FALSE)
data_pred <- rez_func(data_pred, 'rend', 'rez_rend_gold_3s',  3,  FALSE)
data_pred <- rez_func(data_pred, 'rend', 'rez_rend_gold_4s',  4,  FALSE)
data_pred <- rez_func(data_pred, 'rend', 'rez_rend_gold_6s',  6,  FALSE)
data_pred <- rez_func(data_pred, 'rend', 'rez_rend_gold_8s',  8,  FALSE)
```

## Medias Móviles


```R
data_pred <- mm_func(data_pred, 'avg_gold', 'mm2_avg_gold', 2, TRUE)
data_pred <- mm_func(data_pred, 'avg_gold', 'mm3_avg_gold', 3, TRUE)
data_pred <- mm_func(data_pred, 'avg_gold', 'mm4_avg_gold', 4, TRUE)

data_pred <- mm_func(data_pred, 'std_gold', 'mm2_std_gold', 2, TRUE)
data_pred <- mm_func(data_pred, 'std_gold', 'mm3_std_gold', 3, TRUE)
data_pred <- mm_func(data_pred, 'std_gold', 'mm4_std_gold', 4, TRUE)

data_pred <- mm_func(data_pred, 'rend', 'mm2_rend', 2, TRUE)
data_pred <- mm_func(data_pred, 'rend', 'mm3_rend', 3, TRUE)
data_pred <- mm_func(data_pred, 'rend', 'mm4_rend', 4, TRUE)
```

## Rachas


```R
data_pred <- racha(data_pred, 'avg_gold', 'rachas_sube_gold', FALSE)
data_pred <- racha(data_pred, 'rez_avg_gold_1s', 'rachas_sube_gold_rez1d', FALSE)
data_pred <- racha(data_pred, 'rez_std_gold_1s', 'rachas_sube_stdgold_rez1d', FALSE)
```

## Dummies mes


```R
data_pred %>%
 mutate(mes = as.factor(mes)) -> data_pred

one_hot(as.data.table(data_pred)) %>%
 as.data.frame -> data_pred
```

## Polinomios


```R
data_pred %>%
 mutate(rez_avg_gold_1s_2 = rez_avg_gold_1s^2,
        rend_2 = rend^2,
        rend_3 = rend^3,
        rachas_sube_gold_2 = rachas_sube_gold^2,
        rez_rend_gold_2s_2 = rez_rend_gold_2s^2,
        rez_avg_gold_8s_2 = rez_avg_gold_8s^2) -> data_pred
```

## Se quitan NAs


```R
data_pred %>%
 na.omit() -> data_pred
```

## Pronosticado vs Observado


```R
options(repr.plot.width=7, repr.plot.height=4)
data_pred %>%
 mutate(pred = predict(m1, data_pred)) %>%
 mutate(avg_gold = lead(avg_gold)) %>%
 ggplot() +
 geom_line(aes(x = max_date, y = avg_gold, group = 1), colour = 'darkblue') +
 geom_line(aes(x = max_date, y = pred, group = 1), colour = 'darkgreen', size = 0.3) +
 theme_bw() +
 ggtitle('Promedio Precio Oro Semanal - Real vs Estimado Histórico') + 
 scale_y_continuous(labels = scales::comma) +
 xlab('Día') +
 ylab('Precio del Oro') +
 theme(axis.text.x = element_text(face = 'bold'),
       axis.text.y = element_text(face = 'bold')) +
 annotate(geom  = 'text',
          x     = as.Date('2015-11-01'),
          y     = 1350,
          label = 'Estimado',
          colour = 'darkgreen',
          hjust = 1) +
 annotate(geom  = 'text',
          x     = as.Date('2015-11-01'),
          y     = 1320,
          label = 'Real',
          colour = 'darkblue',
          hjust = 1.2)
```

    Warning message:
    “Removed 1 rows containing missing values (geom_path).”


![png](output_135_1.png)


## Predicción siguiente semana


```R
sp <- predict(m1, tail(data_pred,1))
paste0('Predicción Promedio Oro siguiente semana',
       ' : ',
       sp
       )
```


'Predicción Promedio Oro siguiente semana : 1276.41576217953'


## Promedio Semana Actual


```R
#sa <- predict(m1, data_pred[nrow(data_pred)-2,])
sa <- tail(data_pred$avg_gold,1)
paste0('Predicción Promedio Oro Semana actual',
       ' : ',
       sa)
```


'Predicción Promedio Oro Semana actual : 1279.02'


## Resumen Pronóstico


```R
# RMSE del modelo en validación
rmse_valid <- rmse(valid$avg_gold, predict(m1, valid))

# Indicador de alta o baja: pronosticado proxima semana vs semana actual
if(sp >= sa){
    a <- 'subida'
    r    <- abs((sp)/(sa))-1
} else {
    a <- 'bajada'
    r    <- 1-abs((sp)/(sa))
}

# Intervalo de confianza inferior
if((sp-rmse_valid)/(sa) >= 1){
    II <- (sp-rmse_valid)/(sa)-1
} else {
    II <- 1-(sp-rmse_valid)/(sa)
}

# Intervalo de confianza superior
if((sp+rmse_valid)/(sa) >= 1){
    IS <- (sp+rmse_valid)/(sa)-1
} else {
    IS <- 1-(sp+rmse_valid)/(sa)
}

# Indicador de alta o baja de intervalo inferior
if(sp-rmse_valid>=sa){
    ai <- 'subir'
} else {
    ai <- 'bajar'
}

# Indicador de alta o baja de intervalo superior
if(sp+rmse_valid>=sa){
    as <- 'subir'
} else {
    as <- 'bajar'
}
```


```R
t0 <- paste0('La semana del ', tail(data_pred$min_date,1), ' al ', tail(data_pred$max_date,1), ' tuvo un precio promedio de: $', tail(data_pred$avg_gold,1), ' dlls')
t1 <- paste0('Para la próxima semana, se pronostica un valor promedio del oro de: $', round(sp,2), ' dlls')
t2 <- paste0('Lo cuál indicaría una ', a, ' del ', round(r*100,2), '% respecto a esta semana')
t3 <- paste0('Pero podría ', ai, ' de ', round(II*100,2), '%', ' a ', as, ' ', round(IS*100,2), '%')
```


```R
data.frame(. = c(t0,t1,t2,t3))
```


<table>
<thead><tr><th scope=col>.</th></tr></thead>
<tbody>
	<tr><td>La semana del 2019-04-29 al 2019-05-03 tuvo un precio promedio de: $1279.02 dlls </td></tr>
	<tr><td>Para la próxima semana, se pronostica un valor promedio del oro de: $1276.42 dlls</td></tr>
	<tr><td>Lo cuál indicaría una bajada del 0.2% respecto a esta semana                     </td></tr>
	<tr><td>Pero podría bajar de 1.9% a subir 1.49%                                          </td></tr>
</tbody>
</table>


