
# Predicción valor del oro semanal

Al parecer, en muchas páginas en las que se descarga el precio del oro diario, se actualizan cada semana, por lo cuál, un modelo que haga una predicción diaria, no será útil.

En este documento se presentará un modelo que hace una predicción del promedio semanal del precio del oro.

$$y_i = \alpha + \beta_i*X_i + \epsilon$$

En dónde:
 - yi = Promedio del precio del oro de la semana _i_
 - Bi = Coeficientes de la regresión
 - Xi = Variables de la regresión

# Fuentes de datos

Se usarán los siguientes datos:
 - **Históricos diarios - Precio Oro en dólares:** https://www.quandl.com/data/WGC/GOLD_DAILY_USD-Gold-Prices-Daily-Currency-USD
 - **Históricos diarios - Predio del dólar:** http://www.anterior.banxico.org.mx/SieInternet/consultarDirectorioInternetAction.do?sector=6&accion=consultarCuadro&idCuadro=CF373&locale=es

Se obtendrán los valores del precio del dólar, con el único de fin de usar las fechas como referencia, ya que el dólar sólo se cotiza en los días hábiles mexicanos.

# Librerías


```R
library(dplyr)
library(ggplot2)
library(readr)
library(forcats)
library(plotly)
library(repr)
library(mltools)
library(data.table)
library(rvest)
library(stringi)
library(zoo)
library(PMwR)
```

    Warning message:
    “package ‘dplyr’ was built under R version 3.5.2”
    Attaching package: ‘dplyr’
    
    The following objects are masked from ‘package:stats’:
    
        filter, lag
    
    The following objects are masked from ‘package:base’:
    
        intersect, setdiff, setequal, union
    
    
    Attaching package: ‘plotly’
    
    The following object is masked from ‘package:ggplot2’:
    
        last_plot
    
    The following object is masked from ‘package:stats’:
    
        filter
    
    The following object is masked from ‘package:graphics’:
    
        layout
    
    Warning message:
    “package ‘repr’ was built under R version 3.5.2”Warning message:
    “package ‘data.table’ was built under R version 3.5.2”
    Attaching package: ‘data.table’
    
    The following objects are masked from ‘package:dplyr’:
    
        between, first, last
    
    Loading required package: xml2
    
    Attaching package: ‘rvest’
    
    The following object is masked from ‘package:readr’:
    
        guess_encoding
    
    Warning message:
    “package ‘stringi’ was built under R version 3.5.2”Warning message:
    “package ‘zoo’ was built under R version 3.5.2”
    Attaching package: ‘zoo’
    
    The following objects are masked from ‘package:base’:
    
        as.Date, as.Date.numeric
    
    Warning message:
    “package ‘PMwR’ was built under R version 3.5.2”

# Lectura de datos

## Históricos oro


```R
gold_price <- read_csv("/Users/darias/Documents/GoldPrice/Data/WGC-GOLD_DAILY_USD.csv")

names(gold_price) <- c('date', 'goldprice')

gold_price %>%
 arrange(date) -> gold_price
```

    Parsed with column specification:
    cols(
      Date = col_date(format = ""),
      Value = col_double()
    )


## Históricos dólar


```R
dolar_price <- read_csv("/Users/darias/Documents/GoldPrice/Data/Consulta_20190502-142917176.csv", 
    skip = 17)

names(dolar_price) <- c('date', 'dolarprice')

dolar_price %>%
 mutate(date = paste0(substr(date,7,10),
                     '-',
                     substr(date,4,5),
                     '-',
                     substr(date,1,2))) %>%
 mutate(date = as.Date(date)) -> dolar_price
```

    Parsed with column specification:
    cols(
      Fecha = col_character(),
      SF63528 = col_double()
    )


# Homologar datos

Se dejarán los datos con las mismas fechas de inicio y fin


```R
max_date <- min(max(gold_price$date), max(dolar_price$date))
min_date <- max(min(gold_price$date), min(dolar_price$date))

dolar_price %>%
 filter(date >= min_date) %>%
 filter(date <= max_date) -> dolar_price

gold_price %>%
 filter(date >= min_date) %>%
 filter(date <= max_date) -> gold_price
```

Se hace de nuevo por si llegó a haber algún desfase


```R
max_date <- min(max(gold_price$date), max(dolar_price$date))
min_date <- max(min(gold_price$date), min(dolar_price$date))

dolar_price %>%
 filter(date >= min_date) %>%
 filter(date <= max_date) -> dolar_price

gold_price %>%
 filter(date >= min_date) %>%
 filter(date <= max_date) -> gold_price
```

# Join de Dólar y Oro

Se pondrán los datos en un único data frame, usando como referencia las fechas de los precios del dólar, ya que respetan los días inhábiles de México

Por lo cuál posteriormente del join, se quitará el valor del dólar


```R
dolar_price %>%
 left_join(gold_price, by = 'date') %>%
 select(-dolarprice) -> data
```

# Limpiar datos

Se quitan datos con NAs, y se dejarán sólo fechas a partir del 2015


```R
data %>%
 filter(!is.na(goldprice)) -> data
```


```R
data %>%
 filter(date >= as.Date('2010-01-01')) -> data
```

# Ingeniería de datos

Se creará:
 - Promedio semanal del precio del oro
 - Desviación estandar semanal del precio del oro
 - Log Rendimientos
 - Rezagos
 - Medias Móviles
 - Rachas
 - Dummies de mes
 - Dummies de número de semana del mes

## Promedio semanal

Se creará el promedio semanal del precio del oro.


```R
data %>%
 mutate(week = paste0(year(date),'-',format(date, '%W'))) %>%
 group_by(week) %>%
 summarise(min_date = min(date),
           max_date = max(date),
           avg_gold = mean(goldprice, na.rm = TRUE),
           std_gold = sd(goldprice, na.rm = TRUE)) %>%
 arrange(as.numeric(gsub('-','',week))) %>%
 filter(!is.na(std_gold)) %>%
 mutate(mes = month(max_date)) %>%
 mutate(mes = ifelse(mes == 1, 'Ene', mes),
        mes = ifelse(mes == 2, 'Feb', mes),
        mes = ifelse(mes == 3, 'Mar', mes),
        mes = ifelse(mes == 4, 'Abr', mes),
        mes = ifelse(mes == 5, 'May', mes),
        mes = ifelse(mes == 6, 'Jun', mes),
        mes = ifelse(mes == 7, 'Jul', mes),
        mes = ifelse(mes == 8, 'Ago', mes),
        mes = ifelse(mes == 9, 'Sep', mes),
        mes = ifelse(mes == 10, 'Oct', mes),
        mes = ifelse(mes == 11, 'Nov', mes),
        mes = ifelse(mes == 12, 'Dic', mes)) -> data
```


```R
head(data)
```


<table>
<thead><tr><th scope=col>week</th><th scope=col>min_date</th><th scope=col>max_date</th><th scope=col>avg_gold</th><th scope=col>std_gold</th><th scope=col>mes</th></tr></thead>
<tbody>
	<tr><td>2010-01   </td><td>2010-01-04</td><td>2010-01-08</td><td>1126.350  </td><td> 3.931444 </td><td>Ene       </td></tr>
	<tr><td>2010-02   </td><td>2010-01-11</td><td>2010-01-15</td><td>1139.550  </td><td>12.289986 </td><td>Ene       </td></tr>
	<tr><td>2010-03   </td><td>2010-01-18</td><td>2010-01-22</td><td>1116.000  </td><td>20.816910 </td><td>Ene       </td></tr>
	<tr><td>2010-04   </td><td>2010-01-25</td><td>2010-01-29</td><td>1089.950  </td><td> 7.016498 </td><td>Ene       </td></tr>
	<tr><td>2010-05   </td><td>2010-02-02</td><td>2010-02-05</td><td>1091.875  </td><td>26.671224 </td><td>Feb       </td></tr>
	<tr><td>2010-06   </td><td>2010-02-08</td><td>2010-02-12</td><td>1072.600  </td><td> 6.838768 </td><td>Feb       </td></tr>
</tbody>
</table>



## Rendimientos de precio del oro

Se obtendrá el _log rendimiento_ del oro, como una variable del modelo.

$$Rend_i = \ln{\frac{PrecioOro_{t+1}}{PrecioOro_{t}}}$$

### Función

Se crea función que agrega una columna con los rendimientos del oro.

Esta función tiene un indicador, el cual funciona para saber si se agrega a un _Data frame_ para entrenar un modelo o para hacer una predicción.

Si es para hacer una predicción, se hace con un _lag_ menos, ya que en una predicción se busca usar los datos más recientes para hacer una predicción a futuro.


```R
rend_func <- function(df, col_ref, nom_col_nueva, ind_train){
  
  # Para predict se quita un lag
  
  if(ind_train == TRUE){
    df %>%
      mutate_(aux1 = col_ref) %>%
      mutate(aux2  = lag(aux1)) %>%
      mutate(aux3  = log(aux2/lag(aux2))) %>%
      select(-aux1, -aux2) -> d 
  } else{
    df %>%
      mutate_(aux1 = col_ref) %>%
      mutate(aux2  = aux1) %>%
      mutate(aux3  = log(aux1/lag(aux2))) %>%
      select(-aux1, -aux2) -> d
  }
  
  names(d)[which(names(d) == 'aux3')] <- nom_col_nueva
  
  return(d)
}
```

### Se agregan rendimientos

Se aplica la función para agregar rendimientos


```R
data <- rend_func(data, 'avg_gold', 'rend', ind_train = TRUE)
```

    Warning message:
    “mutate_() is deprecated. 
    Please use mutate() instead
    
    The 'programming' vignette or the tidyeval book can help you
    to program with mutate() : https://tidyeval.tidyverse.org
    [90mThis warning is displayed once per session.[39m”

## Rezagos

Se agregarán rezagos de:
 - Promedio del precio del oro
 - Desviación estandar del precio del oro
 - Rendimientos del oro
 
A 1,2,3,4,6 y 8 semanas

Se agregará rezafo de 1 semana para el mes

### Función

Se creará una función que agregar una columna con los rezagos a _X_ semanas de una columnas específica.

Tendrá la misma consideración del indicador de si es una _data frame_ para entrenar o para predecir


```R
rez_func <- function(df, col_ref, nom_col_nueva, num_rezagos, ind_train){
  # Cuando es para predict, se quita un lag
  if(ind_train == TRUE){
    # Se crea vector con código de número de lags a hacer y sus paréntesis de cierre para train
    num_lags  <- rep('lag(',num_rezagos)
    close_par <- rep(')', num_rezagos)
  } else {
    # Se crea vector con código de número de lags a hacer y sus paréntesis de cierre para predict
    num_lags  <- rep('lag(',num_rezagos-1)
    close_par <- rep(')', num_rezagos-1)
  }
  
  # Se convierten a un sólo caracter
  num_lags  <- stri_paste(num_lags, collapse = '')
  close_par <- stri_paste(close_par, collapse = '')
  
  # Se pega el código
  cod <- paste0(num_lags, 'aux', close_par)
  
  # Se crea la columna nueva
  df %>%
    mutate_(aux = col_ref) %>%
    mutate_(aux = cod) -> d
  
  names(d)[which(names(d) == 'aux')] <- nom_col_nueva
  
  return(d)
}
```

### Agregan Rezagos


```R
data <- rez_func(data, 'avg_gold', 'rez_avg_gold_1s',  1,  TRUE)
data <- rez_func(data, 'avg_gold', 'rez_avg_gold_2s',  2,  TRUE)
data <- rez_func(data, 'avg_gold', 'rez_avg_gold_3s',  3,  TRUE)
data <- rez_func(data, 'avg_gold', 'rez_avg_gold_4s',  4,  TRUE)
data <- rez_func(data, 'avg_gold', 'rez_avg_gold_6s',  6,  TRUE)
data <- rez_func(data, 'avg_gold', 'rez_avg_gold_8s',  8,  TRUE)
```


```R
data <- rez_func(data, 'std_gold', 'rez_std_gold_1s',  1,  TRUE)
data <- rez_func(data, 'std_gold', 'rez_std_gold_2s',  2,  TRUE)
data <- rez_func(data, 'std_gold', 'rez_std_gold_3s',  3,  TRUE)
data <- rez_func(data, 'std_gold', 'rez_std_gold_4s',  4,  TRUE)
data <- rez_func(data, 'std_gold', 'rez_std_gold_6s',  6,  TRUE)
data <- rez_func(data, 'std_gold', 'rez_std_gold_8s',  8,  TRUE)
```


```R
data <- rez_func(data, 'rend', 'rez_rend_gold_1s',  1,  TRUE)
data <- rez_func(data, 'rend', 'rez_rend_gold_2s',  2,  TRUE)
data <- rez_func(data, 'rend', 'rez_rend_gold_3s',  3,  TRUE)
data <- rez_func(data, 'rend', 'rez_rend_gold_4s',  4,  TRUE)
data <- rez_func(data, 'rend', 'rez_rend_gold_6s',  6,  TRUE)
data <- rez_func(data, 'rend', 'rez_rend_gold_8s',  8,  TRUE)
```


```R
data %>%
 mutate(mes = lag(mes)) -> data
```

## Medias Móviles

Se obtendrán las medias móviles a diferentes tiempos para:
 - Promedio de precio de oro
 - Desviación estandar del precio del oro
 - Rendimiento del oro
 
Con el indicador si para _train_ o _predict_ el _Data frame_

Media Móvil de una variable de _T_ tiempos

$$MediaMóvil_i = \frac{1}{n}\sum_{i=1}^{n} Variable_{t-i}$$

### Función


```R
mm_func <- function(df, col_ref, nom_col_nueva, t, ind_train){
  # Cuando es para predict se quita un lag para calcular las medias móviles
  if(ind_train == TRUE){
    # Se crea el vector de medias móviles a "t" tiempos para train
    df %>%
      mutate_(aux = col_ref) %>%
      mutate(aux = lag(aux)) %>%
      select(aux) %>%
      pull() %>%
      rollmean(x = ., k = t) -> mm
  } else{
    # Se crea el vector de medias móviles a "t" tiempos para predict
    df %>%
      mutate_(aux = col_ref) %>%
      mutate(aux = aux) %>%
      select(aux) %>%
      pull() %>%
      rollmean(x = ., k = t) -> mm
    
  }
  
  # Para poder agregar la columna, se deben agregar NA's al principio para que coincida el número de columnas
  mm <- c(rep(NA,t-1), mm)
  
  # Se agrega el vector de medias móviles al df
  df %>%
    mutate(aux = mm) -> d
  
  names(d)[which(names(d) == 'aux')] <- nom_col_nueva
  
  return(d)
}
```

### Agregan Medias Móviles


```R
data <- mm_func(data, 'avg_gold', 'mm2_avg_gold', 2, TRUE)
data <- mm_func(data, 'avg_gold', 'mm3_avg_gold', 3, TRUE)
data <- mm_func(data, 'avg_gold', 'mm4_avg_gold', 4, TRUE)

data <- mm_func(data, 'std_gold', 'mm2_std_gold', 2, TRUE)
data <- mm_func(data, 'std_gold', 'mm3_std_gold', 3, TRUE)
data <- mm_func(data, 'std_gold', 'mm4_std_gold', 4, TRUE)

data <- mm_func(data, 'rend', 'mm2_rend', 2, TRUE)
data <- mm_func(data, 'rend', 'mm3_rend', 3, TRUE)
data <- mm_func(data, 'rend', 'mm4_rend', 4, TRUE)
```

## Rachas

Se hará una función que obtenga las rachas de rendimiento positivo, que agregue una columna con el número de semanas que ha subido el dólar de manera constante.

### Función


```R
racha <- function(df, col_ref, nom_col_nueva, ind_train){
  # Cuando es para predict se quita un lag para calcular las medias móviles
  if(ind_train == TRUE){
    df %>%
      mutate_(aux1 = col_ref) %>%
      mutate(aux2  = lag(aux1)) %>%
      mutate(aux3  = aux2-lag(aux2)) %>%
      mutate(ind = ifelse(aux3 <= 0, FALSE, TRUE)) %>%
      group_by(grp = rleid(ind)) %>%
      mutate(n = rank(min_date)) %>%
      mutate(n = ifelse(ind == FALSE, 0, n)) %>%
      data.frame() %>%
      mutate(racha = lag(n)) %>%
      select(-aux1, -aux2, -aux3, -n, -grp, -ind) -> d
  } else {
      df %>%
       mutate_(aux1 = col_ref) %>%
       mutate(aux2  = aux1) %>%
       mutate(aux3  = aux1-lag(aux2)) %>%
       mutate(ind = ifelse(aux3 <= 0, FALSE, TRUE)) %>%
       group_by(grp = rleid(ind)) %>%
       mutate(n = rank(min_date)) %>%
       mutate(n = ifelse(ind == FALSE, 0, n)) %>%
       data.frame() %>%
       mutate(racha = lag(n)) %>%
       select(-aux1, -aux2, -aux3, -n, -grp, -ind) -> d 
  }

    names(d)[which(names(d) == 'racha')] <- nom_col_nueva
    
    return(d)
    
}
```


```R
data <- racha(data, 'avg_gold', 'rachas_sube_gold', TRUE)
data <- racha(data, 'rez_avg_gold_1s', 'rachas_sube_gold_rez1d', TRUE)
data <- racha(data, 'rez_std_gold_1s', 'rachas_sube_stdgold_rez1d', TRUE)
```

## Dummies mes

Se agregan dummies de mes del año


```R
data %>%
 mutate(mes = as.factor(mes)) -> data

one_hot(as.data.table(data)) %>%
 as.data.frame -> data
```

Se quita una dummie para que la matriz sea invertible


```R
data$mes_Ene <- NULL
```

## Cuadrados

Se agrega el rezago de 1s al cuadrado para:
 - Promedio semanal del Precio del oro
 - Desviación estanmdar del Precio del oro


```R
data %>%
 mutate(rez_avg_gold_1s_2 = rez_avg_gold_1s^2,
        rend_2 = rend^2,
        rachas_sube_gold_2 = rachas_sube_gold^2) -> data
```

## Se quitan NAs


```R
data %>%
 filter(!is.na(rez_avg_gold_8s)) %>%
 filter(!is.na(rez_rend_gold_8s)) -> data
```


# Descriptivos

## Serie de tiempo histórica


```R
options(repr.plot.width=7, repr.plot.height=4)
data %>%
 ggplot() +
 geom_line(aes(x = max_date, y = avg_gold, group = 1), colour = 'darkblue') +
 theme_bw() +
 ggtitle('Precio del oro - Promedio Semanal') +
 theme(axis.text.x = element_text(face = 'bold'),
       axis.text.y = element_text(face = 'bold')) +
 scale_y_continuous(labels = scales::comma) +
 xlab('semana') +
 ylab('$ dlls')
```


![png](output_79_0.png)


## Correlaciones

Se obtendrán las correlaciones entre las variables de medias móviles, rezagos y rendimientos


```R
options(repr.plot.width=8, repr.plot.height=8)
data %>%
  select(-grep('mes', names(data))) %>%
 select(-week, -min_date, -max_date, -std_gold) %>%
 cor() %>%
 heatmap(main = 'Heatmap - Correlaciones')

```


![png](output_82_0.png)


# Modelo

Se hará un modelo de regresión lineal para estimar el valor del oro

$$PromedioPrecioOro_i = \alpha + \eta_1*MM2Oro_i + \eta_2*MM3Oro_i + \eta_3*MM4Oro_i + \eta_4*MM2RO_i + \eta_5*MM3RO_i + \eta_6*MM4RO_i + \eta_7*MM2SO_i + \eta8*MM3SO_i + \eta_9*MM4SO_i + \sum_j^{j\in\{1,2,3,4,6,8\}} \beta_j*RO_{j_i} + \sum_j^{j\in\{1,2,3,4,6,8\}} \delta_j*RezagoOro_{j_i} + \sum_j^{j\in\{1,2,3,4,6,8\}} \delta_j*RezagoSO_{j_i} + \sigma_j*DummieMes_{j_i} + \phi_1*RachaPO_i + \phi_2*RachaRO1_i + \phi_3*RachaRSD_i + \epsilon_i$$

Posteriormente se ajustará con un _stepwise_

Dónde:
 - **RO:** Rendimiento del oro
 - **MMi:** Media Móvile a "i" unidades de tiempo
 - **SO:** Desviación estándard del oro

## Fit

Se ajusta la regresión lineal


```R
set.seed(50)
indices <- sample(1:nrow(data), nrow(data)*0.8)
train <- data[indices,]
valid <- data[-indices,]
```


```R
names(data)
```


<ol class=list-inline>
	<li>'week'</li>
	<li>'min_date'</li>
	<li>'max_date'</li>
	<li>'avg_gold'</li>
	<li>'std_gold'</li>
	<li>'mes_Abr'</li>
	<li>'mes_Ago'</li>
	<li>'mes_Dic'</li>
	<li>'mes_Feb'</li>
	<li>'mes_Jul'</li>
	<li>'mes_Jun'</li>
	<li>'mes_Mar'</li>
	<li>'mes_May'</li>
	<li>'mes_Nov'</li>
	<li>'mes_Oct'</li>
	<li>'mes_Sep'</li>
	<li>'rend'</li>
	<li>'rez_avg_gold_1s'</li>
	<li>'rez_avg_gold_2s'</li>
	<li>'rez_avg_gold_3s'</li>
	<li>'rez_avg_gold_4s'</li>
	<li>'rez_avg_gold_6s'</li>
	<li>'rez_avg_gold_8s'</li>
	<li>'rez_std_gold_1s'</li>
	<li>'rez_std_gold_2s'</li>
	<li>'rez_std_gold_3s'</li>
	<li>'rez_std_gold_4s'</li>
	<li>'rez_std_gold_6s'</li>
	<li>'rez_std_gold_8s'</li>
	<li>'rez_rend_gold_1s'</li>
	<li>'rez_rend_gold_2s'</li>
	<li>'rez_rend_gold_3s'</li>
	<li>'rez_rend_gold_4s'</li>
	<li>'rez_rend_gold_6s'</li>
	<li>'rez_rend_gold_8s'</li>
	<li>'mm2_avg_gold'</li>
	<li>'mm3_avg_gold'</li>
	<li>'mm4_avg_gold'</li>
	<li>'mm2_std_gold'</li>
	<li>'mm3_std_gold'</li>
	<li>'mm4_std_gold'</li>
	<li>'mm2_rend'</li>
	<li>'mm3_rend'</li>
	<li>'mm4_rend'</li>
	<li>'rachas_sube_gold'</li>
	<li>'rachas_sube_gold_rez1d'</li>
	<li>'rachas_sube_stdgold_rez1d'</li>
	<li>'rez_avg_gold_1s_2'</li>
	<li>'rend_2'</li>
	<li>'rachas_sube_gold_2'</li>
</ol>




```R
m1 <- lm(avg_gold ~ ., data = train %>% select(-week, -min_date, -max_date, -std_gold))
```


```R
m1 <- step(m1)
```

    Start:  AIC=2469.69
    avg_gold ~ mes_Abr + mes_Ago + mes_Dic + mes_Feb + mes_Jul + 
        mes_Jun + mes_Mar + mes_May + mes_Nov + mes_Oct + mes_Sep + 
        rend + rez_avg_gold_1s + rez_avg_gold_2s + rez_avg_gold_3s + 
        rez_avg_gold_4s + rez_avg_gold_6s + rez_avg_gold_8s + rez_std_gold_1s + 
        rez_std_gold_2s + rez_std_gold_3s + rez_std_gold_4s + rez_std_gold_6s + 
        rez_std_gold_8s + rez_rend_gold_1s + rez_rend_gold_2s + rez_rend_gold_3s + 
        rez_rend_gold_4s + rez_rend_gold_6s + rez_rend_gold_8s + 
        mm2_avg_gold + mm3_avg_gold + mm4_avg_gold + mm2_std_gold + 
        mm3_std_gold + mm4_std_gold + mm2_rend + mm3_rend + mm4_rend + 
        rachas_sube_gold + rachas_sube_gold_rez1d + rachas_sube_stdgold_rez1d + 
        rez_avg_gold_1s_2 + rend_2 + rachas_sube_gold_2
    
    
    Step:  AIC=2469.69
    avg_gold ~ mes_Abr + mes_Ago + mes_Dic + mes_Feb + mes_Jul + 
        mes_Jun + mes_Mar + mes_May + mes_Nov + mes_Oct + mes_Sep + 
        rend + rez_avg_gold_1s + rez_avg_gold_2s + rez_avg_gold_3s + 
        rez_avg_gold_4s + rez_avg_gold_6s + rez_avg_gold_8s + rez_std_gold_1s + 
        rez_std_gold_2s + rez_std_gold_3s + rez_std_gold_4s + rez_std_gold_6s + 
        rez_std_gold_8s + rez_rend_gold_1s + rez_rend_gold_2s + rez_rend_gold_3s + 
        rez_rend_gold_4s + rez_rend_gold_6s + rez_rend_gold_8s + 
        mm2_avg_gold + mm3_avg_gold + mm4_avg_gold + mm2_std_gold + 
        mm3_std_gold + mm4_std_gold + mm2_rend + mm3_rend + rachas_sube_gold + 
        rachas_sube_gold_rez1d + rachas_sube_stdgold_rez1d + rez_avg_gold_1s_2 + 
        rend_2 + rachas_sube_gold_2
    
    
    Step:  AIC=2469.69
    avg_gold ~ mes_Abr + mes_Ago + mes_Dic + mes_Feb + mes_Jul + 
        mes_Jun + mes_Mar + mes_May + mes_Nov + mes_Oct + mes_Sep + 
        rend + rez_avg_gold_1s + rez_avg_gold_2s + rez_avg_gold_3s + 
        rez_avg_gold_4s + rez_avg_gold_6s + rez_avg_gold_8s + rez_std_gold_1s + 
        rez_std_gold_2s + rez_std_gold_3s + rez_std_gold_4s + rez_std_gold_6s + 
        rez_std_gold_8s + rez_rend_gold_1s + rez_rend_gold_2s + rez_rend_gold_3s + 
        rez_rend_gold_4s + rez_rend_gold_6s + rez_rend_gold_8s + 
        mm2_avg_gold + mm3_avg_gold + mm4_avg_gold + mm2_std_gold + 
        mm3_std_gold + mm4_std_gold + mm2_rend + rachas_sube_gold + 
        rachas_sube_gold_rez1d + rachas_sube_stdgold_rez1d + rez_avg_gold_1s_2 + 
        rend_2 + rachas_sube_gold_2
    
    
    Step:  AIC=2469.69
    avg_gold ~ mes_Abr + mes_Ago + mes_Dic + mes_Feb + mes_Jul + 
        mes_Jun + mes_Mar + mes_May + mes_Nov + mes_Oct + mes_Sep + 
        rend + rez_avg_gold_1s + rez_avg_gold_2s + rez_avg_gold_3s + 
        rez_avg_gold_4s + rez_avg_gold_6s + rez_avg_gold_8s + rez_std_gold_1s + 
        rez_std_gold_2s + rez_std_gold_3s + rez_std_gold_4s + rez_std_gold_6s + 
        rez_std_gold_8s + rez_rend_gold_1s + rez_rend_gold_2s + rez_rend_gold_3s + 
        rez_rend_gold_4s + rez_rend_gold_6s + rez_rend_gold_8s + 
        mm2_avg_gold + mm3_avg_gold + mm4_avg_gold + mm2_std_gold + 
        mm3_std_gold + mm4_std_gold + rachas_sube_gold + rachas_sube_gold_rez1d + 
        rachas_sube_stdgold_rez1d + rez_avg_gold_1s_2 + rend_2 + 
        rachas_sube_gold_2
    
    
    Step:  AIC=2469.69
    avg_gold ~ mes_Abr + mes_Ago + mes_Dic + mes_Feb + mes_Jul + 
        mes_Jun + mes_Mar + mes_May + mes_Nov + mes_Oct + mes_Sep + 
        rend + rez_avg_gold_1s + rez_avg_gold_2s + rez_avg_gold_3s + 
        rez_avg_gold_4s + rez_avg_gold_6s + rez_avg_gold_8s + rez_std_gold_1s + 
        rez_std_gold_2s + rez_std_gold_3s + rez_std_gold_4s + rez_std_gold_6s + 
        rez_std_gold_8s + rez_rend_gold_1s + rez_rend_gold_2s + rez_rend_gold_3s + 
        rez_rend_gold_4s + rez_rend_gold_6s + rez_rend_gold_8s + 
        mm2_avg_gold + mm3_avg_gold + mm4_avg_gold + mm2_std_gold + 
        mm3_std_gold + rachas_sube_gold + rachas_sube_gold_rez1d + 
        rachas_sube_stdgold_rez1d + rez_avg_gold_1s_2 + rend_2 + 
        rachas_sube_gold_2
    
    
    Step:  AIC=2469.69
    avg_gold ~ mes_Abr + mes_Ago + mes_Dic + mes_Feb + mes_Jul + 
        mes_Jun + mes_Mar + mes_May + mes_Nov + mes_Oct + mes_Sep + 
        rend + rez_avg_gold_1s + rez_avg_gold_2s + rez_avg_gold_3s + 
        rez_avg_gold_4s + rez_avg_gold_6s + rez_avg_gold_8s + rez_std_gold_1s + 
        rez_std_gold_2s + rez_std_gold_3s + rez_std_gold_4s + rez_std_gold_6s + 
        rez_std_gold_8s + rez_rend_gold_1s + rez_rend_gold_2s + rez_rend_gold_3s + 
        rez_rend_gold_4s + rez_rend_gold_6s + rez_rend_gold_8s + 
        mm2_avg_gold + mm3_avg_gold + mm4_avg_gold + mm2_std_gold + 
        rachas_sube_gold + rachas_sube_gold_rez1d + rachas_sube_stdgold_rez1d + 
        rez_avg_gold_1s_2 + rend_2 + rachas_sube_gold_2
    
    
    Step:  AIC=2469.69
    avg_gold ~ mes_Abr + mes_Ago + mes_Dic + mes_Feb + mes_Jul + 
        mes_Jun + mes_Mar + mes_May + mes_Nov + mes_Oct + mes_Sep + 
        rend + rez_avg_gold_1s + rez_avg_gold_2s + rez_avg_gold_3s + 
        rez_avg_gold_4s + rez_avg_gold_6s + rez_avg_gold_8s + rez_std_gold_1s + 
        rez_std_gold_2s + rez_std_gold_3s + rez_std_gold_4s + rez_std_gold_6s + 
        rez_std_gold_8s + rez_rend_gold_1s + rez_rend_gold_2s + rez_rend_gold_3s + 
        rez_rend_gold_4s + rez_rend_gold_6s + rez_rend_gold_8s + 
        mm2_avg_gold + mm3_avg_gold + mm4_avg_gold + rachas_sube_gold + 
        rachas_sube_gold_rez1d + rachas_sube_stdgold_rez1d + rez_avg_gold_1s_2 + 
        rend_2 + rachas_sube_gold_2
    
    
    Step:  AIC=2469.69
    avg_gold ~ mes_Abr + mes_Ago + mes_Dic + mes_Feb + mes_Jul + 
        mes_Jun + mes_Mar + mes_May + mes_Nov + mes_Oct + mes_Sep + 
        rend + rez_avg_gold_1s + rez_avg_gold_2s + rez_avg_gold_3s + 
        rez_avg_gold_4s + rez_avg_gold_6s + rez_avg_gold_8s + rez_std_gold_1s + 
        rez_std_gold_2s + rez_std_gold_3s + rez_std_gold_4s + rez_std_gold_6s + 
        rez_std_gold_8s + rez_rend_gold_1s + rez_rend_gold_2s + rez_rend_gold_3s + 
        rez_rend_gold_4s + rez_rend_gold_6s + rez_rend_gold_8s + 
        mm2_avg_gold + mm3_avg_gold + rachas_sube_gold + rachas_sube_gold_rez1d + 
        rachas_sube_stdgold_rez1d + rez_avg_gold_1s_2 + rend_2 + 
        rachas_sube_gold_2
    
    
    Step:  AIC=2469.69
    avg_gold ~ mes_Abr + mes_Ago + mes_Dic + mes_Feb + mes_Jul + 
        mes_Jun + mes_Mar + mes_May + mes_Nov + mes_Oct + mes_Sep + 
        rend + rez_avg_gold_1s + rez_avg_gold_2s + rez_avg_gold_3s + 
        rez_avg_gold_4s + rez_avg_gold_6s + rez_avg_gold_8s + rez_std_gold_1s + 
        rez_std_gold_2s + rez_std_gold_3s + rez_std_gold_4s + rez_std_gold_6s + 
        rez_std_gold_8s + rez_rend_gold_1s + rez_rend_gold_2s + rez_rend_gold_3s + 
        rez_rend_gold_4s + rez_rend_gold_6s + rez_rend_gold_8s + 
        mm2_avg_gold + rachas_sube_gold + rachas_sube_gold_rez1d + 
        rachas_sube_stdgold_rez1d + rez_avg_gold_1s_2 + rend_2 + 
        rachas_sube_gold_2
    
    
    Step:  AIC=2469.69
    avg_gold ~ mes_Abr + mes_Ago + mes_Dic + mes_Feb + mes_Jul + 
        mes_Jun + mes_Mar + mes_May + mes_Nov + mes_Oct + mes_Sep + 
        rend + rez_avg_gold_1s + rez_avg_gold_2s + rez_avg_gold_3s + 
        rez_avg_gold_4s + rez_avg_gold_6s + rez_avg_gold_8s + rez_std_gold_1s + 
        rez_std_gold_2s + rez_std_gold_3s + rez_std_gold_4s + rez_std_gold_6s + 
        rez_std_gold_8s + rez_rend_gold_1s + rez_rend_gold_2s + rez_rend_gold_3s + 
        rez_rend_gold_4s + rez_rend_gold_6s + rez_rend_gold_8s + 
        rachas_sube_gold + rachas_sube_gold_rez1d + rachas_sube_stdgold_rez1d + 
        rez_avg_gold_1s_2 + rend_2 + rachas_sube_gold_2
    
                                Df Sum of Sq    RSS    AIC
    - mes_Feb                    1       0.1 204986 2467.7
    - rez_rend_gold_8s           1       6.6 204992 2467.7
    - rez_avg_gold_2s            1      12.1 204998 2467.7
    - rachas_sube_gold_rez1d     1      19.4 205005 2467.7
    - rez_std_gold_3s            1      30.1 205016 2467.7
    - rend                       1      59.3 205045 2467.8
    - rez_avg_gold_1s_2          1      59.8 205045 2467.8
    - mes_Ago                    1      64.2 205050 2467.8
    - mes_Jun                    1     210.5 205196 2468.1
    - mes_Oct                    1     320.4 205306 2468.3
    - rez_avg_gold_3s            1     328.2 205314 2468.3
    - rez_rend_gold_1s           1     392.9 205378 2468.4
    - rez_std_gold_2s            1     403.2 205389 2468.4
    - rez_std_gold_4s            1     405.2 205391 2468.4
    - mes_Abr                    1     444.4 205430 2468.5
    - mes_Jul                    1     508.0 205494 2468.6
    - rez_std_gold_8s            1     554.9 205541 2468.7
    - rez_rend_gold_6s           1     571.3 205557 2468.8
    - mes_Dic                    1     850.4 205836 2469.3
    - mes_Mar                    1    1015.1 206001 2469.6
    <none>                                   204986 2469.7
    - mes_Sep                    1    1091.1 206077 2469.7
    - rachas_sube_gold_2         1    1101.4 206087 2469.7
    - rez_avg_gold_6s            1    1160.5 206146 2469.8
    - rachas_sube_stdgold_rez1d  1    1343.9 206330 2470.2
    - rez_rend_gold_4s           1    1587.3 206573 2470.6
    - rez_rend_gold_3s           1    1608.6 206594 2470.7
    - rez_rend_gold_2s           1    1641.1 206627 2470.7
    - rachas_sube_gold           1    1765.6 206751 2471.0
    - rez_avg_gold_8s            1    2017.7 207003 2471.4
    - mes_Nov                    1    2065.3 207051 2471.5
    - mes_May                    1    2511.3 207497 2472.3
    - rez_std_gold_1s            1    2992.4 207978 2473.2
    - rez_avg_gold_4s            1    3234.5 208220 2473.7
    - rez_avg_gold_1s            1    3679.1 208665 2474.5
    - rez_std_gold_6s            1    5542.0 210528 2477.8
    - rend_2                     1   10895.2 215881 2487.4
    
    Step:  AIC=2467.69
    avg_gold ~ mes_Abr + mes_Ago + mes_Dic + mes_Jul + mes_Jun + 
        mes_Mar + mes_May + mes_Nov + mes_Oct + mes_Sep + rend + 
        rez_avg_gold_1s + rez_avg_gold_2s + rez_avg_gold_3s + rez_avg_gold_4s + 
        rez_avg_gold_6s + rez_avg_gold_8s + rez_std_gold_1s + rez_std_gold_2s + 
        rez_std_gold_3s + rez_std_gold_4s + rez_std_gold_6s + rez_std_gold_8s + 
        rez_rend_gold_1s + rez_rend_gold_2s + rez_rend_gold_3s + 
        rez_rend_gold_4s + rez_rend_gold_6s + rez_rend_gold_8s + 
        rachas_sube_gold + rachas_sube_gold_rez1d + rachas_sube_stdgold_rez1d + 
        rez_avg_gold_1s_2 + rend_2 + rachas_sube_gold_2
    
                                Df Sum of Sq    RSS    AIC
    - rez_rend_gold_8s           1       6.6 204992 2465.7
    - rez_avg_gold_2s            1      12.3 204998 2465.7
    - rachas_sube_gold_rez1d     1      19.3 205005 2465.7
    - rez_std_gold_3s            1      30.0 205016 2465.7
    - rend                       1      59.5 205045 2465.8
    - rez_avg_gold_1s_2          1      59.7 205045 2465.8
    - mes_Ago                    1      84.8 205071 2465.8
    - mes_Jun                    1     291.3 205277 2466.2
    - rez_avg_gold_3s            1     328.2 205314 2466.3
    - rez_rend_gold_1s           1     395.0 205381 2466.4
    - rez_std_gold_2s            1     403.5 205389 2466.4
    - rez_std_gold_4s            1     406.7 205392 2466.4
    - mes_Oct                    1     441.8 205428 2466.5
    - rez_std_gold_8s            1     562.3 205548 2466.7
    - rez_rend_gold_6s           1     571.5 205557 2466.8
    - mes_Abr                    1     617.8 205604 2466.8
    - mes_Jul                    1     699.8 205686 2467.0
    <none>                                   204986 2467.7
    - rachas_sube_gold_2         1    1101.4 206087 2467.7
    - mes_Dic                    1    1144.1 206130 2467.8
    - rez_avg_gold_6s            1    1161.7 206147 2467.8
    - rachas_sube_stdgold_rez1d  1    1343.8 206330 2468.2
    - mes_Mar                    1    1441.7 206427 2468.4
    - mes_Sep                    1    1487.0 206473 2468.4
    - rez_rend_gold_4s           1    1588.9 206575 2468.6
    - rez_rend_gold_3s           1    1611.4 206597 2468.7
    - rez_rend_gold_2s           1    1641.5 206627 2468.7
    - rachas_sube_gold           1    1768.2 206754 2469.0
    - rez_avg_gold_8s            1    2036.2 207022 2469.4
    - mes_Nov                    1    2784.9 207771 2470.8
    - rez_std_gold_1s            1    2992.8 207978 2471.2
    - rez_avg_gold_4s            1    3235.4 208221 2471.7
    - mes_May                    1    3302.8 208289 2471.8
    - rez_avg_gold_1s            1    3679.0 208665 2472.5
    - rez_std_gold_6s            1    5550.8 210537 2475.9
    - rend_2                     1   10907.7 215893 2485.4
    
    Step:  AIC=2465.7
    avg_gold ~ mes_Abr + mes_Ago + mes_Dic + mes_Jul + mes_Jun + 
        mes_Mar + mes_May + mes_Nov + mes_Oct + mes_Sep + rend + 
        rez_avg_gold_1s + rez_avg_gold_2s + rez_avg_gold_3s + rez_avg_gold_4s + 
        rez_avg_gold_6s + rez_avg_gold_8s + rez_std_gold_1s + rez_std_gold_2s + 
        rez_std_gold_3s + rez_std_gold_4s + rez_std_gold_6s + rez_std_gold_8s + 
        rez_rend_gold_1s + rez_rend_gold_2s + rez_rend_gold_3s + 
        rez_rend_gold_4s + rez_rend_gold_6s + rachas_sube_gold + 
        rachas_sube_gold_rez1d + rachas_sube_stdgold_rez1d + rez_avg_gold_1s_2 + 
        rend_2 + rachas_sube_gold_2
    
                                Df Sum of Sq    RSS    AIC
    - rez_avg_gold_2s            1      11.3 205004 2463.7
    - rachas_sube_gold_rez1d     1      20.0 205012 2463.7
    - rez_std_gold_3s            1      27.9 205020 2463.8
    - rend                       1      57.6 205050 2463.8
    - rez_avg_gold_1s_2          1      58.7 205051 2463.8
    - mes_Ago                    1      87.1 205079 2463.9
    - mes_Jun                    1     288.6 205281 2464.2
    - rez_avg_gold_3s            1     323.3 205316 2464.3
    - rez_rend_gold_1s           1     391.9 205384 2464.4
    - rez_std_gold_4s            1     407.3 205400 2464.5
    - rez_std_gold_2s            1     418.1 205410 2464.5
    - mes_Oct                    1     435.3 205428 2464.5
    - rez_std_gold_8s            1     555.7 205548 2464.7
    - rez_rend_gold_6s           1     566.6 205559 2464.8
    - mes_Abr                    1     611.2 205604 2464.8
    - mes_Jul                    1     698.5 205691 2465.0
    <none>                                   204992 2465.7
    - rachas_sube_gold_2         1    1095.0 206087 2465.7
    - mes_Dic                    1    1142.4 206135 2465.8
    - rez_avg_gold_6s            1    1163.2 206155 2465.9
    - rachas_sube_stdgold_rez1d  1    1356.1 206348 2466.2
    - mes_Mar                    1    1437.4 206430 2466.4
    - mes_Sep                    1    1480.8 206473 2466.4
    - rez_rend_gold_4s           1    1586.5 206579 2466.6
    - rez_rend_gold_3s           1    1610.5 206603 2466.7
    - rez_rend_gold_2s           1    1646.6 206639 2466.8
    - rachas_sube_gold           1    1761.9 206754 2467.0
    - rez_avg_gold_8s            1    2031.6 207024 2467.5
    - mes_Nov                    1    2779.4 207772 2468.8
    - rez_std_gold_1s            1    2995.3 207988 2469.2
    - rez_avg_gold_4s            1    3247.9 208240 2469.7
    - mes_May                    1    3315.6 208308 2469.8
    - rez_avg_gold_1s            1    3710.9 208703 2470.5
    - rez_std_gold_6s            1    5544.9 210537 2473.9
    - rend_2                     1   10946.1 215938 2483.5
    
    Step:  AIC=2463.72
    avg_gold ~ mes_Abr + mes_Ago + mes_Dic + mes_Jul + mes_Jun + 
        mes_Mar + mes_May + mes_Nov + mes_Oct + mes_Sep + rend + 
        rez_avg_gold_1s + rez_avg_gold_3s + rez_avg_gold_4s + rez_avg_gold_6s + 
        rez_avg_gold_8s + rez_std_gold_1s + rez_std_gold_2s + rez_std_gold_3s + 
        rez_std_gold_4s + rez_std_gold_6s + rez_std_gold_8s + rez_rend_gold_1s + 
        rez_rend_gold_2s + rez_rend_gold_3s + rez_rend_gold_4s + 
        rez_rend_gold_6s + rachas_sube_gold + rachas_sube_gold_rez1d + 
        rachas_sube_stdgold_rez1d + rez_avg_gold_1s_2 + rend_2 + 
        rachas_sube_gold_2
    
                                Df Sum of Sq    RSS    AIC
    - rachas_sube_gold_rez1d     1      21.2 205025 2461.8
    - rez_std_gold_3s            1      28.2 205032 2461.8
    - rez_avg_gold_1s_2          1      62.0 205066 2461.8
    - rend                       1      62.0 205066 2461.8
    - mes_Ago                    1      90.1 205094 2461.9
    - mes_Jun                    1     285.3 205289 2462.2
    - rez_std_gold_2s            1     408.4 205412 2462.5
    - mes_Oct                    1     436.0 205440 2462.5
    - rez_std_gold_4s            1     441.1 205445 2462.5
    - rez_std_gold_8s            1     545.2 205549 2462.7
    - rez_rend_gold_6s           1     568.6 205572 2462.8
    - mes_Abr                    1     613.0 205617 2462.9
    - rez_avg_gold_3s            1     673.7 205677 2463.0
    - mes_Jul                    1     700.2 205704 2463.0
    - rez_rend_gold_1s           1     802.2 205806 2463.2
    <none>                                   205004 2463.7
    - rachas_sube_gold_2         1    1086.0 206090 2463.7
    - mes_Dic                    1    1131.5 206135 2463.8
    - rez_avg_gold_6s            1    1164.8 206168 2463.9
    - rachas_sube_stdgold_rez1d  1    1352.7 206356 2464.2
    - mes_Mar                    1    1433.9 206437 2464.4
    - mes_Sep                    1    1471.4 206475 2464.4
    - rez_rend_gold_4s           1    1598.0 206602 2464.7
    - rez_rend_gold_3s           1    1606.9 206611 2464.7
    - rez_rend_gold_2s           1    1686.7 206690 2464.8
    - rachas_sube_gold           1    1752.0 206756 2465.0
    - rez_avg_gold_8s            1    2024.6 207028 2465.5
    - mes_Nov                    1    2770.2 207774 2466.8
    - rez_std_gold_1s            1    3031.6 208035 2467.3
    - rez_avg_gold_4s            1    3303.5 208307 2467.8
    - mes_May                    1    3309.1 208313 2467.8
    - rez_std_gold_6s            1    5549.9 210553 2471.9
    - rez_avg_gold_1s            1    8006.6 213010 2476.3
    - rend_2                     1   11366.7 216370 2482.3
    
    Step:  AIC=2461.76
    avg_gold ~ mes_Abr + mes_Ago + mes_Dic + mes_Jul + mes_Jun + 
        mes_Mar + mes_May + mes_Nov + mes_Oct + mes_Sep + rend + 
        rez_avg_gold_1s + rez_avg_gold_3s + rez_avg_gold_4s + rez_avg_gold_6s + 
        rez_avg_gold_8s + rez_std_gold_1s + rez_std_gold_2s + rez_std_gold_3s + 
        rez_std_gold_4s + rez_std_gold_6s + rez_std_gold_8s + rez_rend_gold_1s + 
        rez_rend_gold_2s + rez_rend_gold_3s + rez_rend_gold_4s + 
        rez_rend_gold_6s + rachas_sube_gold + rachas_sube_stdgold_rez1d + 
        rez_avg_gold_1s_2 + rend_2 + rachas_sube_gold_2
    
                                Df Sum of Sq    RSS    AIC
    - rez_std_gold_3s            1      28.0 205053 2459.8
    - rend                       1      63.6 205088 2459.9
    - rez_avg_gold_1s_2          1      68.8 205094 2459.9
    - mes_Ago                    1      91.9 205117 2459.9
    - mes_Jun                    1     275.3 205300 2460.3
    - rez_std_gold_2s            1     414.2 205439 2460.5
    - rez_std_gold_4s            1     429.0 205454 2460.6
    - mes_Oct                    1     445.5 205470 2460.6
    - rez_std_gold_8s            1     546.8 205572 2460.8
    - rez_rend_gold_6s           1     569.1 205594 2460.8
    - mes_Abr                    1     601.8 205627 2460.9
    - rez_avg_gold_3s            1     678.0 205703 2461.0
    - mes_Jul                    1     689.6 205714 2461.0
    - rez_rend_gold_1s           1     792.0 205817 2461.2
    <none>                                   205025 2461.8
    - mes_Dic                    1    1125.2 206150 2461.8
    - rez_avg_gold_6s            1    1157.1 206182 2461.9
    - rachas_sube_stdgold_rez1d  1    1369.9 206395 2462.3
    - rachas_sube_gold_2         1    1378.7 206403 2462.3
    - mes_Mar                    1    1427.2 206452 2462.4
    - mes_Sep                    1    1485.8 206511 2462.5
    - rez_rend_gold_4s           1    1588.3 206613 2462.7
    - rez_rend_gold_3s           1    1591.4 206616 2462.7
    - rez_rend_gold_2s           1    1730.6 206755 2463.0
    - rachas_sube_gold           1    1756.0 206781 2463.0
    - rez_avg_gold_8s            1    2092.3 207117 2463.6
    - mes_Nov                    1    2751.4 207776 2464.8
    - rez_std_gold_1s            1    3071.7 208096 2465.4
    - rez_avg_gold_4s            1    3305.3 208330 2465.8
    - mes_May                    1    3329.6 208354 2465.9
    - rez_std_gold_6s            1    5581.7 210606 2470.0
    - rez_avg_gold_1s            1    7985.4 213010 2474.3
    - rend_2                     1   11398.3 216423 2480.4
    
    Step:  AIC=2459.81
    avg_gold ~ mes_Abr + mes_Ago + mes_Dic + mes_Jul + mes_Jun + 
        mes_Mar + mes_May + mes_Nov + mes_Oct + mes_Sep + rend + 
        rez_avg_gold_1s + rez_avg_gold_3s + rez_avg_gold_4s + rez_avg_gold_6s + 
        rez_avg_gold_8s + rez_std_gold_1s + rez_std_gold_2s + rez_std_gold_4s + 
        rez_std_gold_6s + rez_std_gold_8s + rez_rend_gold_1s + rez_rend_gold_2s + 
        rez_rend_gold_3s + rez_rend_gold_4s + rez_rend_gold_6s + 
        rachas_sube_gold + rachas_sube_stdgold_rez1d + rez_avg_gold_1s_2 + 
        rend_2 + rachas_sube_gold_2
    
                                Df Sum of Sq    RSS    AIC
    - rend                       1      61.4 205114 2457.9
    - rez_avg_gold_1s_2          1      68.1 205121 2457.9
    - mes_Ago                    1      83.2 205136 2458.0
    - mes_Jun                    1     287.4 205340 2458.3
    - rez_std_gold_2s            1     439.9 205493 2458.6
    - mes_Oct                    1     445.9 205499 2458.6
    - rez_std_gold_4s            1     573.1 205626 2458.9
    - rez_std_gold_8s            1     580.2 205633 2458.9
    - rez_rend_gold_6s           1     589.8 205643 2458.9
    - mes_Abr                    1     613.8 205667 2458.9
    - rez_avg_gold_3s            1     673.6 205726 2459.1
    - mes_Jul                    1     695.5 205748 2459.1
    - rez_rend_gold_1s           1     805.6 205858 2459.3
    <none>                                   205053 2459.8
    - rez_avg_gold_6s            1    1139.3 206192 2459.9
    - mes_Dic                    1    1165.5 206218 2460.0
    - rachas_sube_gold_2         1    1385.0 206438 2460.4
    - mes_Mar                    1    1463.1 206516 2460.5
    - mes_Sep                    1    1479.1 206532 2460.6
    - rez_rend_gold_3s           1    1569.7 206622 2460.7
    - rez_rend_gold_4s           1    1574.5 206627 2460.7
    - rez_rend_gold_2s           1    1738.0 206791 2461.0
    - rachas_sube_gold           1    1779.2 206832 2461.1
    - rez_avg_gold_8s            1    2133.1 207186 2461.8
    - rachas_sube_stdgold_rez1d  1    2452.4 207505 2462.3
    - mes_Nov                    1    2768.4 207821 2462.9
    - rez_std_gold_1s            1    3044.4 208097 2463.4
    - rez_avg_gold_4s            1    3290.8 208344 2463.9
    - mes_May                    1    3390.5 208443 2464.1
    - rez_std_gold_6s            1    5581.2 210634 2468.0
    - rez_avg_gold_1s            1    8018.9 213072 2472.4
    - rend_2                     1   11414.9 216468 2478.4
    
    Step:  AIC=2457.93
    avg_gold ~ mes_Abr + mes_Ago + mes_Dic + mes_Jul + mes_Jun + 
        mes_Mar + mes_May + mes_Nov + mes_Oct + mes_Sep + rez_avg_gold_1s + 
        rez_avg_gold_3s + rez_avg_gold_4s + rez_avg_gold_6s + rez_avg_gold_8s + 
        rez_std_gold_1s + rez_std_gold_2s + rez_std_gold_4s + rez_std_gold_6s + 
        rez_std_gold_8s + rez_rend_gold_1s + rez_rend_gold_2s + rez_rend_gold_3s + 
        rez_rend_gold_4s + rez_rend_gold_6s + rachas_sube_gold + 
        rachas_sube_stdgold_rez1d + rez_avg_gold_1s_2 + rend_2 + 
        rachas_sube_gold_2
    
                                Df Sum of Sq    RSS    AIC
    - rez_avg_gold_1s_2          1        45 205159 2456.0
    - mes_Ago                    1        71 205185 2456.1
    - mes_Jun                    1       325 205439 2456.5
    - rez_std_gold_2s            1       442 205556 2456.8
    - mes_Oct                    1       453 205567 2456.8
    - rez_std_gold_4s            1       578 205692 2457.0
    - rez_std_gold_8s            1       579 205693 2457.0
    - rez_rend_gold_6s           1       590 205704 2457.0
    - mes_Abr                    1       623 205737 2457.1
    - rez_avg_gold_3s            1       676 205790 2457.2
    - mes_Jul                    1       783 205897 2457.4
    <none>                                   205114 2457.9
    - rez_avg_gold_6s            1      1126 206240 2458.0
    - mes_Dic                    1      1166 206280 2458.1
    - rachas_sube_gold_2         1      1416 206530 2458.6
    - mes_Sep                    1      1509 206623 2458.7
    - mes_Mar                    1      1511 206625 2458.7
    - rez_rend_gold_3s           1      1539 206653 2458.8
    - rez_rend_gold_4s           1      1540 206654 2458.8
    - rez_rend_gold_2s           1      1712 206826 2459.1
    - rachas_sube_gold           1      1800 206914 2459.3
    - rez_avg_gold_8s            1      2076 207190 2459.8
    - rachas_sube_stdgold_rez1d  1      2431 207546 2460.4
    - mes_Nov                    1      2951 208065 2461.4
    - rez_std_gold_1s            1      3048 208163 2461.6
    - rez_avg_gold_4s            1      3246 208360 2461.9
    - mes_May                    1      3446 208560 2462.3
    - rez_std_gold_6s            1      5533 210647 2466.1
    - rez_rend_gold_1s           1      9929 215043 2473.9
    - rend_2                     1     11791 216905 2477.2
    - rez_avg_gold_1s            1     43777 248891 2529.6
    
    Step:  AIC=2456.01
    avg_gold ~ mes_Abr + mes_Ago + mes_Dic + mes_Jul + mes_Jun + 
        mes_Mar + mes_May + mes_Nov + mes_Oct + mes_Sep + rez_avg_gold_1s + 
        rez_avg_gold_3s + rez_avg_gold_4s + rez_avg_gold_6s + rez_avg_gold_8s + 
        rez_std_gold_1s + rez_std_gold_2s + rez_std_gold_4s + rez_std_gold_6s + 
        rez_std_gold_8s + rez_rend_gold_1s + rez_rend_gold_2s + rez_rend_gold_3s + 
        rez_rend_gold_4s + rez_rend_gold_6s + rachas_sube_gold + 
        rachas_sube_stdgold_rez1d + rend_2 + rachas_sube_gold_2
    
                                Df Sum of Sq    RSS    AIC
    - mes_Ago                    1        60 205220 2454.1
    - mes_Jun                    1       370 205529 2454.7
    - mes_Oct                    1       438 205598 2454.8
    - rez_std_gold_2s            1       466 205626 2454.9
    - rez_rend_gold_6s           1       574 205733 2455.1
    - rez_std_gold_4s            1       608 205767 2455.1
    - rez_std_gold_8s            1       625 205784 2455.2
    - mes_Abr                    1       663 205822 2455.2
    - rez_avg_gold_3s            1       796 205955 2455.5
    - mes_Jul                    1       825 205984 2455.5
    <none>                                   205159 2456.0
    - rez_avg_gold_6s            1      1089 206248 2456.0
    - mes_Dic                    1      1148 206307 2456.1
    - rachas_sube_gold_2         1      1425 206584 2456.7
    - mes_Sep                    1      1490 206649 2456.8
    - mes_Mar                    1      1503 206663 2456.8
    - rez_rend_gold_3s           1      1513 206672 2456.8
    - rez_rend_gold_4s           1      1515 206674 2456.8
    - rachas_sube_gold           1      1781 206941 2457.3
    - rez_rend_gold_2s           1      1939 207099 2457.6
    - rez_avg_gold_8s            1      2059 207218 2457.8
    - rachas_sube_stdgold_rez1d  1      2535 207694 2458.7
    - mes_Nov                    1      2916 208075 2459.4
    - rez_std_gold_1s            1      3004 208163 2459.6
    - rez_avg_gold_4s            1      3278 208437 2460.1
    - mes_May                    1      3632 208791 2460.7
    - rez_std_gold_6s            1      5489 210648 2464.1
    - rez_rend_gold_1s           1      9923 215083 2472.0
    - rend_2                     1     11746 216905 2475.2
    - rez_avg_gold_1s            1    280632 485792 2782.4
    
    Step:  AIC=2454.12
    avg_gold ~ mes_Abr + mes_Dic + mes_Jul + mes_Jun + mes_Mar + 
        mes_May + mes_Nov + mes_Oct + mes_Sep + rez_avg_gold_1s + 
        rez_avg_gold_3s + rez_avg_gold_4s + rez_avg_gold_6s + rez_avg_gold_8s + 
        rez_std_gold_1s + rez_std_gold_2s + rez_std_gold_4s + rez_std_gold_6s + 
        rez_std_gold_8s + rez_rend_gold_1s + rez_rend_gold_2s + rez_rend_gold_3s + 
        rez_rend_gold_4s + rez_rend_gold_6s + rachas_sube_gold + 
        rachas_sube_stdgold_rez1d + rend_2 + rachas_sube_gold_2
    
                                Df Sum of Sq    RSS    AIC
    - rez_std_gold_2s            1       453 205673 2453.0
    - mes_Jun                    1       552 205772 2453.2
    - rez_rend_gold_6s           1       561 205780 2453.2
    - rez_std_gold_4s            1       583 205802 2453.2
    - mes_Oct                    1       615 205835 2453.3
    - rez_std_gold_8s            1       667 205887 2453.4
    - rez_avg_gold_3s            1       843 206063 2453.7
    - mes_Abr                    1       925 206145 2453.8
    - rez_avg_gold_6s            1      1039 206258 2454.1
    <none>                                   205220 2454.1
    - mes_Jul                    1      1132 206352 2454.2
    - rachas_sube_gold_2         1      1432 206652 2454.8
    - rez_rend_gold_3s           1      1458 206678 2454.8
    - rez_rend_gold_4s           1      1458 206678 2454.8
    - mes_Dic                    1      1567 206787 2455.0
    - rachas_sube_gold           1      1785 207004 2455.4
    - mes_Sep                    1      1862 207082 2455.6
    - mes_Mar                    1      1917 207137 2455.7
    - rez_rend_gold_2s           1      2031 207250 2455.9
    - rez_avg_gold_8s            1      2060 207280 2455.9
    - rachas_sube_stdgold_rez1d  1      2550 207770 2456.8
    - rez_std_gold_1s            1      2958 208177 2457.6
    - rez_avg_gold_4s            1      3282 208501 2458.2
    - mes_Nov                    1      3705 208925 2458.9
    - mes_May                    1      4448 209668 2460.3
    - rez_std_gold_6s            1      5531 210751 2462.3
    - rez_rend_gold_1s           1      9909 215128 2470.1
    - rend_2                     1     11726 216946 2473.3
    - rez_avg_gold_1s            1    280677 485897 2780.5
    
    Step:  AIC=2452.96
    avg_gold ~ mes_Abr + mes_Dic + mes_Jul + mes_Jun + mes_Mar + 
        mes_May + mes_Nov + mes_Oct + mes_Sep + rez_avg_gold_1s + 
        rez_avg_gold_3s + rez_avg_gold_4s + rez_avg_gold_6s + rez_avg_gold_8s + 
        rez_std_gold_1s + rez_std_gold_4s + rez_std_gold_6s + rez_std_gold_8s + 
        rez_rend_gold_1s + rez_rend_gold_2s + rez_rend_gold_3s + 
        rez_rend_gold_4s + rez_rend_gold_6s + rachas_sube_gold + 
        rachas_sube_stdgold_rez1d + rend_2 + rachas_sube_gold_2
    
                                Df Sum of Sq    RSS    AIC
    - mes_Jun                    1       515 206188 2451.9
    - mes_Oct                    1       668 206341 2452.2
    - rez_rend_gold_6s           1       690 206363 2452.2
    - rez_std_gold_4s            1       750 206423 2452.3
    - rez_std_gold_8s            1       895 206569 2452.6
    - rez_avg_gold_3s            1       972 206646 2452.8
    - rez_avg_gold_6s            1       982 206656 2452.8
    - mes_Abr                    1      1009 206682 2452.8
    <none>                                   205673 2453.0
    - mes_Jul                    1      1179 206852 2453.1
    - rachas_sube_gold_2         1      1356 207029 2453.5
    - rez_rend_gold_4s           1      1428 207101 2453.6
    - rez_rend_gold_3s           1      1453 207126 2453.7
    - mes_Dic                    1      1530 207204 2453.8
    - mes_Sep                    1      1708 207381 2454.1
    - rachas_sube_gold           1      1756 207429 2454.2
    - mes_Mar                    1      1956 207629 2454.6
    - rez_avg_gold_8s            1      2253 207926 2455.1
    - rez_rend_gold_2s           1      2254 207927 2455.1
    - rez_std_gold_1s            1      2757 208430 2456.0
    - rachas_sube_stdgold_rez1d  1      2819 208492 2456.2
    - rez_avg_gold_4s            1      3459 209132 2457.3
    - mes_Nov                    1      3660 209333 2457.7
    - mes_May                    1      4231 209904 2458.7
    - rez_std_gold_6s            1      5247 210920 2460.6
    - rez_rend_gold_1s           1     10005 215678 2469.1
    - rend_2                     1     13672 219345 2475.5
    - rez_avg_gold_1s            1    282416 488090 2780.2
    
    Step:  AIC=2451.92
    avg_gold ~ mes_Abr + mes_Dic + mes_Jul + mes_Mar + mes_May + 
        mes_Nov + mes_Oct + mes_Sep + rez_avg_gold_1s + rez_avg_gold_3s + 
        rez_avg_gold_4s + rez_avg_gold_6s + rez_avg_gold_8s + rez_std_gold_1s + 
        rez_std_gold_4s + rez_std_gold_6s + rez_std_gold_8s + rez_rend_gold_1s + 
        rez_rend_gold_2s + rez_rend_gold_3s + rez_rend_gold_4s + 
        rez_rend_gold_6s + rachas_sube_gold + rachas_sube_stdgold_rez1d + 
        rend_2 + rachas_sube_gold_2
    
                                Df Sum of Sq    RSS    AIC
    - mes_Oct                    1       422 206610 2450.7
    - rez_rend_gold_6s           1       650 206838 2451.1
    - rez_std_gold_4s            1       698 206886 2451.2
    - mes_Abr                    1       700 206888 2451.2
    - mes_Jul                    1       836 207023 2451.5
    - rez_avg_gold_3s            1       927 207115 2451.6
    - rez_std_gold_8s            1       971 207159 2451.7
    - rez_avg_gold_6s            1       999 207187 2451.8
    <none>                                   206188 2451.9
    - mes_Dic                    1      1133 207321 2452.0
    - mes_Sep                    1      1364 207551 2452.4
    - rachas_sube_gold_2         1      1388 207576 2452.5
    - rez_rend_gold_4s           1      1452 207640 2452.6
    - rez_rend_gold_3s           1      1481 207669 2452.6
    - mes_Mar                    1      1562 207749 2452.8
    - rachas_sube_gold           1      1830 208018 2453.3
    - rez_avg_gold_8s            1      2191 208379 2453.9
    - rez_rend_gold_2s           1      2196 208384 2453.9
    - rachas_sube_stdgold_rez1d  1      2617 208804 2454.7
    - rez_std_gold_1s            1      2855 209043 2455.2
    - mes_Nov                    1      3178 209366 2455.7
    - rez_avg_gold_4s            1      3431 209619 2456.2
    - mes_May                    1      3740 209928 2456.8
    - rez_std_gold_6s            1      5427 211615 2459.8
    - rez_rend_gold_1s           1     10343 216531 2468.6
    - rend_2                     1     13950 220138 2474.9
    - rez_avg_gold_1s            1    289729 495917 2784.3
    
    Step:  AIC=2450.7
    avg_gold ~ mes_Abr + mes_Dic + mes_Jul + mes_Mar + mes_May + 
        mes_Nov + mes_Sep + rez_avg_gold_1s + rez_avg_gold_3s + rez_avg_gold_4s + 
        rez_avg_gold_6s + rez_avg_gold_8s + rez_std_gold_1s + rez_std_gold_4s + 
        rez_std_gold_6s + rez_std_gold_8s + rez_rend_gold_1s + rez_rend_gold_2s + 
        rez_rend_gold_3s + rez_rend_gold_4s + rez_rend_gold_6s + 
        rachas_sube_gold + rachas_sube_stdgold_rez1d + rend_2 + rachas_sube_gold_2
    
                                Df Sum of Sq    RSS    AIC
    - mes_Abr                    1       530 207141 2449.7
    - rez_rend_gold_6s           1       639 207249 2449.9
    - mes_Jul                    1       640 207250 2449.9
    - rez_std_gold_4s            1       688 207298 2450.0
    - mes_Dic                    1       909 207520 2450.4
    - rez_std_gold_8s            1       935 207545 2450.4
    - rez_avg_gold_6s            1       960 207570 2450.5
    - rez_avg_gold_3s            1      1008 207618 2450.6
    <none>                                   206610 2450.7
    - mes_Sep                    1      1154 207765 2450.8
    - mes_Mar                    1      1283 207893 2451.1
    - rez_rend_gold_4s           1      1434 208044 2451.3
    - rez_rend_gold_3s           1      1456 208066 2451.4
    - rachas_sube_gold_2         1      1792 208402 2452.0
    - rachas_sube_gold           1      2191 208801 2452.7
    - rez_avg_gold_8s            1      2205 208815 2452.7
    - rez_rend_gold_2s           1      2335 208945 2453.0
    - rachas_sube_stdgold_rez1d  1      2656 209266 2453.6
    - mes_Nov                    1      2861 209472 2453.9
    - rez_std_gold_1s            1      2871 209482 2453.9
    - mes_May                    1      3410 210020 2454.9
    - rez_avg_gold_4s            1      3520 210130 2455.1
    - rez_std_gold_6s            1      5463 212073 2458.6
    - rez_rend_gold_1s           1     10529 217140 2467.6
    - rend_2                     1     14057 220667 2473.8
    - rez_avg_gold_1s            1    290006 496617 2782.8
    
    Step:  AIC=2449.67
    avg_gold ~ mes_Dic + mes_Jul + mes_Mar + mes_May + mes_Nov + 
        mes_Sep + rez_avg_gold_1s + rez_avg_gold_3s + rez_avg_gold_4s + 
        rez_avg_gold_6s + rez_avg_gold_8s + rez_std_gold_1s + rez_std_gold_4s + 
        rez_std_gold_6s + rez_std_gold_8s + rez_rend_gold_1s + rez_rend_gold_2s + 
        rez_rend_gold_3s + rez_rend_gold_4s + rez_rend_gold_6s + 
        rachas_sube_gold + rachas_sube_stdgold_rez1d + rend_2 + rachas_sube_gold_2
    
                                Df Sum of Sq    RSS    AIC
    - mes_Jul                    1       461 207602 2448.5
    - rez_std_gold_4s            1       652 207792 2448.9
    - rez_rend_gold_6s           1       657 207797 2448.9
    - mes_Dic                    1       682 207822 2448.9
    - mes_Sep                    1       948 208088 2449.4
    - rez_std_gold_8s            1       993 208133 2449.5
    - rez_avg_gold_6s            1      1015 208156 2449.5
    - rez_avg_gold_3s            1      1045 208186 2449.6
    - mes_Mar                    1      1053 208193 2449.6
    <none>                                   207141 2449.7
    - rez_rend_gold_4s           1      1498 208639 2450.4
    - rez_rend_gold_3s           1      1512 208653 2450.4
    - rachas_sube_gold_2         1      1692 208832 2450.8
    - rachas_sube_gold           1      2096 209236 2451.5
    - rez_avg_gold_8s            1      2151 209291 2451.6
    - rez_rend_gold_2s           1      2404 209545 2452.1
    - rachas_sube_stdgold_rez1d  1      2520 209660 2452.3
    - mes_Nov                    1      2521 209661 2452.3
    - rez_std_gold_1s            1      2757 209897 2452.7
    - mes_May                    1      3058 210199 2453.3
    - rez_avg_gold_4s            1      3638 210778 2454.3
    - rez_std_gold_6s            1      5343 212484 2457.4
    - rez_rend_gold_1s           1     10339 217480 2466.2
    - rend_2                     1     13753 220893 2472.2
    - rez_avg_gold_1s            1    291446 498586 2782.3
    
    Step:  AIC=2448.52
    avg_gold ~ mes_Dic + mes_Mar + mes_May + mes_Nov + mes_Sep + 
        rez_avg_gold_1s + rez_avg_gold_3s + rez_avg_gold_4s + rez_avg_gold_6s + 
        rez_avg_gold_8s + rez_std_gold_1s + rez_std_gold_4s + rez_std_gold_6s + 
        rez_std_gold_8s + rez_rend_gold_1s + rez_rend_gold_2s + rez_rend_gold_3s + 
        rez_rend_gold_4s + rez_rend_gold_6s + rachas_sube_gold + 
        rachas_sube_stdgold_rez1d + rend_2 + rachas_sube_gold_2
    
                                Df Sum of Sq    RSS    AIC
    - mes_Dic                    1       520 208122 2447.5
    - rez_std_gold_4s            1       572 208173 2447.6
    - rez_rend_gold_6s           1       655 208257 2447.7
    - mes_Sep                    1       800 208402 2448.0
    - mes_Mar                    1       887 208489 2448.1
    - rez_avg_gold_3s            1       976 208578 2448.3
    - rez_std_gold_8s            1      1010 208611 2448.4
    - rez_avg_gold_6s            1      1039 208640 2448.4
    <none>                                   207602 2448.5
    - rez_rend_gold_4s           1      1529 209130 2449.3
    - rez_rend_gold_3s           1      1553 209155 2449.4
    - rachas_sube_gold_2         1      1726 209327 2449.7
    - rez_avg_gold_8s            1      2151 209752 2450.4
    - rachas_sube_gold           1      2179 209780 2450.5
    - mes_Nov                    1      2245 209846 2450.6
    - rez_rend_gold_2s           1      2294 209895 2450.7
    - rachas_sube_stdgold_rez1d  1      2387 209989 2450.9
    - rez_std_gold_1s            1      2622 210224 2451.3
    - mes_May                    1      2797 210398 2451.6
    - rez_avg_gold_4s            1      3557 211158 2453.0
    - rez_std_gold_6s            1      5421 213023 2456.3
    - rez_rend_gold_1s           1     10304 217905 2465.0
    - rend_2                     1     13768 221369 2471.0
    - rez_avg_gold_1s            1    291625 499227 2780.8
    
    Step:  AIC=2447.47
    avg_gold ~ mes_Mar + mes_May + mes_Nov + mes_Sep + rez_avg_gold_1s + 
        rez_avg_gold_3s + rez_avg_gold_4s + rez_avg_gold_6s + rez_avg_gold_8s + 
        rez_std_gold_1s + rez_std_gold_4s + rez_std_gold_6s + rez_std_gold_8s + 
        rez_rend_gold_1s + rez_rend_gold_2s + rez_rend_gold_3s + 
        rez_rend_gold_4s + rez_rend_gold_6s + rachas_sube_gold + 
        rachas_sube_stdgold_rez1d + rend_2 + rachas_sube_gold_2
    
                                Df Sum of Sq    RSS    AIC
    - rez_std_gold_4s            1       623 208745 2446.6
    - rez_rend_gold_6s           1       649 208771 2446.7
    - mes_Sep                    1       678 208800 2446.7
    - mes_Mar                    1       729 208850 2446.8
    - rez_avg_gold_3s            1       956 209077 2447.2
    - rez_std_gold_8s            1       957 209079 2447.2
    <none>                                   208122 2447.5
    - rez_avg_gold_6s            1      1214 209335 2447.7
    - rachas_sube_gold_2         1      1707 209828 2448.6
    - rez_rend_gold_4s           1      1740 209862 2448.7
    - rez_rend_gold_3s           1      1744 209865 2448.7
    - mes_Nov                    1      1963 210085 2449.1
    - rez_avg_gold_8s            1      2022 210144 2449.2
    - rachas_sube_gold           1      2141 210263 2449.4
    - rachas_sube_stdgold_rez1d  1      2223 210344 2449.5
    - rez_rend_gold_2s           1      2261 210383 2449.6
    - mes_May                    1      2486 210608 2450.0
    - rez_std_gold_1s            1      2804 210925 2450.6
    - rez_avg_gold_4s            1      3714 211836 2452.2
    - rez_std_gold_6s            1      5243 213365 2454.9
    - rez_rend_gold_1s           1     10541 218663 2464.3
    - rend_2                     1     14504 222626 2471.1
    - rez_avg_gold_1s            1    303565 511687 2788.2
    
    Step:  AIC=2446.61
    avg_gold ~ mes_Mar + mes_May + mes_Nov + mes_Sep + rez_avg_gold_1s + 
        rez_avg_gold_3s + rez_avg_gold_4s + rez_avg_gold_6s + rez_avg_gold_8s + 
        rez_std_gold_1s + rez_std_gold_6s + rez_std_gold_8s + rez_rend_gold_1s + 
        rez_rend_gold_2s + rez_rend_gold_3s + rez_rend_gold_4s + 
        rez_rend_gold_6s + rachas_sube_gold + rachas_sube_stdgold_rez1d + 
        rend_2 + rachas_sube_gold_2
    
                                Df Sum of Sq    RSS    AIC
    - rez_rend_gold_6s           1       567 209312 2445.7
    - mes_Sep                    1       577 209321 2445.7
    - mes_Mar                    1       835 209580 2446.1
    - rez_avg_gold_3s            1      1011 209756 2446.4
    <none>                                   208745 2446.6
    - rez_avg_gold_6s            1      1136 209881 2446.7
    - rez_std_gold_8s            1      1319 210063 2447.0
    - rez_rend_gold_3s           1      1560 210304 2447.4
    - rez_rend_gold_4s           1      1580 210325 2447.5
    - rachas_sube_gold_2         1      1610 210355 2447.5
    - rez_avg_gold_8s            1      1765 210510 2447.8
    - rachas_sube_stdgold_rez1d  1      1802 210546 2447.9
    - mes_Nov                    1      1948 210693 2448.2
    - rachas_sube_gold           1      2118 210863 2448.5
    - rez_std_gold_1s            1      2318 211063 2448.8
    - rez_rend_gold_2s           1      2412 211157 2449.0
    - mes_May                    1      2571 211316 2449.3
    - rez_avg_gold_4s            1      3687 212432 2451.3
    - rez_std_gold_6s            1      4762 213506 2453.2
    - rez_rend_gold_1s           1     10529 219273 2463.4
    - rend_2                     1     13966 222710 2469.3
    - rez_avg_gold_1s            1    305134 513879 2787.8
    
    Step:  AIC=2445.65
    avg_gold ~ mes_Mar + mes_May + mes_Nov + mes_Sep + rez_avg_gold_1s + 
        rez_avg_gold_3s + rez_avg_gold_4s + rez_avg_gold_6s + rez_avg_gold_8s + 
        rez_std_gold_1s + rez_std_gold_6s + rez_std_gold_8s + rez_rend_gold_1s + 
        rez_rend_gold_2s + rez_rend_gold_3s + rez_rend_gold_4s + 
        rachas_sube_gold + rachas_sube_stdgold_rez1d + rend_2 + rachas_sube_gold_2
    
                                Df Sum of Sq    RSS    AIC
    - mes_Sep                    1       578 209890 2444.7
    - mes_Mar                    1       814 210126 2445.1
    - rez_avg_gold_3s            1       885 210197 2445.2
    <none>                                   209312 2445.7
    - rez_std_gold_8s            1      1108 210420 2445.7
    - rez_avg_gold_6s            1      1367 210679 2446.1
    - rez_avg_gold_8s            1      1392 210704 2446.2
    - rez_rend_gold_4s           1      1447 210759 2446.3
    - rez_rend_gold_3s           1      1495 210807 2446.4
    - rachas_sube_gold_2         1      1623 210936 2446.6
    - rachas_sube_stdgold_rez1d  1      1644 210956 2446.6
    - mes_Nov                    1      1879 211191 2447.1
    - rachas_sube_gold           1      2081 211393 2447.4
    - rez_rend_gold_2s           1      2223 211535 2447.7
    - rez_std_gold_1s            1      2272 211584 2447.8
    - mes_May                    1      2557 211869 2448.3
    - rez_avg_gold_4s            1      3447 212759 2449.9
    - rez_std_gold_6s            1      4593 213906 2451.9
    - rez_rend_gold_1s           1     10457 219769 2462.2
    - rend_2                     1     13648 222960 2467.7
    - rez_avg_gold_1s            1    305080 514392 2786.2
    
    Step:  AIC=2444.7
    avg_gold ~ mes_Mar + mes_May + mes_Nov + rez_avg_gold_1s + rez_avg_gold_3s + 
        rez_avg_gold_4s + rez_avg_gold_6s + rez_avg_gold_8s + rez_std_gold_1s + 
        rez_std_gold_6s + rez_std_gold_8s + rez_rend_gold_1s + rez_rend_gold_2s + 
        rez_rend_gold_3s + rez_rend_gold_4s + rachas_sube_gold + 
        rachas_sube_stdgold_rez1d + rend_2 + rachas_sube_gold_2
    
                                Df Sum of Sq    RSS    AIC
    - mes_Mar                    1       658 210549 2443.9
    - rez_avg_gold_3s            1       790 210680 2444.1
    <none>                                   209890 2444.7
    - rez_std_gold_8s            1      1209 211100 2444.9
    - rez_avg_gold_6s            1      1535 211426 2445.5
    - rachas_sube_gold_2         1      1560 211450 2445.5
    - rez_rend_gold_4s           1      1618 211508 2445.6
    - rez_avg_gold_8s            1      1629 211519 2445.6
    - rez_rend_gold_3s           1      1665 211556 2445.7
    - rachas_sube_stdgold_rez1d  1      1708 211598 2445.8
    - mes_Nov                    1      1709 211599 2445.8
    - rachas_sube_gold           1      1913 211803 2446.2
    - rez_rend_gold_2s           1      2087 211978 2446.5
    - rez_std_gold_1s            1      2333 212223 2446.9
    - mes_May                    1      2394 212285 2447.0
    - rez_avg_gold_4s            1      3453 213344 2448.9
    - rez_std_gold_6s            1      4834 214725 2451.4
    - rez_rend_gold_1s           1     10371 220261 2461.1
    - rend_2                     1     13770 223660 2466.9
    - rez_avg_gold_1s            1    307967 517858 2786.8
    
    Step:  AIC=2443.89
    avg_gold ~ mes_May + mes_Nov + rez_avg_gold_1s + rez_avg_gold_3s + 
        rez_avg_gold_4s + rez_avg_gold_6s + rez_avg_gold_8s + rez_std_gold_1s + 
        rez_std_gold_6s + rez_std_gold_8s + rez_rend_gold_1s + rez_rend_gold_2s + 
        rez_rend_gold_3s + rez_rend_gold_4s + rachas_sube_gold + 
        rachas_sube_stdgold_rez1d + rend_2 + rachas_sube_gold_2
    
                                Df Sum of Sq    RSS    AIC
    - rez_avg_gold_3s            1       773 211322 2443.3
    <none>                                   210549 2443.9
    - rez_std_gold_8s            1      1281 211830 2444.2
    - rez_avg_gold_6s            1      1300 211848 2444.2
    - rez_rend_gold_4s           1      1368 211917 2444.4
    - rez_rend_gold_3s           1      1428 211977 2444.5
    - mes_Nov                    1      1539 212088 2444.7
    - rachas_sube_stdgold_rez1d  1      1662 212210 2444.9
    - rez_avg_gold_8s            1      1685 212234 2444.9
    - rachas_sube_gold_2         1      1831 212380 2445.2
    - rachas_sube_gold           1      2068 212617 2445.6
    - rez_rend_gold_2s           1      2075 212624 2445.6
    - mes_May                    1      2187 212736 2445.8
    - rez_std_gold_1s            1      2262 212810 2446.0
    - rez_avg_gold_4s            1      3270 213818 2447.8
    - rez_std_gold_6s            1      4892 215441 2450.6
    - rez_rend_gold_1s           1     10509 221058 2460.4
    - rend_2                     1     14112 224661 2466.6
    - rez_avg_gold_1s            1    310547 521096 2787.2
    
    Step:  AIC=2443.29
    avg_gold ~ mes_May + mes_Nov + rez_avg_gold_1s + rez_avg_gold_4s + 
        rez_avg_gold_6s + rez_avg_gold_8s + rez_std_gold_1s + rez_std_gold_6s + 
        rez_std_gold_8s + rez_rend_gold_1s + rez_rend_gold_2s + rez_rend_gold_3s + 
        rez_rend_gold_4s + rachas_sube_gold + rachas_sube_stdgold_rez1d + 
        rend_2 + rachas_sube_gold_2
    
                                Df Sum of Sq    RSS    AIC
    - rez_avg_gold_6s            1       961 212282 2443.0
    - rez_rend_gold_4s           1      1017 212338 2443.1
    - rez_rend_gold_3s           1      1068 212390 2443.2
    <none>                                   211322 2443.3
    - rez_std_gold_8s            1      1205 212527 2443.4
    - mes_Nov                    1      1366 212688 2443.7
    - rez_avg_gold_8s            1      1594 212916 2444.2
    - rachas_sube_gold_2         1      1713 213035 2444.4
    - rachas_sube_stdgold_rez1d  1      1760 213081 2444.4
    - rachas_sube_gold           1      1908 213229 2444.7
    - rez_std_gold_1s            1      2062 213383 2445.0
    - mes_May                    1      2291 213612 2445.4
    - rez_avg_gold_4s            1      4570 215892 2449.4
    - rez_std_gold_6s            1      5197 216519 2450.5
    - rez_rend_gold_2s           1      8773 220095 2456.8
    - rez_rend_gold_1s           1     11673 222995 2461.8
    - rend_2                     1     14298 225619 2466.2
    - rez_avg_gold_1s            1    329106 540427 2799.0
    
    Step:  AIC=2443.01
    avg_gold ~ mes_May + mes_Nov + rez_avg_gold_1s + rez_avg_gold_4s + 
        rez_avg_gold_8s + rez_std_gold_1s + rez_std_gold_6s + rez_std_gold_8s + 
        rez_rend_gold_1s + rez_rend_gold_2s + rez_rend_gold_3s + 
        rez_rend_gold_4s + rachas_sube_gold + rachas_sube_stdgold_rez1d + 
        rend_2 + rachas_sube_gold_2
    
                                Df Sum of Sq    RSS    AIC
    - rez_rend_gold_4s           1        58 212340 2441.1
    - rez_rend_gold_3s           1       112 212394 2441.2
    <none>                                   212282 2443.0
    - mes_Nov                    1      1496 213779 2443.7
    - rez_std_gold_8s            1      1541 213823 2443.8
    - rachas_sube_stdgold_rez1d  1      1580 213862 2443.8
    - rachas_sube_gold_2         1      1764 214046 2444.2
    - rachas_sube_gold           1      2021 214303 2444.6
    - rez_std_gold_1s            1      2030 214313 2444.6
    - mes_May                    1      2207 214489 2444.9
    - rez_avg_gold_8s            1      2303 214586 2445.1
    - rez_std_gold_6s            1      5508 217790 2450.8
    - rez_rend_gold_2s           1      8554 220836 2456.1
    - rez_rend_gold_1s           1     11728 224010 2461.5
    - rend_2                     1     13988 226270 2465.3
    - rez_avg_gold_4s            1     20488 232770 2476.1
    - rez_avg_gold_1s            1    328194 540476 2797.1
    
    Step:  AIC=2441.12
    avg_gold ~ mes_May + mes_Nov + rez_avg_gold_1s + rez_avg_gold_4s + 
        rez_avg_gold_8s + rez_std_gold_1s + rez_std_gold_6s + rez_std_gold_8s + 
        rez_rend_gold_1s + rez_rend_gold_2s + rez_rend_gold_3s + 
        rachas_sube_gold + rachas_sube_stdgold_rez1d + rend_2 + rachas_sube_gold_2
    
                                Df Sum of Sq    RSS    AIC
    - rez_rend_gold_3s           1        91 212431 2439.3
    <none>                                   212340 2441.1
    - mes_Nov                    1      1474 213814 2441.8
    - rez_std_gold_8s            1      1597 213937 2442.0
    - rachas_sube_stdgold_rez1d  1      1658 213998 2442.1
    - rachas_sube_gold_2         1      1775 214115 2442.3
    - rez_std_gold_1s            1      2051 214391 2442.8
    - rachas_sube_gold           1      2067 214407 2442.8
    - mes_May                    1      2201 214541 2443.1
    - rez_avg_gold_8s            1      2766 215106 2444.1
    - rez_std_gold_6s            1      5665 218006 2449.2
    - rez_rend_gold_2s           1      8672 221012 2454.4
    - rez_rend_gold_1s           1     11807 224147 2459.7
    - rend_2                     1     14085 226425 2463.6
    - rez_avg_gold_4s            1     21429 233769 2475.8
    - rez_avg_gold_1s            1    328651 540991 2795.4
    
    Step:  AIC=2439.28
    avg_gold ~ mes_May + mes_Nov + rez_avg_gold_1s + rez_avg_gold_4s + 
        rez_avg_gold_8s + rez_std_gold_1s + rez_std_gold_6s + rez_std_gold_8s + 
        rez_rend_gold_1s + rez_rend_gold_2s + rachas_sube_gold + 
        rachas_sube_stdgold_rez1d + rend_2 + rachas_sube_gold_2
    
                                Df Sum of Sq    RSS    AIC
    <none>                                   212431 2439.3
    - mes_Nov                    1      1489 213920 2439.9
    - rez_std_gold_8s            1      1629 214060 2440.2
    - rachas_sube_stdgold_rez1d  1      1687 214117 2440.3
    - rachas_sube_gold_2         1      1842 214273 2440.6
    - rez_std_gold_1s            1      2045 214476 2440.9
    - rachas_sube_gold           1      2173 214604 2441.2
    - mes_May                    1      2187 214618 2441.2
    - rez_avg_gold_8s            1      2975 215406 2442.6
    - rez_std_gold_6s            1      5614 218045 2447.2
    - rez_rend_gold_2s           1      8582 221013 2452.4
    - rez_rend_gold_1s           1     12105 224536 2458.4
    - rend_2                     1     14111 226542 2461.8
    - rez_avg_gold_4s            1     21595 234026 2474.2
    - rez_avg_gold_1s            1    329461 541891 2794.1


## Coeficientes

### Summary


```R
summary(m1)
```


    
    Call:
    lm(formula = avg_gold ~ mes_May + mes_Nov + rez_avg_gold_1s + 
        rez_avg_gold_4s + rez_avg_gold_8s + rez_std_gold_1s + rez_std_gold_6s + 
        rez_std_gold_8s + rez_rend_gold_1s + rez_rend_gold_2s + rachas_sube_gold + 
        rachas_sube_stdgold_rez1d + rend_2 + rachas_sube_gold_2, 
        data = train %>% select(-week, -min_date, -max_date, -std_gold))
    
    Residuals:
         Min       1Q   Median       3Q      Max 
    -167.137  -13.188    0.499   12.676   76.218 
    
    Coefficients:
                                Estimate Std. Error t value Pr(>|t|)    
    (Intercept)                  7.40167    9.77964   0.757 0.449630    
    mes_May                     -9.36185    4.82313  -1.941 0.053022 .  
    mes_Nov                     -7.33546    4.57926  -1.602 0.110043    
    rez_avg_gold_1s              1.28961    0.05413  23.825  < 2e-16 ***
    rez_avg_gold_4s             -0.34591    0.05671  -6.100 2.70e-09 ***
    rez_avg_gold_8s              0.05150    0.02274   2.264 0.024150 *  
    rez_std_gold_1s             -0.32865    0.17509  -1.877 0.061311 .  
    rez_std_gold_6s             -0.54979    0.17678  -3.110 0.002017 ** 
    rez_std_gold_8s              0.28938    0.17272   1.675 0.094704 .  
    rez_rend_gold_1s          -567.85666  124.34386  -4.567 6.77e-06 ***
    rez_rend_gold_2s          -384.15294   99.90284  -3.845 0.000142 ***
    rachas_sube_gold             3.55963    1.83977   1.935 0.053782 .  
    rachas_sube_stdgold_rez1d    2.35322    1.38050   1.705 0.089116 .  
    rend_2                    7729.55782 1567.64795   4.931 1.25e-06 ***
    rachas_sube_gold_2          -0.34552    0.19396  -1.781 0.075672 .  
    ---
    Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
    
    Residual standard error: 24.09 on 366 degrees of freedom
    Multiple R-squared:  0.984,	Adjusted R-squared:  0.9834 
    F-statistic:  1606 on 14 and 366 DF,  p-value: < 2.2e-16



### Gráfica


```R
m1$coefficients %>%
 data.frame() %>%
 rename('coef' = '.') %>%
 mutate(variable = rownames(.)) %>%
 filter(variable != '(Intercept)') %>%
 filter(!is.na(coef)) %>%
 arrange(coef) %>%
 mutate(variable = fct_inorder(variable)) %>%
 ggplot() +
 geom_col(aes(x = variable, y = coef), fill = 'darkblue') +
 theme_bw() +
 scale_y_continuous(labels = scales::comma) +
 theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.5))
```


![png](output_98_0.png)


## RMSE - Train & Valid


```R
data.frame(desc = c('train', 'valid'),
           rmse = c(rmse(train$avg_gold, predict(m1, train)),
                    rmse(valid$avg_gold, predict(m1, valid)))) %>%
 ggplot() +
 geom_bar(aes(x = desc, y = rmse), stat = 'identity', fill = 'darkblue') +
 theme_bw() +
 ggtitle('RMSE') +
 theme(axis.text.x = element_text(face = 'bold'),
       axis.text.y = element_text(face = 'bold')) +
 geom_text(aes(x = desc, y = rmse, label = round(rmse,2)), vjust = -0.5, size = 4)
```


![png](output_100_0.png)


## Gráfico de errores

Gráfica de errores buscando que se comporte como ruido blanco _(es decir, sin alguna tendencia fuertemente perceptible)_

$$Error_i = PrecioOro_i - PrecioEstimado_i $$


```R
valid %>%
 mutate(pred = predict(m1, valid)) %>%
 mutate(error = avg_gold - pred) %>%
 ggplot() +
 geom_line(aes(x = max_date, y = error, group = 1), colour = 'darkblue') +
 theme_bw() +
 ggtitle('Gráfico de errores')
```


![png](output_104_0.png)


## Observado vs Estimado


```R
options(repr.plot.width=7, repr.plot.height=4)
valid %>%
 mutate(pred = predict(m1, valid)) %>%
 ggplot() +
 geom_line(aes(x = max_date, y = avg_gold, group = 1), colour = 'darkblue') +
 geom_line(aes(x = max_date, y = pred, group = 1), colour = 'darkgreen', size = 0.3) +
 theme_bw() +
 ggtitle('Goldprice - Real vs Estimado') + 
 scale_y_continuous(labels = scales::comma) +
 xlab('Día') +
 ylab('Precio del Oro') +
 theme(axis.text.x = element_text(face = 'bold'),
       axis.text.y = element_text(face = 'bold')) +
 annotate(geom  = 'text',
          x     = as.Date('2016-01-01'),
          y     = 1650,
          label = 'Estimado',
          colour = 'darkgreen',
          hjust = 1) +
 annotate(geom  = 'text',
          x     = as.Date('2016-01-01'),
          y     = 1620,
          label = 'Real',
          colour = 'darkblue',
          hjust = 1.2)
```


![png](output_106_0.png)


# Predicción

Se hará la predicción del promedio del precio del oro de una semana después de la última registrada.

Para crear el _data frame_ para la predicción, los _rezagos_ serán equivalentes al _rezago - 1_ de los usados para crear el _data frame_ de _train_, ya que aquí se busco que el "último rezago" se la última observación-

## Lectura de datos

Se leen de nuevo los datos para crear el _data frame_ para _predict_


```R
gold_pred <- read_csv("/Users/darias/Documents/GoldPrice/Data/WGC-GOLD_DAILY_USD.csv")
names(gold_pred) <- c('date', 'goldprice')

dolar_pred <- read_csv("/Users/darias/Documents/GoldPrice/Data/Consulta_20190502-142917176.csv", 
    skip = 17)
names(dolar_pred) <- c('date', 'dolarprice')
dolar_pred %>%
 mutate(date = paste0(substr(date,7,10),
                     '-',
                     substr(date,4,5),
                     '-',
                     substr(date,1,2))) %>%
 mutate(date = as.Date(date)) -> dolar_pred
```

    Parsed with column specification:
    cols(
      Date = col_date(format = ""),
      Value = col_double()
    )
    Parsed with column specification:
    cols(
      Fecha = col_character(),
      SF63528 = col_double()
    )


## Homologación de datos


```R
max_date <- min(max(gold_pred$date), max(dolar_pred$date))
min_date <- max(min(gold_pred$date), min(dolar_pred$date))

dolar_pred %>%
 filter(date >= min_date) %>%
 filter(date <= max_date) -> dolar_pred

gold_pred %>%
 filter(date >= min_date) %>%
 filter(date <= max_date) -> gold_pred

max_date <- min(max(gold_pred$date), max(dolar_pred$date))
min_date <- max(min(gold_pred$date), min(dolar_pred$date))

dolar_pred %>%
 filter(date >= min_date) %>%
 filter(date <= max_date) -> dolar_pred

gold_pred %>%
 filter(date >= min_date) %>%
 filter(date <= max_date) -> gold_pred
```

## Juntar datos


```R
dolar_pred %>%
 left_join(gold_pred, by = 'date') %>%
 select(-dolarprice) -> data_pred
```

## Limpiar datos


```R
data_pred %>%
 filter(!is.na(goldprice)) -> data_pred
```

Se dejarán datos a partir del 2015


```R
data_pred %>%
 filter(date >= as.Date('2015-01-01')) -> data_pred
```

## Promedio Semanal


```R
data_pred %>%
 mutate(week = paste0(year(date),'-',format(date, '%W'))) %>%
 group_by(week) %>%
 summarise(min_date = min(date),
           max_date = max(date),
           avg_gold = mean(goldprice, na.rm = TRUE),
           std_gold = sd(goldprice, na.rm = TRUE)) %>%
 arrange(as.numeric(gsub('-','',week))) %>%
 filter(!is.na(std_gold)) %>%
 mutate(mes = month(max_date)) %>%
 mutate(mes = ifelse(mes == 1, 'Ene', mes),
        mes = ifelse(mes == 2, 'Feb', mes),
        mes = ifelse(mes == 3, 'Mar', mes),
        mes = ifelse(mes == 4, 'Abr', mes),
        mes = ifelse(mes == 5, 'May', mes),
        mes = ifelse(mes == 6, 'Jun', mes),
        mes = ifelse(mes == 7, 'Jul', mes),
        mes = ifelse(mes == 8, 'Ago', mes),
        mes = ifelse(mes == 9, 'Sep', mes),
        mes = ifelse(mes == 10, 'Oct', mes),
        mes = ifelse(mes == 11, 'Nov', mes),
        mes = ifelse(mes == 12, 'Dic', mes)) -> data_pred
```

## Rendimientos del oro


```R
data_pred <- rend_func(data_pred, 'avg_gold', 'rend', ind_train = FALSE)
```

## Rezagos


```R
data_pred <- rez_func(data_pred, 'avg_gold', 'rez_avg_gold_1s',  1,  FALSE)
data_pred <- rez_func(data_pred, 'avg_gold', 'rez_avg_gold_2s',  2,  FALSE)
data_pred <- rez_func(data_pred, 'avg_gold', 'rez_avg_gold_3s',  3,  FALSE)
data_pred <- rez_func(data_pred, 'avg_gold', 'rez_avg_gold_4s',  4,  FALSE)
data_pred <- rez_func(data_pred, 'avg_gold', 'rez_avg_gold_6s',  6,  FALSE)
data_pred <- rez_func(data_pred, 'avg_gold', 'rez_avg_gold_8s',  8,  FALSE)
```


```R
data_pred <- rez_func(data_pred, 'std_gold', 'rez_std_gold_1s',  1,  FALSE)
data_pred <- rez_func(data_pred, 'std_gold', 'rez_std_gold_2s',  2,  FALSE)
data_pred <- rez_func(data_pred, 'std_gold', 'rez_std_gold_3s',  3,  FALSE)
data_pred <- rez_func(data_pred, 'std_gold', 'rez_std_gold_4s',  4,  FALSE)
data_pred <- rez_func(data_pred, 'std_gold', 'rez_std_gold_6s',  6,  FALSE)
data_pred <- rez_func(data_pred, 'std_gold', 'rez_std_gold_8s',  8,  FALSE)
```


```R
data_pred <- rez_func(data_pred, 'rend', 'rez_rend_gold_1s',  1,  FALSE)
data_pred <- rez_func(data_pred, 'rend', 'rez_rend_gold_2s',  2,  FALSE)
data_pred <- rez_func(data_pred, 'rend', 'rez_rend_gold_3s',  3,  FALSE)
data_pred <- rez_func(data_pred, 'rend', 'rez_rend_gold_4s',  4,  FALSE)
data_pred <- rez_func(data_pred, 'rend', 'rez_rend_gold_6s',  6,  FALSE)
data_pred <- rez_func(data_pred, 'rend', 'rez_rend_gold_8s',  8,  FALSE)
```

## Medias Móviles


```R
data_pred <- mm_func(data_pred, 'avg_gold', 'mm2_avg_gold', 2, TRUE)
data_pred <- mm_func(data_pred, 'avg_gold', 'mm3_avg_gold', 3, TRUE)
data_pred <- mm_func(data_pred, 'avg_gold', 'mm4_avg_gold', 4, TRUE)

data_pred <- mm_func(data_pred, 'std_gold', 'mm2_std_gold', 2, TRUE)
data_pred <- mm_func(data_pred, 'std_gold', 'mm3_std_gold', 3, TRUE)
data_pred <- mm_func(data_pred, 'std_gold', 'mm4_std_gold', 4, TRUE)

data_pred <- mm_func(data_pred, 'rend', 'mm2_rend', 2, TRUE)
data_pred <- mm_func(data_pred, 'rend', 'mm3_rend', 3, TRUE)
data_pred <- mm_func(data_pred, 'rend', 'mm4_rend', 4, TRUE)
```

## Rachas


```R
data_pred <- racha(data_pred, 'avg_gold', 'rachas_sube_gold', FALSE)
data_pred <- racha(data_pred, 'rez_avg_gold_1s', 'rachas_sube_gold_rez1d', FALSE)
data_pred <- racha(data_pred, 'rez_std_gold_1s', 'rachas_sube_stdgold_rez1d', FALSE)
```

## Dummies mes


```R
data_pred %>%
 mutate(mes = as.factor(mes)) -> data_pred

one_hot(as.data.table(data_pred)) %>%
 as.data.frame -> data_pred
```

## Cuadrados


```R
data_pred %>%
 mutate(rez_avg_gold_1s_2 = rez_avg_gold_1s^2,
        rend_2 = rend^2,
        rachas_sube_gold_2 = rachas_sube_gold^2) -> data_pred
```

## Se quitan NA's


```R
data_pred %>%
 filter(!is.na(rez_avg_gold_8s)) %>%
 filter(!is.na(rez_rend_gold_8s)) -> data_pred
```

## Pronóstico vs Observado


```R
options(repr.plot.width=7, repr.plot.height=4)
data_pred %>%
 mutate(pred = predict(m1, data_pred)) %>%
 ggplot() +
 geom_line(aes(x = max_date, y = avg_gold, group = 1), colour = 'darkblue') +
 geom_line(aes(x = max_date, y = pred, group = 1), colour = 'darkgreen', size = 0.3) +
 theme_bw() +
 ggtitle('Promedio Precio Oro Semanal - Real vs Estimado Histórico') + 
 scale_y_continuous(labels = scales::comma) +
 xlab('Día') +
 ylab('Precio del Oro') +
 theme(axis.text.x = element_text(face = 'bold'),
       axis.text.y = element_text(face = 'bold')) +
 annotate(geom  = 'text',
          x     = as.Date('2015-11-01'),
          y     = 1350,
          label = 'Estimado',
          colour = 'darkgreen',
          hjust = 1) +
 annotate(geom  = 'text',
          x     = as.Date('2015-11-01'),
          y     = 1320,
          label = 'Real',
          colour = 'darkblue',
          hjust = 1.2)
```


![png](output_140_0.png)


## Predicción siguiente semana


```R
sp <- predict(m1, tail(data_pred,1))
paste0('Predicción Promedio Oro siguiente semana',
       ' : ',
       sp
       )
```


'Predicción Promedio Oro siguiente semana : 1283.80309083262'


## Precio Promedio semana actual


```R
#sa <- predict(m1, data_pred[nrow(data_pred)-2,])
sa <- tail(data_pred$avg_gold,1)
paste0('Predicción Promedio Oro Semana actual',
       ' : ',
       sa)
```


'Predicción Promedio Oro Semana actual : 1276.37'


## Resumen Pronóstico


```R
# RMSE del modelo en validación
rmse_valid <- rmse(valid$avg_gold, predict(m1, valid))

# Indicador de alta o baja: pronosticado proxima semana vs semana actual
if(sp >= sa){
    a <- 'subida'
    r    <- abs((sp)/(sa))-1
} else {
    a <- 'bajada'
    r    <- 1-abs((sp)/(sa))
}

# Intervalo de confianza inferior
if((sp-rmse_valid)/(sa) >= 1){
    II <- (sp-rmse_valid)/(sa)-1
} else {
    II <- 1-(sp-rmse_valid)/(sa)
}

# Intervalo de confianza superior
if((sp+rmse_valid)/(sa) >= 1){
    IS <- (sp+rmse_valid)/(sa)-1
} else {
    IS <- 1-(sp+rmse_valid)/(sa)
}

# Indicador de alta o baja de intervalo inferior
if(sp-rmse_valid>=sa){
    ai <- 'subir'
} else {
    ai <- 'bajar'
}

# Indicador de alta o baja de intervalo superior
if(sp+rmse_valid>=sa){
    as <- 'subir'
} else {
    as <- 'bajar'
}
```


```R
t0 <- paste0('La semana del ', tail(data_pred$min_date,1), ' al ', tail(data_pred$max_date,1), ' tuvo un precio promedio de: $', tail(data_pred$avg_gold,1), ' dlls')
t1 <- paste0('Para la próxima semana, se pronostica un valor promedio del oro de: $', round(sp,2), ' dlls')
t2 <- paste0('Lo cuál indicaría una ', a, ' del ', round(r*100,2), '% respecto a esta semana')
t3 <- paste0('Pero podría ', ai, ' de ', round(II*100,2), '%', ' a ', as, ' ', round(IS*100,2), '%')
```


```R
data.frame(. = c(t0,t1,t2,t3))
```


<table>
<thead><tr><th scope=col>.</th></tr></thead>
<tbody>
	<tr><td>La semana del 2019-04-22 al 2019-04-26 tuvo un precio promedio de: $1276.37 dlls</td></tr>
	<tr><td>Para la próxima semana, se pronostica un valor promedio del oro de: $1283.8 dlls</td></tr>
	<tr><td>Lo cuál indicaría una subida del 0.58% respecto a esta semana                   </td></tr>
	<tr><td>Pero podría bajar de 0.92% a subir 2.08%                                        </td></tr>
</tbody>
</table>


